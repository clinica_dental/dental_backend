"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExternalMiddleware = void 0;
const common_1 = require("@nestjs/common");
const sender_handling_1 = require("../exception/sender.handling");
const error_handling_1 = require("../exception/error.handling");
const CustomLogger_1 = require("../util/CustomLogger");
const logger = new CustomLogger_1.Logger('MIDDLEWARE');
let ExternalMiddleware = class ExternalMiddleware {
    use(req, res, next) {
        try {
            req.logger = logger;
            if (!['GET', 'POST'].includes(req.method)) {
                (0, error_handling_1.throwError)(common_1.HttpStatus.UNAUTHORIZED, 'cabecera HTTP no Valida', 2002)();
            }
            next();
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.UNAUTHORIZED, 0, error.message);
        }
    }
};
ExternalMiddleware = __decorate([
    (0, common_1.Injectable)()
], ExternalMiddleware);
exports.ExternalMiddleware = ExternalMiddleware;
//# sourceMappingURL=external.middleware.js.map