"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var GlobalMiddleware_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.GlobalMiddleware = void 0;
const sender_handling_1 = require("../exception/sender.handling");
const error_handling_1 = require("../exception/error.handling");
const common_1 = require("@nestjs/common");
const jsonwebtoken_1 = require("jsonwebtoken");
const CustomLogger_1 = require("../util/CustomLogger");
const uuid_1 = require("uuid");
const key_1 = require("../../config/key");
const getDurationInMilliseconds = (start) => {
    const NS_PER_SEC = 1e9;
    const NS_TO_MS = 1e6;
    const diff = process.hrtime(start);
    return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};
let GlobalMiddleware = GlobalMiddleware_1 = class GlobalMiddleware {
    constructor() {
        this.algorithm = 'RS256';
    }
    use(req, res, next) {
        try {
            req.headers.tracerId = req.headers.tracerId ? req.headers.tracerId.toString() : `${process.env.NAME.replace(/ /, '-').toUpperCase() || ''}#${(0, uuid_1.v4)()}`;
            const logger = new CustomLogger_1.Logger(GlobalMiddleware_1.name, req.headers.tracerId);
            logger.setContext(GlobalMiddleware_1.name);
            logger.debug(`${req.method}   ${req.ip}   ${req.get('User-Agent')}   ${req.protocol}   ${req.originalUrl}   [STARTED]`);
            const start = process.hrtime();
            res.on('finish', () => {
                const durationInMilliseconds = getDurationInMilliseconds(start);
                logger.setContext(GlobalMiddleware_1.name);
                logger.debug(`${req.method}   ${req.ip}   ${req.get('User-Agent')}   ${req.protocol}   ${req.originalUrl}   [FINISHED]   ${res.statusCode}   ${durationInMilliseconds.toLocaleString()} ms`);
            });
            res.on('close', () => {
                const durationInMilliseconds = getDurationInMilliseconds(start);
                logger.setContext(GlobalMiddleware_1.name);
                logger.debug(`${req.method}   ${req.ip}   ${req.get('User-Agent')}   ${req.protocol}   ${req.originalUrl}   [CLOSED]   ${res.statusCode}   ${durationInMilliseconds.toLocaleString()} ms`);
            });
            let legit = {};
            if (!req.headers.authorization) {
                (0, error_handling_1.throwError)(common_1.HttpStatus.UNAUTHORIZED, 'La petición no tiene la cabecera de autenticación');
            }
            let tokenAuth = req.headers.authorization.split(' ');
            if (tokenAuth[0] !== 'Bearer') {
                (0, error_handling_1.throwError)(common_1.HttpStatus.UNAUTHORIZED, 'El tipo de autorización no es válido');
            }
            const token = tokenAuth[1];
            const verifyOptions = {};
            try {
                legit = (0, jsonwebtoken_1.verify)(token, key_1.publicKEY, verifyOptions);
            }
            catch (error) {
                console.log(error);
                (0, error_handling_1.throwError)(common_1.HttpStatus.UNAUTHORIZED, 'El token no es válido');
            }
            if (!legit.per_codigo) {
                (0, error_handling_1.throwError)(common_1.HttpStatus.UNAUTHORIZED, 'Estructura de token no valida', 2002)();
            }
            req.headers.perCodigo = legit.per_codigo;
            req.logger = logger;
            switch (req.method) {
                case 'GET':
                    req.query.usuario = legit.per_codigo.toString();
                    req.params.usuario = legit.per_codigo.toString();
                    break;
                case 'DELETE':
                    req.body.usuario = legit.per_codigo;
                    req.params.usuario = legit.per_codigo.toString();
                    break;
                case 'POST':
                    req.body.usuario = legit.per_codigo;
                    break;
                case 'PUT':
                    req.body.usuario = legit.per_codigo;
                    break;
                case 'PATCH':
                    req.body.usuario = legit.per_codigo;
                    req.params.usuario = legit.per_codigo.toString();
                    break;
                default:
                    (0, error_handling_1.throwError)(common_1.HttpStatus.UNAUTHORIZED, 'cabecera HTTP no Valida', 2002)();
            }
            next();
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.UNAUTHORIZED, 0, error.message);
        }
    }
};
GlobalMiddleware = GlobalMiddleware_1 = __decorate([
    (0, common_1.Injectable)()
], GlobalMiddleware);
exports.GlobalMiddleware = GlobalMiddleware;
//# sourceMappingURL=global.middleware.js.map