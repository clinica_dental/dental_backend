import { Request, Response, NextFunction } from 'express';
import { NestMiddleware } from '@nestjs/common';
export declare class GlobalMiddleware implements NestMiddleware {
    private algorithm;
    use(req: Request, res: Response, next: NextFunction): any;
}
