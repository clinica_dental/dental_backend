"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomToken = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
const moment = require("moment");
const uuid_1 = require("uuid");
const key_1 = require("../../config/key");
const error_handling_1 = require("../exception/error.handling");
const common_1 = require("@nestjs/common");
class CustomToken {
    constructor() {
        this.algorithm = 'RS256';
    }
    async createUserToken(payload) {
        try {
            let tokenCreated = 'invalid';
            payload.tipo = 'usuario';
            payload.iat = moment().unix();
            payload.exp = moment().add(process.env.MINUTES_EXPIRE_TOKEN, 'minutes').unix();
            payload.id = payload.id || (0, uuid_1.v4)();
            const verifyOptions = {
                algorithm: this.algorithm,
            };
            tokenCreated = (0, jsonwebtoken_1.sign)(payload, key_1.privateKEY, verifyOptions);
            return { token: `Bearer ${tokenCreated}`, finalizado: tokenCreated !== 'invalid' };
        }
        catch (error) {
            console.log(error);
            (0, error_handling_1.throwError)(common_1.HttpStatus.UNAUTHORIZED, 'No se pudo crear el token');
        }
    }
}
exports.CustomToken = CustomToken;
//# sourceMappingURL=CustomJwt.js.map