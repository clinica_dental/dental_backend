import { ConsoleLogger } from '@nestjs/common';
export declare class Logger extends ConsoleLogger {
    private tracerId;
    private method;
    private stackLogger;
    constructor(context: string, tracerId?: string);
    setLogger(level: any, message: any, stack: any): void;
    log(message: any, stack?: string): void;
    error(message: any, stack?: string): void;
    warn(message: any, stack?: string): void;
    debug(message: any, stack?: string): void;
    verbose(message: any, stack?: string): void;
    getTracerId(): string;
    getStackLogger(): any[];
    getSaveStackLogger(): {
        tracerId: string;
        stack: any[];
    };
    setContext(context: string): void;
}
