import { DataSource, EntityManager } from "typeorm";
export declare class CustomTransaction {
    private dataSource;
    private queryRunner;
    manager: EntityManager;
    static idTransaction: number;
    constructor(dataSource: DataSource);
    getStartTransaction(): Promise<void>;
    getRollbackTransaction(): Promise<void>;
    getCommitTransaction(): Promise<void>;
    getIsTransactionActive(): boolean;
    getManager(): Promise<EntityManager>;
}
