"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var CustomTransaction_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomTransaction = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
let CustomTransaction = CustomTransaction_1 = class CustomTransaction {
    constructor(dataSource) {
        this.dataSource = dataSource;
        this.queryRunner = this.dataSource.createQueryRunner();
        this.manager = this.queryRunner.manager;
        CustomTransaction_1.idTransaction++;
    }
    async getStartTransaction() {
        this.manager = await this.getManager();
        return await this.queryRunner.startTransaction();
    }
    async getRollbackTransaction() {
        return await this.queryRunner.rollbackTransaction();
    }
    async getCommitTransaction() {
        if (process.env.NODE_ENV !== 'test')
            return await this.queryRunner.commitTransaction();
        return await this.getRollbackTransaction();
    }
    getIsTransactionActive() {
        return this.queryRunner.isTransactionActive;
    }
    async getManager() {
        try {
            const resultQuery = await this.manager.query('SELECT 1');
        }
        catch (error) {
            let queryRunner2 = this.dataSource.createQueryRunner();
            let manager2 = queryRunner2.manager;
            const resultQuery2 = await manager2.query('SELECT 1');
            if (resultQuery2.length > 0) {
                this.queryRunner = queryRunner2;
                this.manager = manager2;
            }
        }
        return this.manager;
    }
};
CustomTransaction.idTransaction = 0;
CustomTransaction = CustomTransaction_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeorm_1.DataSource])
], CustomTransaction);
exports.CustomTransaction = CustomTransaction;
//# sourceMappingURL=CustomTransaction.js.map