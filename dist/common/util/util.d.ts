declare function request(config: any): Promise<object>;
declare function reemplazaParametros(texto: string, parametros: Record<string, any>): Promise<string>;
declare function enviaMailSMTP(options: Record<string, any>): Promise<Record<string, any>>;
declare function getDateFormat(fecha: string): string;
declare function bindingObjects(parentObject: any, childObject: any): any;
export { request, reemplazaParametros, enviaMailSMTP, getDateFormat, bindingObjects, };
