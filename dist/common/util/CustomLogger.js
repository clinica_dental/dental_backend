"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
const common_1 = require("@nestjs/common");
class Logger extends common_1.ConsoleLogger {
    constructor(context, tracerId) {
        super();
        this.stackLogger = [];
        super.setContext(context);
        this.tracerId = tracerId;
    }
    setLogger(level, message, stack) {
        this.stackLogger.push({
            level,
            message: message,
            stack,
            context: this.context
        });
    }
    log(message, stack) {
        this.setLogger('log', message, stack);
        super.log(`${this.tracerId} ${message}`);
    }
    error(message, stack) {
        this.setLogger('error', message, stack);
        super.error(`${this.tracerId} ${message}`);
    }
    warn(message, stack) {
        this.setLogger('warn', message, stack);
        super.warn(`${this.tracerId} ${message}`);
    }
    debug(message, stack) {
        this.setLogger('debug', message, stack);
        super.debug(`${this.tracerId} ${message}`);
    }
    verbose(message, stack) {
        this.setLogger('verbose', message, stack);
        super.verbose(`${this.tracerId} ${message}`);
    }
    getTracerId() {
        return this.tracerId;
    }
    getStackLogger() {
        return this.stackLogger;
    }
    getSaveStackLogger() {
        return {
            tracerId: this.tracerId,
            stack: this.stackLogger
        };
    }
    setContext(context) {
        super.setContext(context);
    }
}
exports.Logger = Logger;
//# sourceMappingURL=CustomLogger.js.map