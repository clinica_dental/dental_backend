"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bindingObjects = exports.getDateFormat = exports.enviaMailSMTP = exports.reemplazaParametros = exports.request = void 0;
const node_fetch_1 = require("node-fetch");
const nodemailer_1 = require("nodemailer");
const CustomLogger_1 = require("./CustomLogger");
const queryString = require("query-string");
const loggerRequest = new CustomLogger_1.Logger('REQUEST');
async function request(config) {
    const limit = 500;
    const llaves_permitidas = ['method', 'baseUrl', 'url', 'headers', 'Authorization', 'body', 'json', 'timeout', 'http_proxy'];
    let inicio = config.url.indexOf('/:');
    while (inicio > 0) {
        const final = config.url.indexOf('/', inicio + 2);
        const parametro = final > 0 ? config.url.substring(inicio + 1, final) : config.url.substring(inicio + 1);
        const indice = final > 0 ? config.url.substring(inicio + 2, final) : config.url.substring(inicio + 2);
        if (!config.params || !config.params[indice]) {
            if (config.opcionales && config.opcionales.includes(indice)) {
                config.url = await config.url.replace(parametro + '/', '');
                config.url = await config.url.replace(parametro, '');
                inicio = config.url.indexOf('/:');
                continue;
            }
            return {
                error_existente: 1,
                error_mensaje: 'No es posible armar petición.Parámetros inexistente: ' + indice,
                error_code: 500
            };
        }
        config.url = await config.url.replace(parametro, config.params[indice]);
        inicio = config.url.indexOf('/:');
    }
    loggerRequest.log(JSON.stringify(config, (key, value) => {
        if (llaves_permitidas.indexOf(key) > -1) {
            return value;
        }
        if (typeof value === 'string' && value.length > limit) {
            return value.substring(0, limit);
        }
        return value;
    }));
    config.body = JSON.stringify(config.body);
    let tmp_qs = '';
    if (config.bandera_qs) {
        tmp_qs = '?' + queryString.stringify(config.qs);
    }
    let result = (0, node_fetch_1.default)(config.baseUrl + config.url + tmp_qs, config)
        .then(verify => {
        if (verify.ok) {
            return verify;
        }
        else {
            throw verify;
        }
    })
        .then(async (res) => {
        let tmp_contenty_type = res.headers.get("Content-Type");
        if (tmp_contenty_type.includes('application/json'))
            return res.json();
        return Buffer.from(await res.buffer()).toString('base64');
    })
        .then(data => { return data; })
        .catch(async (error) => {
        loggerRequest.log(error.message);
        error.error = {
            error_existente: 1,
            error_mensaje: 'ERROR',
            codigo_estado: error.status,
            error_codigo: 0
        };
        if (error.status === 401) {
            error.error = {
                error_existente: 1,
                error_mensaje: 'ERROR 401',
                codigo_estado: error.status,
                error_codigo: config.es_jasper ? 3003 : 7003,
            };
        }
        else if (error.status === 400) {
            error.error = {
                error_existente: 1,
                error_mensaje: 'ERROR 400',
                codigo_estado: error.status,
                datos: error.error.datos ? error.error.datos : undefined,
            };
        }
        else if (error.status === 500) {
            error.error = {
                error_existente: 1,
                error_mensaje: 'Error de servicio externo',
                codigo_estado: error.status,
                error_codigo: config.es_jasper ? 3001 : 7002,
            };
        }
        else if (error.status === 404) {
            error.error = {
                error_existente: 1,
                error_mensaje: 'ERROR 404',
                codigo_estado: error.status,
                error_codigo: config.es_jasper ? 3006 : 7004,
            };
        }
        else {
            error.error = {
                error_existente: 1,
                error_mensaje: 'ERROR 400',
                codigo_estado: error.status,
                error_codigo: 7002
            };
        }
        return error.error;
    });
    return result;
}
exports.request = request;
async function reemplazaParametros(texto, parametros) {
    const texto_parametrizado = texto.replace(/\{\{(.*?)\}\}/g, function (match, token) {
        return parametros[token] ? parametros[token] : '';
    });
    return texto_parametrizado;
}
exports.reemplazaParametros = reemplazaParametros;
async function enviaMailSMTP(options) {
    if (options.message && options.message.html && options.params) {
        options.message.html = await reemplazaParametros(options.message.html, options.params);
    }
    else {
        return {
            error_existente: 1,
            error_mensaje: 'Opciones para el envio de email no establecidos'
        };
    }
    const transport = nodemailer_1.nodemailer.createTransport(options.transport);
    const envio = await transport.sendMail(options.message).then((response) => {
        return response;
    }).catch((error) => {
        return {
            error_existente: 1,
            error_mensaje: error.message
        };
    });
    if (!envio || envio.error_existente === 1) {
        return {
            error_existente: 1,
            error_mensaje: envio.error_mensaje || 'Error en envio de email'
        };
    }
    return {
        error_existente: 0,
        datos: envio
    };
}
exports.enviaMailSMTP = enviaMailSMTP;
function getDateFormat(fecha) {
    if (fecha) {
        const new_date = fecha.split('/');
        return `${new_date[2]}-${new_date[1]}-${new_date[0]}`;
    }
    else {
        return null;
    }
}
exports.getDateFormat = getDateFormat;
function bindingObjects(parentObject, childObject) {
    for (const key in childObject) {
        parentObject[key] = childObject[key];
    }
    delete parentObject['usuario'];
    return parentObject;
}
exports.bindingObjects = bindingObjects;
//# sourceMappingURL=util.js.map