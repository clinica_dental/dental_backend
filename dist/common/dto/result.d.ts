export declare class ResultDto {
    codigo: number;
    error_existente: number;
    error_mensaje: string;
    error_codigo?: number;
    datos?: any;
}
