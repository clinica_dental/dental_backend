"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataEntity = void 0;
const customService_1 = require("../exception/customService");
const error_handling_1 = require("../exception/error.handling");
class DataEntity {
    constructor() {
        this.message_custom = 'DATA ENTITY -';
    }
    async getColumns(query, manager) {
        let entityCustom = query.entity.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);
        if (entityCustom[0] == '_')
            entityCustom = entityCustom.slice(1);
        try {
            let sql = `
            SELECT
                col.column_name,
                col.data_type,
                kcu.column_name,
                tco.constraint_type
            FROM information_schema.columns col
                LEFT JOIN information_schema.key_column_usage kcu ON
                    kcu.table_name = col.table_name AND
                    kcu.column_name = col.column_name
                LEFT JOIN information_schema.table_constraints tco ON
                    tco.table_name = kcu.table_name AND
                    tco.constraint_name = kcu.constraint_name
            WHERE TRUE
                AND col.table_schema = 'documentacion'
                AND col.table_name = '${entityCustom}'
                ${query.constraint_type ? ` AND tco.constraint_type IN ${query.constraint_type} ` : ''}
                ${query.column_name ? ` AND col.column_name LIKE '%${query.column_name}%' ` : ''}
            `;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
}
exports.DataEntity = DataEntity;
//# sourceMappingURL=dataEntity.js.map