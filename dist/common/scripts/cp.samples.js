"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const ignorados = ['logs', 'node_modules', '.git'];
const raiz = './';
const extension = /\.sample$/;
function listarDirectorio(ruta) {
    const ficheros = fs.readdirSync(ruta, { encoding: 'utf8', withFileTypes: true });
    for (const fichero of ficheros) {
        if (fichero.isDirectory()) {
            if (ignorados.indexOf(fichero.name) === -1) {
                listarDirectorio(ruta + '/' + fichero.name);
            }
        }
        else {
            if (extension.test(fichero.name)) {
                const nombre = fichero.name.replace(/\.[^/.]+$/, "");
                fs.copyFileSync(ruta + '/' + fichero.name, ruta + '/' + nombre);
                console.log(("Copia generada para: " + fichero.name));
            }
        }
    }
}
listarDirectorio(raiz);
//# sourceMappingURL=cp.samples.js.map