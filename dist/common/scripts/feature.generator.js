"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
try {
    const entitieName = process.argv[2];
    const schemaName = process.argv[3];
    const outDir = process.argv[4];
    function reemplazaParametros(texto, parametros) {
        const texto_parametrizado = texto.replace(/\{\{(.*?)\}\}/g, function (match, token) {
            return parametros[token] ? parametros[token] : '';
        });
        return texto_parametrizado;
    }
    if (!entitieName) {
        console.log('Generato call sample');
        console.log('\t\tnpx ts-node src/common/scripts/feature.generator.ts [entitieName] [schema] [outDir]?');
        console.log('\t\tREQUERIDO: entitieName\t\tName of entitie');
        console.log('\t\tREQUERIDO: schemaName\t\tFile name schema');
        console.log('\t\tOPTIONAL: outDir\t\toutput full path dir');
        console.log('Sample of call');
        console.log('\t\tnpx ts-node src/common/scripts/feature.generator.ts RepositorioSample poai');
        throw new Error("Arguments fail!");
    }
    const variableSeparated = entitieName.replace(/([a-z])([A-Z])/g, '$1 $2');
    const splited = variableSeparated.split(' ');
    const instanceName = entitieName[0].toLowerCase() + entitieName.substring(1, entitieName.length);
    const refEntitie = splited.map(m => m.substring(0, 3)).join('').toLowerCase() + '_';
    let fileName = splited.map(m => '-' + m.toLowerCase()).join('');
    fileName = fileName.substring(1, fileName.length);
    let projectPath = __dirname.split('/');
    projectPath.pop();
    projectPath.pop();
    projectPath = projectPath.join('/');
    const templatePath = `${__dirname}/template`;
    const finalOutDir = outDir ? outDir : `${projectPath}/feature`;
    if (fs.existsSync(`${finalOutDir}/${fileName}`)) {
        console.error('Path already exists');
        throw new Error("Arguments fail!");
    }
    fs.mkdirSync(`${finalOutDir}/${fileName}`);
    fs.mkdirSync(`${finalOutDir}/${fileName}/dto`);
    const filesTree = {
        createDto: `${finalOutDir}/${fileName}/dto/create-${fileName}.dto.ts`,
        deleteEnable: `${finalOutDir}/${fileName}/dto/delete-enable-${fileName}.dto.ts`,
        getall: `${finalOutDir}/${fileName}/dto/getall-${fileName}.dto.ts`,
        update: `${finalOutDir}/${fileName}/dto/update-${fileName}.dto.ts`,
        controllerSpec: `${finalOutDir}/${fileName}/${fileName}.controller.spec.ts`,
        controller: `${finalOutDir}/${fileName}/${fileName}.controller.ts`,
        serviceSpec: `${finalOutDir}/${fileName}/${fileName}.service.spec.ts`,
        service: `${finalOutDir}/${fileName}/${fileName}.service.ts`,
        module: `${finalOutDir}/${fileName}/${fileName}.module.ts`,
    };
    const replacements = {
        entitieName: entitieName,
        instanceName: instanceName,
        refEntitie: refEntitie,
        fileName: fileName,
        tableName: fileName.replace(/-/g, '_'),
        schema: schemaName,
        logMessage: variableSeparated.toUpperCase()
    };
    let outTemplate = '';
    let fileCreateDtoTemplate = fs.readFileSync(path.join(templatePath, `create.dto.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileCreateDtoTemplate, replacements);
    fs.writeFileSync(filesTree.createDto, outTemplate);
    console.log('DONE: ', filesTree.createDto);
    let fileDeleteEnableDtoTemplate = fs.readFileSync(path.join(templatePath, `delete-enable.dto.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileDeleteEnableDtoTemplate, replacements);
    fs.writeFileSync(filesTree.deleteEnable, outTemplate);
    console.log('DONE: ', filesTree.deleteEnable);
    let fileGetallDtoTemplate = fs.readFileSync(path.join(templatePath, `getall.dto.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileGetallDtoTemplate, replacements);
    fs.writeFileSync(filesTree.getall, outTemplate);
    console.log('DONE: ', filesTree.getall);
    let fileUpdateDtoTemplate = fs.readFileSync(path.join(templatePath, `update.dto.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileUpdateDtoTemplate, replacements);
    fs.writeFileSync(filesTree.update, outTemplate);
    console.log('DONE: ', filesTree.update);
    let fileControllerSpectTemplate = fs.readFileSync(path.join(templatePath, `controller.spec.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileControllerSpectTemplate, replacements);
    fs.writeFileSync(filesTree.controllerSpec, outTemplate);
    console.log('DONE: ', filesTree.controllerSpec);
    let fileControllerTemplate = fs.readFileSync(path.join(templatePath, `controller.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileControllerTemplate, replacements);
    fs.writeFileSync(filesTree.controller, outTemplate);
    console.log('DONE: ', filesTree.controller);
    let fileModuleTemplate = fs.readFileSync(path.join(templatePath, `module.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileModuleTemplate, replacements);
    fs.writeFileSync(filesTree.module, outTemplate);
    console.log('DONE: ', filesTree.module);
    let fileServiceSpecTemplate = fs.readFileSync(path.join(templatePath, `service.spec.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileServiceSpecTemplate, replacements);
    fs.writeFileSync(filesTree.serviceSpec, outTemplate);
    console.log('DONE: ', filesTree.serviceSpec);
    let fileServiceTemplate = fs.readFileSync(path.join(templatePath, `service.template`), 'utf-8');
    outTemplate = reemplazaParametros(fileServiceTemplate, replacements);
    fs.writeFileSync(filesTree.service, outTemplate);
    console.log('DONE: ', filesTree.service);
}
catch (error) {
    console.error(error);
}
//# sourceMappingURL=feature.generator.js.map