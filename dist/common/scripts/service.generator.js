"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const model = process.argv[2];
const out_path = process.argv[4];
function reemplazaParametros(texto, parametros) {
    const texto_parametrizado = texto.replace(/\{\{(.*?)\}\}/g, function (match, token) {
        return parametros[token] ? parametros[token] : '';
    });
    return texto_parametrizado;
}
let project_path = __dirname.split('/');
project_path.pop();
project_path.pop();
project_path = project_path.join('/');
console.log('================project_path');
console.log(project_path);
console.log('================project_path');
let file_model = fs.readFileSync(path.join(project_path, `src/persistence/entities/${model}.ts`), 'utf-8');
let file_template = fs.readFileSync(path.join(__dirname, `source.template`), 'utf-8');
file_model = file_model.split('\n');
console.log('================file_model');
console.log(file_model);
console.log('================');
//# sourceMappingURL=service.generator.js.map