"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerMethod = void 0;
const http_1 = require("http");
const typeorm_1 = require("typeorm");
function LoggerMethod(target, propertyKey, descriptor) {
    const original = descriptor.value;
    descriptor.value = async function (...args) {
        this.logger.setContext(this.constructor.name);
        const params = {
            method: propertyKey,
            arguments: []
        };
        for (const a of args) {
            if (!(a instanceof http_1.ServerResponse) && !(a instanceof typeorm_1.EntityManager)) {
                params.arguments.push(JSON.stringify(a));
            }
        }
        this.logger.log('desde metodo', params);
        const result = original.call(this, ...args);
        return result;
    };
}
exports.LoggerMethod = LoggerMethod;
//# sourceMappingURL=LoggerMethod.js.map