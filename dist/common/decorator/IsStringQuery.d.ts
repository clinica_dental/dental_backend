import { ValidationOptions } from 'class-validator';
export declare function IsStringQuery(validationOptions?: ValidationOptions): (object: Object, propertyName: string) => void;
