"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsStringQuery = void 0;
const class_validator_1 = require("class-validator");
function IsStringQuery(validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            name: 'isCodesQuery',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value) {
                    return /\('(\w{1,})'(,'(\w{1,})')*\)/.test(value);
                },
            },
        });
    };
}
exports.IsStringQuery = IsStringQuery;
//# sourceMappingURL=IsStringQuery.js.map