import { ValidationOptions } from 'class-validator';
export declare function IsCodesQuery(validationOptions?: ValidationOptions): (object: Object, propertyName: string) => void;
