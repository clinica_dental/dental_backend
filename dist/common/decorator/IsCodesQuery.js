"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsCodesQuery = void 0;
const class_validator_1 = require("class-validator");
function IsCodesQuery(validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            name: 'isCodesQuery',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value) {
                    if (value && value.length > 0 && value != 'null') {
                        return /\((\d{1,})(,\d{1,})*\)/.test(value);
                    }
                    else {
                        return true;
                    }
                },
            },
        });
    };
}
exports.IsCodesQuery = IsCodesQuery;
//# sourceMappingURL=IsCodesQuery.js.map