declare function sendSuccess(res: any, status: number, code: any, message: string, datos?: any, codigo?: number): any;
declare function sendError(res: any, status: number, code: any, message: string, codigo?: number): any;
export { sendError, sendSuccess, };
