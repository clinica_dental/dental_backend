"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.throwError = void 0;
function throwError(code, error_message, codigo = 0) {
    const error = new Error(error_message || 'Error Desconocido');
    error.code = code;
    error.codigo = codigo;
    throw error;
}
exports.throwError = throwError;
//# sourceMappingURL=error.handling.js.map