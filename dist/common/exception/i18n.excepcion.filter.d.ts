import { ValidationError } from 'class-validator';
export declare class i18nExceptionFilter {
    constructor();
    i18nerrorFormatter(errors: ValidationError): ValidationError;
}
