import { ExceptionFilter, ArgumentsHost, HttpException } from '@nestjs/common';
export declare class HttpExceptionFilter implements ExceptionFilter {
    private result;
    private logger;
    catch(exception: HttpException, host: ArgumentsHost): void;
}
