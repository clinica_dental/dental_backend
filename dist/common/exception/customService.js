"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomService = void 0;
const error_handling_1 = require("./error.handling");
class CustomService {
    static verifyingDataResult(resutlData, extra_message = '') {
        try {
            if (resutlData == null || resutlData.length == 0)
                (0, error_handling_1.throwError)(400, `${extra_message} NO SE ENCONTRARON REGISTROS `);
            return resutlData;
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message || 'OCURRIO UN ERROR DURANTE LA CONSULTA');
        }
    }
}
exports.CustomService = CustomService;
//# sourceMappingURL=customService.js.map