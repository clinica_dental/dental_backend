"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendSuccess = exports.sendError = void 0;
function sendSuccess(res, status, code, message, datos, codigo) {
    if (['POST', 'PUT', 'PATCH', 'DELETE'].includes(res.req.method)) {
        datos = {
            err_mensaje: message,
            codigo: code
        };
    }
    const resultSuccess = {
        codigo: code,
        error_existente: 0,
        error_mensaje: message || 'SERVICIO PROCESO CON EXITO.',
        error_codigo: codigo || 2001,
        datos: datos || []
    };
    res.status(status).send(resultSuccess);
}
exports.sendSuccess = sendSuccess;
function sendError(res, status, code, message, codigo) {
    const resultSuccess = {
        codigo: code,
        error_existente: 1,
        error_mensaje: message || 'ERROR DURANTE EL PROCESO DEL SERVICIO.',
        error_codigo: codigo || 4001,
        datos: []
    };
    res.status(status).send(resultSuccess);
}
exports.sendError = sendError;
//# sourceMappingURL=sender.handling.js.map