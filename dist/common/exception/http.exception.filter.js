"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var HttpExceptionFilter_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const CustomLogger_1 = require("../util/CustomLogger");
let HttpExceptionFilter = HttpExceptionFilter_1 = class HttpExceptionFilter {
    constructor() {
        this.logger = new CustomLogger_1.Logger(HttpExceptionFilter_1.name);
    }
    catch(exception, host) {
        const messageException = exception.getResponse();
        const message = typeof messageException.message == 'string' ? messageException.message : messageException.message.join(', ');
        this.logger.debug(messageException.message);
        this.result = {
            codigo: 0,
            error_existente: 1,
            error_mensaje: message || messageException.errorMensaje || messageException || 'SERVICIO PROCESO CON EXITO.',
            error_codigo: 4001,
        };
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        response
            .status(common_1.HttpStatus.BAD_REQUEST)
            .send(this.result);
    }
};
HttpExceptionFilter = HttpExceptionFilter_1 = __decorate([
    (0, common_1.Catch)(common_1.HttpException)
], HttpExceptionFilter);
exports.HttpExceptionFilter = HttpExceptionFilter;
//# sourceMappingURL=http.exception.filter.js.map