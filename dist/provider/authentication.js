"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationProvider = void 0;
const CustomLogger_1 = require("../common/util/CustomLogger");
class AuthenticationProvider {
    constructor() {
        this.logger = new CustomLogger_1.Logger(AuthenticationProvider.name);
    }
}
exports.AuthenticationProvider = AuthenticationProvider;
//# sourceMappingURL=authentication.js.map