import "reflect-metadata";
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
export declare class TypeOrmConfigPostgres implements TypeOrmOptionsFactory {
    private _configService;
    constructor(_configService: ConfigService);
    createTypeOrmOptions(): TypeOrmModuleOptions;
}
export declare class TypeOrmConfigSqlServer implements TypeOrmOptionsFactory {
    private _configService;
    constructor(_configService: ConfigService);
    createTypeOrmOptions(): TypeOrmModuleOptions;
}
