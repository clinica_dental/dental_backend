"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const path = require("path");
const common_1 = require("@nestjs/common");
const global_middleware_1 = require("./common/middleware/global.middleware");
const external_middleware_1 = require("./common/middleware/external.middleware");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const schedule_1 = require("@nestjs/schedule");
const nestjs_i18n_1 = require("nestjs-i18n");
const database_1 = require("./provider/database");
const app_controller_1 = require("./app.controller");
const tasks_module_1 = require("./jobs/task-scheduler/tasks.module");
const authentication_1 = require("./provider/authentication");
const publicKey_1 = require("./authentication/publicKey");
const parametricas_module_1 = require("./feature/parametricas/parametricas.module");
const persona_module_1 = require("./feature/persona/persona.module");
const personal_odontologico_module_1 = require("./feature/personal-odontologico/personal-odontologico.module");
const paciente_module_1 = require("./feature/paciente/paciente.module");
const antecedente_module_1 = require("./feature/antecedente/antecedente.module");
const antecedentes_patologicos_module_1 = require("./feature/antecedentes-patologicos/antecedentes-patologicos.module");
const antecedentes_personales_module_1 = require("./feature/antecedentes-personales/antecedentes-personales.module");
const antecedentes_afecciones_module_1 = require("./feature/antecedentes-afecciones/antecedentes-afecciones.module");
const antecedentes_dentales_module_1 = require("./feature/antecedentes-dentales/antecedentes-dentales.module");
const usuario_module_1 = require("./feature/usuario/usuario.module");
const tratamiento_module_1 = require("./feature/tratamiento/tratamiento.module");
const tratamiento_plan_module_1 = require("./feature/tratamiento-plan/tratamiento-plan.module");
const reservas_module_1 = require("./feature/reservas/reservas.module");
const reportes_module_1 = require("./feature/reportes/reportes.module");
let AppModule = class AppModule {
    constructor() {
        new publicKey_1.PublicKey(new authentication_1.AuthenticationProvider());
    }
    configure(consumer) {
        consumer.apply(external_middleware_1.ExternalMiddleware).forRoutes('usuario/login');
        consumer.apply(global_middleware_1.GlobalMiddleware).exclude('usuario/login').forRoutes('*');
    }
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            nestjs_i18n_1.I18nModule.forRoot({
                fallbackLanguage: 'es',
                formatter: (template, ...args) => template,
                loaderOptions: {
                    path: path.join(__dirname, '/common/i18n/'),
                    watch: true,
                },
                resolvers: [
                    { use: nestjs_i18n_1.HeaderResolver, options: ['lang'] },
                    nestjs_i18n_1.AcceptLanguageResolver,
                ]
            }),
            config_1.ConfigModule.forRoot({ isGlobal: true, cache: true }),
            typeorm_1.TypeOrmModule.forRootAsync({ useClass: database_1.TypeOrmConfigPostgres, }),
            schedule_1.ScheduleModule.forRoot(),
            tasks_module_1.TasksModule,
            parametricas_module_1.ParametricasModule,
            persona_module_1.PersonaModule,
            personal_odontologico_module_1.PersonalOdontologicoModule,
            paciente_module_1.PacienteModule,
            antecedente_module_1.AntecedenteModule,
            antecedentes_patologicos_module_1.AntecedentesPatologicosModule,
            antecedentes_personales_module_1.AntecedentesPersonalesModule,
            antecedentes_afecciones_module_1.AntecedentesAfeccionesModule,
            antecedentes_dentales_module_1.AntecedentesDentalesModule,
            usuario_module_1.UsuarioModule,
            tratamiento_module_1.TratamientoModule,
            tratamiento_plan_module_1.TratamientoPlanModule,
            reservas_module_1.ReservasModule,
            reportes_module_1.ReportesModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [],
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map