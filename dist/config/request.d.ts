declare const options: {
    local: {
        jasper: {
            obtiene_reporte: {
                method: string;
                baseUrl: string;
                url: string;
                qs: {
                    j_username: string;
                    j_password: string;
                };
                headers: {
                    'Content-Type': string;
                    tracerId: string;
                };
                params: {
                    reporte: string;
                };
                json: boolean;
                timeout: number;
                proxy: boolean;
            };
        };
    };
    ambito: {
        metodo: {
            method: string;
            baseUrl: string;
            url: string;
            headers: {
                Authorization: string;
                'Content-Type': string;
                tracerId: string;
            };
            params: {};
            json: boolean;
            proxy: string;
            timeout: number;
        };
    };
};
export = options;
