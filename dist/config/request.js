'use strict';
const tokenBaseAutentication = process.env.TOKEN_AUTENTICACION;
const usr_base_jasper = process.env.USR_BASE_JASPER;
const pwd_base_jasper = process.env.PWD_BASE_JASPER;
const url_base_jasper = process.env.URL_JASPER;
const httpProxy = process.env.HTTP_PROXY;
const options = {
    local: {
        jasper: {
            obtiene_reporte: {
                method: 'GET',
                baseUrl: url_base_jasper,
                url: '/Reports/SGSIR_APOYO/:reporte',
                qs: {
                    j_username: usr_base_jasper,
                    j_password: pwd_base_jasper
                },
                headers: {
                    'Content-Type': 'application/json',
                    tracerId: ''
                },
                params: {
                    reporte: ''
                },
                json: true,
                timeout: 10000,
                proxy: false
            }
        }
    },
    ambito: {
        metodo: {
            method: 'GET',
            baseUrl: url_base_jasper,
            url: '/',
            headers: {
                Authorization: tokenBaseAutentication,
                'Content-Type': 'application/json',
                tracerId: ''
            },
            params: {},
            json: true,
            proxy: httpProxy,
            timeout: 10000
        }
    }
};
module.exports = options;
//# sourceMappingURL=request.js.map