const transport = {
    host: process.env.SMTP_SERVER,
    port: process.env.SMTP_PORT,
    tls: {
        rejectUnauthorized: false,
    },
    auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD,
    },
};
const options = {
    correo_default: {
        transport,
        message: {
            from: process.env.SMTP_USER,
            to: '',
            subject: 'MENSAJE DE CORREO DEFAULT',
            html: `<b>MENSAJE ENVIADO DE FORMA SATISFACTORIA.</b>
			`,
        },
        params: {},
    },
};
module.exports = {
    options,
};
//# sourceMappingURL=nodemailer.js.map