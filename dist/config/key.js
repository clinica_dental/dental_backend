"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publicKEY = exports.privateKEY = void 0;
const fs = require("fs");
const path = require("path");
const privateKEY = fs.readFileSync(path.join(__dirname, 'token.pem'), 'utf8');
exports.privateKEY = privateKEY;
const publicKEY = fs.readFileSync(path.join(__dirname, 'token.pub.pem'), 'utf8');
exports.publicKEY = publicKEY;
//# sourceMappingURL=key.js.map