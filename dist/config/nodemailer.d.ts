declare const transport: {
    host: string;
    port: string;
    tls: {
        rejectUnauthorized: boolean;
    };
    auth: {
        user: string;
        pass: string;
    };
};
declare const options: {
    correo_default: {
        transport: {
            host: string;
            port: string;
            tls: {
                rejectUnauthorized: boolean;
            };
            auth: {
                user: string;
                pass: string;
            };
        };
        message: {
            from: string;
            to: string;
            subject: string;
            html: string;
        };
        params: {};
    };
};
