import { SchedulerRegistry } from '@nestjs/schedule';
import { DataSource, EntityManager } from 'typeorm';
export declare class TasksService {
    private schedulerRegistry;
    private dataSource;
    private readonly logger;
    private queryRunner;
    private manager;
    constructor(schedulerRegistry: SchedulerRegistry, dataSource: DataSource);
    addCronJob(name: string, period: string, method: any): void;
    startCrons(manager: EntityManager): void;
    startSomeTask(): Promise<void>;
    startAnotherTask(): Promise<void>;
}
