"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var TasksService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.TasksService = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const typeorm_1 = require("typeorm");
const cron_1 = require("cron");
const CustomLogger_1 = require("../../common/util/CustomLogger");
let TasksService = TasksService_1 = class TasksService {
    constructor(schedulerRegistry, dataSource) {
        this.schedulerRegistry = schedulerRegistry;
        this.dataSource = dataSource;
        this.logger = new CustomLogger_1.Logger(TasksService_1.name);
        this.queryRunner = this.dataSource.createQueryRunner();
        this.manager = this.queryRunner.manager;
        this.startCrons(this.manager);
    }
    addCronJob(name, period, method) {
        const job = new cron_1.CronJob(period, method);
        this.schedulerRegistry.addCronJob(name, job);
        job.start();
        this.logger.warn(`Job <${name}> added for period <${period}> successfully!`);
    }
    startCrons(manager) {
        const crons = process.env.CRON_ENABLES.split(' ');
        crons.map(m => {
            let cronName = m.toUpperCase();
            let periord = process.env[`CRON_EXECUTE_${cronName}`] || process.env.CRON_EXECUTE_DEFAULT;
            switch (cronName) {
                case 'SOME':
                    this.addCronJob(cronName, periord, () => { this.startSomeTask(); });
                    break;
                case 'ANOTHER':
                    this.addCronJob(cronName, periord, () => { this.startAnotherTask(); });
                    break;
            }
        });
    }
    async startSomeTask() {
        try {
            await this.queryRunner.startTransaction();
            this.logger.debug('RUN SOME CRON TASK SAMPLE.');
            await this.queryRunner.commitTransaction();
        }
        catch (error) {
            if (this.queryRunner.isTransactionActive)
                await this.queryRunner.rollbackTransaction();
        }
    }
    async startAnotherTask() {
        try {
            await this.queryRunner.startTransaction();
            this.logger.debug('RUN ANOTHER CRON TASK SAMPLE.');
            await this.queryRunner.commitTransaction();
        }
        catch (error) {
            if (this.queryRunner.isTransactionActive)
                await this.queryRunner.rollbackTransaction();
        }
    }
};
TasksService = TasksService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [schedule_1.SchedulerRegistry,
        typeorm_1.DataSource])
], TasksService);
exports.TasksService = TasksService;
//# sourceMappingURL=tasks.service.js.map