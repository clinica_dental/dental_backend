import { Estado } from "./Estado";
import { RolMenu } from "./RolMenu";
import { UsuarioRol } from "./UsuarioRol";
export declare class Rol {
    rol_codigo: number;
    rol_nombre: string;
    rol_descripcion: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    rol_estado: Estado;
    rol_menus: RolMenu[];
    usuario_rols: UsuarioRol[];
}
