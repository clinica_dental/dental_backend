"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TratamientoPlan = void 0;
const typeorm_1 = require("typeorm");
const Tratamiento_1 = require("./Tratamiento");
const Estado_1 = require("./Estado");
let TratamientoPlan = class TratamientoPlan {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "trapla_codigo" }),
    __metadata("design:type", Number)
], TratamientoPlan.prototype, "trapla_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("date", { name: "trapla_fecha", default: () => "CURRENT_DATE" }),
    __metadata("design:type", String)
], TratamientoPlan.prototype, "trapla_fecha", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "trapla_pieza" }),
    __metadata("design:type", String)
], TratamientoPlan.prototype, "trapla_pieza", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "trapla_diagnostico", nullable: true }),
    __metadata("design:type", String)
], TratamientoPlan.prototype, "trapla_diagnostico", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "trapla_tratamiento", nullable: true }),
    __metadata("design:type", String)
], TratamientoPlan.prototype, "trapla_tratamiento", void 0);
__decorate([
    (0, typeorm_1.Column)("numeric", {
        name: "trapla_acuenta",
        precision: 10,
        scale: 2,
        default: () => "0.0",
    }),
    __metadata("design:type", String)
], TratamientoPlan.prototype, "trapla_acuenta", void 0);
__decorate([
    (0, typeorm_1.Column)("numeric", { name: "trapla_saldo", precision: 10, scale: 2 }),
    __metadata("design:type", String)
], TratamientoPlan.prototype, "trapla_saldo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "trapla_observaciones", nullable: true }),
    __metadata("design:type", String)
], TratamientoPlan.prototype, "trapla_observaciones", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], TratamientoPlan.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], TratamientoPlan.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], TratamientoPlan.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], TratamientoPlan.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TratamientoPlan.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TratamientoPlan.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Tratamiento_1.Tratamiento, (tratamiento) => tratamiento.tratamiento_plans),
    (0, typeorm_1.JoinColumn)([{ name: "tra_codigo", referencedColumnName: "tra_codigo" }]),
    __metadata("design:type", Tratamiento_1.Tratamiento)
], TratamientoPlan.prototype, "tra_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.tratamiento_plans),
    (0, typeorm_1.JoinColumn)([{ name: "trapla_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], TratamientoPlan.prototype, "trapla_estado", void 0);
TratamientoPlan = __decorate([
    (0, typeorm_1.Index)("tratamiento_plan_pk", ["trapla_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("tratamiento_plan", { schema: "registro" })
], TratamientoPlan);
exports.TratamientoPlan = TratamientoPlan;
//# sourceMappingURL=TratamientoPlan.js.map