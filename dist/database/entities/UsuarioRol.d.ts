import { Rol } from "./Rol";
import { Usuario } from "./Usuario";
import { Estado } from "./Estado";
export declare class UsuarioRol {
    usu_codigo: number;
    rol_codigo: number;
    usurol_fechaexpiracion: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    rol_codigo2: Rol;
    usu_codigo2: Usuario;
    usurol_estado: Estado;
}
