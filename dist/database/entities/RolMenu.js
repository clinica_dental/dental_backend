"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolMenu = void 0;
const typeorm_1 = require("typeorm");
const Menu_1 = require("./Menu");
const Rol_1 = require("./Rol");
const Estado_1 = require("./Estado");
let RolMenu = class RolMenu {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "rol_codigo" }),
    __metadata("design:type", Number)
], RolMenu.prototype, "rol_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "men_codigo" }),
    __metadata("design:type", Number)
], RolMenu.prototype, "men_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], RolMenu.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], RolMenu.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], RolMenu.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], RolMenu.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], RolMenu.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], RolMenu.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Menu_1.Menu, (menu) => menu.rol_menus),
    (0, typeorm_1.JoinColumn)([{ name: "men_codigo", referencedColumnName: "men_codigo" }]),
    __metadata("design:type", Menu_1.Menu)
], RolMenu.prototype, "men_codigo2", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Rol_1.Rol, (rol) => rol.rol_menus),
    (0, typeorm_1.JoinColumn)([{ name: "rol_codigo", referencedColumnName: "rol_codigo" }]),
    __metadata("design:type", Rol_1.Rol)
], RolMenu.prototype, "rol_codigo2", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.rol_menus),
    (0, typeorm_1.JoinColumn)([{ name: "rolmen_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], RolMenu.prototype, "rolmen_estado", void 0);
RolMenu = __decorate([
    (0, typeorm_1.Index)("rol_menu_pk", ["men_codigo", "rol_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("rol_menu", { schema: "autenticacion" })
], RolMenu);
exports.RolMenu = RolMenu;
//# sourceMappingURL=RolMenu.js.map