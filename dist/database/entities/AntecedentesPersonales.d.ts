import { Antecedente } from "./Antecedente";
import { Estado } from "./Estado";
export declare class AntecedentesPersonales {
    antper_codigo: number;
    antper_hopitalizacion: string | null;
    antper_atencion: string | null;
    antper_alergias: string | null;
    antper_infecciosos: string | null;
    antper_alteraciones: string | null;
    antper_tratamiento: string | null;
    antper_medicacion: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    ant_codigo: Antecedente;
    antper_estado: Estado;
}
