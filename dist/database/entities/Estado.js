"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Estado = void 0;
const typeorm_1 = require("typeorm");
const Antecedente_1 = require("./Antecedente");
const AntecedentesAfecciones_1 = require("./AntecedentesAfecciones");
const AntecedentesDentales_1 = require("./AntecedentesDentales");
const AntecedentesPatologicos_1 = require("./AntecedentesPatologicos");
const AntecedentesPersonales_1 = require("./AntecedentesPersonales");
const Especialidad_1 = require("./Especialidad");
const EstadoCivil_1 = require("./EstadoCivil");
const Genero_1 = require("./Genero");
const Menu_1 = require("./Menu");
const Paciente_1 = require("./Paciente");
const Persona_1 = require("./Persona");
const PersonalOdontologico_1 = require("./PersonalOdontologico");
const Reservas_1 = require("./Reservas");
const Rol_1 = require("./Rol");
const RolMenu_1 = require("./RolMenu");
const TipoAfeccion_1 = require("./TipoAfeccion");
const TipoDocumento_1 = require("./TipoDocumento");
const TipoTratamiento_1 = require("./TipoTratamiento");
const Tratamiento_1 = require("./Tratamiento");
const TratamientoPlan_1 = require("./TratamientoPlan");
const Usuario_1 = require("./Usuario");
const UsuarioRol_1 = require("./UsuarioRol");
let Estado = class Estado {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "est_codigo" }),
    __metadata("design:type", Number)
], Estado.prototype, "est_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "est_nombre" }),
    __metadata("design:type", String)
], Estado.prototype, "est_nombre", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Estado.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Estado.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Antecedente_1.Antecedente, (antecedente) => antecedente.ant_estado),
    __metadata("design:type", Array)
], Estado.prototype, "antecedentes", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesAfecciones_1.AntecedentesAfecciones, (antecedentes_afecciones) => antecedentes_afecciones.antafe_estado),
    __metadata("design:type", Array)
], Estado.prototype, "antecedentes_afecciones", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesDentales_1.AntecedentesDentales, (antecedentes_dentales) => antecedentes_dentales.antden_estado),
    __metadata("design:type", Array)
], Estado.prototype, "antecedentes_dentales", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesPatologicos_1.AntecedentesPatologicos, (antecedentes_patologicos) => antecedentes_patologicos.antpat_estado),
    __metadata("design:type", Array)
], Estado.prototype, "antecedentes_patologicos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesPersonales_1.AntecedentesPersonales, (antecedentes_personales) => antecedentes_personales.antper_estado),
    __metadata("design:type", Array)
], Estado.prototype, "antecedentes_personales", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Especialidad_1.Especialidad, (especialidad) => especialidad.esp_estado),
    __metadata("design:type", Array)
], Estado.prototype, "especialidads", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => EstadoCivil_1.EstadoCivil, (estado_civil) => estado_civil.estciv_estado),
    __metadata("design:type", Array)
], Estado.prototype, "estado_civils", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Genero_1.Genero, (genero) => genero.gen_estado),
    __metadata("design:type", Array)
], Estado.prototype, "generos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Menu_1.Menu, (menu) => menu.men_estado),
    __metadata("design:type", Array)
], Estado.prototype, "menus", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Paciente_1.Paciente, (paciente) => paciente.pac_estado),
    __metadata("design:type", Array)
], Estado.prototype, "pacientes", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Persona_1.Persona, (persona) => persona.per_estado),
    __metadata("design:type", Array)
], Estado.prototype, "personas", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => PersonalOdontologico_1.PersonalOdontologico, (personal_odontologico) => personal_odontologico.perodo_estado),
    __metadata("design:type", Array)
], Estado.prototype, "personal_odontologicos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Reservas_1.Reservas, (reservas) => reservas.res_estado),
    __metadata("design:type", Array)
], Estado.prototype, "reservas", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Rol_1.Rol, (rol) => rol.rol_estado),
    __metadata("design:type", Array)
], Estado.prototype, "rols", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => RolMenu_1.RolMenu, (rol_menu) => rol_menu.rolmen_estado),
    __metadata("design:type", Array)
], Estado.prototype, "rol_menus", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => TipoAfeccion_1.TipoAfeccion, (tipo_afeccion) => tipo_afeccion.tipafe_estado),
    __metadata("design:type", Array)
], Estado.prototype, "tipo_afeccions", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => TipoDocumento_1.TipoDocumento, (tipo_documento) => tipo_documento.tipdoc_estado),
    __metadata("design:type", Array)
], Estado.prototype, "tipo_documentos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => TipoTratamiento_1.TipoTratamiento, (tipo_tratamiento) => tipo_tratamiento.tiptra_estado),
    __metadata("design:type", Array)
], Estado.prototype, "tipo_tratamientos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Tratamiento_1.Tratamiento, (tratamiento) => tratamiento.tra_estado),
    __metadata("design:type", Array)
], Estado.prototype, "tratamientos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => TratamientoPlan_1.TratamientoPlan, (tratamiento_plan) => tratamiento_plan.trapla_estado),
    __metadata("design:type", Array)
], Estado.prototype, "tratamiento_plans", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Usuario_1.Usuario, (usuario) => usuario.usu_estado),
    __metadata("design:type", Array)
], Estado.prototype, "usuarios", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => UsuarioRol_1.UsuarioRol, (usuario_rol) => usuario_rol.usurol_estado),
    __metadata("design:type", Array)
], Estado.prototype, "usuario_rols", void 0);
Estado = __decorate([
    (0, typeorm_1.Index)("estado_pk", ["est_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("estado", { schema: "parametricas" })
], Estado);
exports.Estado = Estado;
//# sourceMappingURL=Estado.js.map