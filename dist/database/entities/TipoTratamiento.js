"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoTratamiento = void 0;
const typeorm_1 = require("typeorm");
const Estado_1 = require("./Estado");
const Tratamiento_1 = require("./Tratamiento");
let TipoTratamiento = class TipoTratamiento {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "tiptra_codigo" }),
    __metadata("design:type", Number)
], TipoTratamiento.prototype, "tiptra_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "tiptra_nombre" }),
    __metadata("design:type", String)
], TipoTratamiento.prototype, "tiptra_nombre", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "tiptra_descripcion", nullable: true }),
    __metadata("design:type", String)
], TipoTratamiento.prototype, "tiptra_descripcion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], TipoTratamiento.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], TipoTratamiento.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], TipoTratamiento.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], TipoTratamiento.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TipoTratamiento.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TipoTratamiento.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.tipo_tratamientos),
    (0, typeorm_1.JoinColumn)([{ name: "tiptra_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], TipoTratamiento.prototype, "tiptra_estado", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Tratamiento_1.Tratamiento, (tratamiento) => tratamiento.tiptra_codigo),
    __metadata("design:type", Array)
], TipoTratamiento.prototype, "tratamientos", void 0);
TipoTratamiento = __decorate([
    (0, typeorm_1.Index)("tipo_tratamiento_pk", ["tiptra_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("tipo_tratamiento", { schema: "parametricas" })
], TipoTratamiento);
exports.TipoTratamiento = TipoTratamiento;
//# sourceMappingURL=TipoTratamiento.js.map