import { Paciente } from "./Paciente";
import { PersonalOdontologico } from "./PersonalOdontologico";
import { TipoTratamiento } from "./TipoTratamiento";
import { Estado } from "./Estado";
import { TratamientoPlan } from "./TratamientoPlan";
export declare class Tratamiento {
    tra_codigo: number;
    tra_total: string;
    tra_observaciones: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    pac_codigo: Paciente;
    perodo_codigo: PersonalOdontologico;
    tiptra_codigo: TipoTratamiento;
    tra_estado: Estado;
    tratamiento_plans: TratamientoPlan[];
}
