"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Antecedente = void 0;
const typeorm_1 = require("typeorm");
const Estado_1 = require("./Estado");
const Paciente_1 = require("./Paciente");
const AntecedentesAfecciones_1 = require("./AntecedentesAfecciones");
const AntecedentesDentales_1 = require("./AntecedentesDentales");
const AntecedentesPatologicos_1 = require("./AntecedentesPatologicos");
const AntecedentesPersonales_1 = require("./AntecedentesPersonales");
let Antecedente = class Antecedente {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "ant_codigo" }),
    __metadata("design:type", Number)
], Antecedente.prototype, "ant_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Antecedente.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Antecedente.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Antecedente.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Antecedente.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Antecedente.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Antecedente.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.antecedentes),
    (0, typeorm_1.JoinColumn)([{ name: "ant_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Antecedente.prototype, "ant_estado", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Paciente_1.Paciente, (paciente) => paciente.antecedentes),
    (0, typeorm_1.JoinColumn)([{ name: "pac_codigo", referencedColumnName: "pac_codigo" }]),
    __metadata("design:type", Paciente_1.Paciente)
], Antecedente.prototype, "pac_codigo", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesAfecciones_1.AntecedentesAfecciones, (antecedentes_afecciones) => antecedentes_afecciones.ant_codigo),
    __metadata("design:type", Array)
], Antecedente.prototype, "antecedentes_afecciones", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => AntecedentesDentales_1.AntecedentesDentales, (antecedentes_dentales) => antecedentes_dentales.ant_codigo2),
    __metadata("design:type", AntecedentesDentales_1.AntecedentesDentales)
], Antecedente.prototype, "antecedentes_dentales", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesPatologicos_1.AntecedentesPatologicos, (antecedentes_patologicos) => antecedentes_patologicos.ant_codigo),
    __metadata("design:type", Array)
], Antecedente.prototype, "antecedentes_patologicos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesPersonales_1.AntecedentesPersonales, (antecedentes_personales) => antecedentes_personales.ant_codigo),
    __metadata("design:type", Array)
], Antecedente.prototype, "antecedentes_personales", void 0);
Antecedente = __decorate([
    (0, typeorm_1.Index)("antecedente_pk", ["ant_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("antecedente", { schema: "registro" })
], Antecedente);
exports.Antecedente = Antecedente;
//# sourceMappingURL=Antecedente.js.map