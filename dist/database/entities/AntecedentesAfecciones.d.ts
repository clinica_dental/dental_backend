import { Antecedente } from "./Antecedente";
import { Estado } from "./Estado";
import { TipoAfeccion } from "./TipoAfeccion";
export declare class AntecedentesAfecciones {
    antafe_codigo: number;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    ant_codigo: Antecedente;
    antafe_estado: Estado;
    tipafe_codigo: TipoAfeccion;
}
