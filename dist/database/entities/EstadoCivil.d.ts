import { Estado } from "./Estado";
import { Paciente } from "./Paciente";
export declare class EstadoCivil {
    estciv_codigo: number;
    estciv_nombre: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    estciv_estado: Estado;
    pacientes: Paciente[];
}
