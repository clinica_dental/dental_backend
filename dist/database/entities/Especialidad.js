"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Especialidad = void 0;
const typeorm_1 = require("typeorm");
const Estado_1 = require("./Estado");
const PersonalOdontologico_1 = require("./PersonalOdontologico");
let Especialidad = class Especialidad {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "esp_codigo" }),
    __metadata("design:type", Number)
], Especialidad.prototype, "esp_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "esp_nombre" }),
    __metadata("design:type", String)
], Especialidad.prototype, "esp_nombre", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Especialidad.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Especialidad.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Especialidad.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Especialidad.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Especialidad.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Especialidad.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.especialidads),
    (0, typeorm_1.JoinColumn)([{ name: "esp_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Especialidad.prototype, "esp_estado", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => PersonalOdontologico_1.PersonalOdontologico, (personal_odontologico) => personal_odontologico.esp_codigo),
    __metadata("design:type", Array)
], Especialidad.prototype, "personal_odontologicos", void 0);
Especialidad = __decorate([
    (0, typeorm_1.Index)("especialidad_pk", ["esp_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("especialidad", { schema: "parametricas" })
], Especialidad);
exports.Especialidad = Especialidad;
//# sourceMappingURL=Especialidad.js.map