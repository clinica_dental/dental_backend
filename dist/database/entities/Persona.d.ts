import { Paciente } from "./Paciente";
import { Genero } from "./Genero";
import { Estado } from "./Estado";
import { TipoDocumento } from "./TipoDocumento";
import { PersonalOdontologico } from "./PersonalOdontologico";
import { Usuario } from "./Usuario";
export declare class Persona {
    per_codigo: number;
    per_docidentidad: string;
    per_nombres: string;
    per_primer_apellido: string;
    per_segundo_apellido: string | null;
    per_fecha_nacimiento: string;
    per_celular: string | null;
    per_correo: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    pacientes: Paciente[];
    gen_codigo: Genero;
    per_estado: Estado;
    tipdoc_codigo: TipoDocumento;
    personal_odontologicos: PersonalOdontologico[];
    usuarios: Usuario[];
}
