"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesAfecciones = void 0;
const typeorm_1 = require("typeorm");
const Antecedente_1 = require("./Antecedente");
const Estado_1 = require("./Estado");
const TipoAfeccion_1 = require("./TipoAfeccion");
let AntecedentesAfecciones = class AntecedentesAfecciones {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "antafe_codigo" }),
    __metadata("design:type", Number)
], AntecedentesAfecciones.prototype, "antafe_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], AntecedentesAfecciones.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], AntecedentesAfecciones.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], AntecedentesAfecciones.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], AntecedentesAfecciones.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], AntecedentesAfecciones.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], AntecedentesAfecciones.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Antecedente_1.Antecedente, (antecedente) => antecedente.antecedentes_afecciones),
    (0, typeorm_1.JoinColumn)([{ name: "ant_codigo", referencedColumnName: "ant_codigo" }]),
    __metadata("design:type", Antecedente_1.Antecedente)
], AntecedentesAfecciones.prototype, "ant_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.antecedentes_afecciones),
    (0, typeorm_1.JoinColumn)([{ name: "antafe_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], AntecedentesAfecciones.prototype, "antafe_estado", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => TipoAfeccion_1.TipoAfeccion, (tipo_afeccion) => tipo_afeccion.antecedentes_afecciones),
    (0, typeorm_1.JoinColumn)([
        { name: "tipafe_codigo", referencedColumnName: "tipafe_codigo" },
    ]),
    __metadata("design:type", TipoAfeccion_1.TipoAfeccion)
], AntecedentesAfecciones.prototype, "tipafe_codigo", void 0);
AntecedentesAfecciones = __decorate([
    (0, typeorm_1.Index)("antecedentes_afecciones_pk", ["antafe_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("antecedentes_afecciones", { schema: "registro" })
], AntecedentesAfecciones);
exports.AntecedentesAfecciones = AntecedentesAfecciones;
//# sourceMappingURL=AntecedentesAfecciones.js.map