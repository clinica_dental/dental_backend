import { Persona } from "./Persona";
import { Estado } from "./Estado";
export declare class TipoDocumento {
    tipdoc_codigo: number;
    tipdoc_nombre: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    personas: Persona[];
    tipdoc_estado: Estado;
}
