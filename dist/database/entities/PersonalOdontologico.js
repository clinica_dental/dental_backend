"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonalOdontologico = void 0;
const typeorm_1 = require("typeorm");
const Especialidad_1 = require("./Especialidad");
const Persona_1 = require("./Persona");
const Estado_1 = require("./Estado");
const Reservas_1 = require("./Reservas");
const Tratamiento_1 = require("./Tratamiento");
let PersonalOdontologico = class PersonalOdontologico {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "perodo_codigo" }),
    __metadata("design:type", Number)
], PersonalOdontologico.prototype, "perodo_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "perodo_turno" }),
    __metadata("design:type", String)
], PersonalOdontologico.prototype, "perodo_turno", void 0);
__decorate([
    (0, typeorm_1.Column)("time without time zone", { name: "perodo_hora_inicio" }),
    __metadata("design:type", String)
], PersonalOdontologico.prototype, "perodo_hora_inicio", void 0);
__decorate([
    (0, typeorm_1.Column)("time without time zone", { name: "perodo_hora_final" }),
    __metadata("design:type", String)
], PersonalOdontologico.prototype, "perodo_hora_final", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], PersonalOdontologico.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], PersonalOdontologico.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], PersonalOdontologico.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], PersonalOdontologico.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], PersonalOdontologico.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], PersonalOdontologico.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Especialidad_1.Especialidad, (especialidad) => especialidad.personal_odontologicos),
    (0, typeorm_1.JoinColumn)([{ name: "esp_codigo", referencedColumnName: "esp_codigo" }]),
    __metadata("design:type", Especialidad_1.Especialidad)
], PersonalOdontologico.prototype, "esp_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Persona_1.Persona, (persona) => persona.personal_odontologicos),
    (0, typeorm_1.JoinColumn)([{ name: "per_codigo", referencedColumnName: "per_codigo" }]),
    __metadata("design:type", Persona_1.Persona)
], PersonalOdontologico.prototype, "per_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.personal_odontologicos),
    (0, typeorm_1.JoinColumn)([{ name: "perodo_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], PersonalOdontologico.prototype, "perodo_estado", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Reservas_1.Reservas, (reservas) => reservas.perodo_codigo),
    __metadata("design:type", Array)
], PersonalOdontologico.prototype, "reservas", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Tratamiento_1.Tratamiento, (tratamiento) => tratamiento.perodo_codigo),
    __metadata("design:type", Array)
], PersonalOdontologico.prototype, "tratamientos", void 0);
PersonalOdontologico = __decorate([
    (0, typeorm_1.Index)("personal_odontologico_pk", ["perodo_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("personal_odontologico", { schema: "registro" })
], PersonalOdontologico);
exports.PersonalOdontologico = PersonalOdontologico;
//# sourceMappingURL=PersonalOdontologico.js.map