import { Estado } from "./Estado";
import { Persona } from "./Persona";
export declare class Genero {
    gen_codigo: number;
    gen_nombre: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    gen_estado: Estado;
    personas: Persona[];
}
