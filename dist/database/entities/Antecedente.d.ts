import { Estado } from "./Estado";
import { Paciente } from "./Paciente";
import { AntecedentesAfecciones } from "./AntecedentesAfecciones";
import { AntecedentesDentales } from "./AntecedentesDentales";
import { AntecedentesPatologicos } from "./AntecedentesPatologicos";
import { AntecedentesPersonales } from "./AntecedentesPersonales";
export declare class Antecedente {
    ant_codigo: number;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    ant_estado: Estado;
    pac_codigo: Paciente;
    antecedentes_afecciones: AntecedentesAfecciones[];
    antecedentes_dentales: AntecedentesDentales;
    antecedentes_patologicos: AntecedentesPatologicos[];
    antecedentes_personales: AntecedentesPersonales[];
}
