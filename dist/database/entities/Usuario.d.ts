import { Persona } from "./Persona";
import { Estado } from "./Estado";
import { UsuarioRol } from "./UsuarioRol";
export declare class Usuario {
    usu_codigo: number;
    usu_usuario: string;
    usu_contrasenia: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    per_codigo: Persona;
    usu_estado: Estado;
    usuario_rols: UsuarioRol[];
}
