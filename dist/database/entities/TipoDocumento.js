"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoDocumento = void 0;
const typeorm_1 = require("typeorm");
const Persona_1 = require("./Persona");
const Estado_1 = require("./Estado");
let TipoDocumento = class TipoDocumento {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "tipdoc_codigo" }),
    __metadata("design:type", Number)
], TipoDocumento.prototype, "tipdoc_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "tipdoc_nombre" }),
    __metadata("design:type", String)
], TipoDocumento.prototype, "tipdoc_nombre", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], TipoDocumento.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], TipoDocumento.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], TipoDocumento.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], TipoDocumento.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TipoDocumento.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TipoDocumento.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Persona_1.Persona, (persona) => persona.tipdoc_codigo),
    __metadata("design:type", Array)
], TipoDocumento.prototype, "personas", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.tipo_documentos),
    (0, typeorm_1.JoinColumn)([{ name: "tipdoc_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], TipoDocumento.prototype, "tipdoc_estado", void 0);
TipoDocumento = __decorate([
    (0, typeorm_1.Index)("tipo_documento_pk", ["tipdoc_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("tipo_documento", { schema: "parametricas" })
], TipoDocumento);
exports.TipoDocumento = TipoDocumento;
//# sourceMappingURL=TipoDocumento.js.map