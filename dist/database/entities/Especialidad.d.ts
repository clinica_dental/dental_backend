import { Estado } from "./Estado";
import { PersonalOdontologico } from "./PersonalOdontologico";
export declare class Especialidad {
    esp_codigo: number;
    esp_nombre: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    esp_estado: Estado;
    personal_odontologicos: PersonalOdontologico[];
}
