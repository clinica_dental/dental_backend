import { Menu } from "./Menu";
import { Rol } from "./Rol";
import { Estado } from "./Estado";
export declare class RolMenu {
    rol_codigo: number;
    men_codigo: number;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    men_codigo2: Menu;
    rol_codigo2: Rol;
    rolmen_estado: Estado;
}
