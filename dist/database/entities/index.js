"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.entities = void 0;
const Rol_1 = require("./Rol");
const Menu_1 = require("./Menu");
const RolMenu_1 = require("./RolMenu");
const Usuario_1 = require("./Usuario");
const UsuarioRol_1 = require("./UsuarioRol");
const Especialidad_1 = require("./Especialidad");
const Estado_1 = require("./Estado");
const Persona_1 = require("./Persona");
const TipoDocumento_1 = require("./TipoDocumento");
const Genero_1 = require("./Genero");
const Paciente_1 = require("./Paciente");
const EstadoCivil_1 = require("./EstadoCivil");
const Antecedente_1 = require("./Antecedente");
const AntecedentesPatologicos_1 = require("./AntecedentesPatologicos");
const AntecedentesPersonales_1 = require("./AntecedentesPersonales");
const AntecedentesAfecciones_1 = require("./AntecedentesAfecciones");
const TipoAfeccion_1 = require("./TipoAfeccion");
const AntecedentesDentales_1 = require("./AntecedentesDentales");
const Tratamiento_1 = require("./Tratamiento");
const TipoTratamiento_1 = require("./TipoTratamiento");
const TratamientoPlan_1 = require("./TratamientoPlan");
const Reservas_1 = require("./Reservas");
const PersonalOdontologico_1 = require("./PersonalOdontologico");
const entities = [
    Rol_1.Rol,
    Menu_1.Menu,
    RolMenu_1.RolMenu,
    Usuario_1.Usuario,
    UsuarioRol_1.UsuarioRol,
    Especialidad_1.Especialidad,
    Estado_1.Estado,
    Persona_1.Persona,
    TipoDocumento_1.TipoDocumento,
    Genero_1.Genero,
    Paciente_1.Paciente,
    EstadoCivil_1.EstadoCivil,
    Antecedente_1.Antecedente,
    AntecedentesPatologicos_1.AntecedentesPatologicos,
    AntecedentesPersonales_1.AntecedentesPersonales,
    AntecedentesAfecciones_1.AntecedentesAfecciones,
    TipoAfeccion_1.TipoAfeccion,
    AntecedentesDentales_1.AntecedentesDentales,
    Tratamiento_1.Tratamiento,
    TipoTratamiento_1.TipoTratamiento,
    TratamientoPlan_1.TratamientoPlan,
    Reservas_1.Reservas,
    PersonalOdontologico_1.PersonalOdontologico,
];
exports.entities = entities;
//# sourceMappingURL=index.js.map