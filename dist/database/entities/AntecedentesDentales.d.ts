import { Antecedente } from "./Antecedente";
import { Estado } from "./Estado";
export declare class AntecedentesDentales {
    ant_codigo: number;
    antden_higiene: string | null;
    antden_respiracion: string | null;
    antden_onicofagia: string | null;
    antden_queilifagia: string | null;
    antden_bruxismo: string | null;
    antden_cepillado: string | null;
    antden_diagnostico: string | null;
    antden_ultimo_tratamiento: string | null;
    antden_observaciones: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    ant_codigo2: Antecedente;
    antden_estado: Estado;
}
