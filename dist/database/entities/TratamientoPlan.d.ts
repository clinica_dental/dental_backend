import { Tratamiento } from "./Tratamiento";
import { Estado } from "./Estado";
export declare class TratamientoPlan {
    trapla_codigo: number;
    trapla_fecha: string;
    trapla_pieza: string;
    trapla_diagnostico: string | null;
    trapla_tratamiento: string | null;
    trapla_acuenta: string;
    trapla_saldo: string;
    trapla_observaciones: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    tra_codigo: Tratamiento;
    trapla_estado: Estado;
}
