"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsuarioRol = void 0;
const typeorm_1 = require("typeorm");
const Rol_1 = require("./Rol");
const Usuario_1 = require("./Usuario");
const Estado_1 = require("./Estado");
let UsuarioRol = class UsuarioRol {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "usu_codigo" }),
    __metadata("design:type", Number)
], UsuarioRol.prototype, "usu_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "rol_codigo" }),
    __metadata("design:type", Number)
], UsuarioRol.prototype, "rol_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("date", { name: "usurol_fechaexpiracion" }),
    __metadata("design:type", String)
], UsuarioRol.prototype, "usurol_fechaexpiracion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], UsuarioRol.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], UsuarioRol.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], UsuarioRol.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], UsuarioRol.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], UsuarioRol.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], UsuarioRol.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Rol_1.Rol, (rol) => rol.usuario_rols),
    (0, typeorm_1.JoinColumn)([{ name: "rol_codigo", referencedColumnName: "rol_codigo" }]),
    __metadata("design:type", Rol_1.Rol)
], UsuarioRol.prototype, "rol_codigo2", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Usuario_1.Usuario, (usuario) => usuario.usuario_rols),
    (0, typeorm_1.JoinColumn)([{ name: "usu_codigo", referencedColumnName: "usu_codigo" }]),
    __metadata("design:type", Usuario_1.Usuario)
], UsuarioRol.prototype, "usu_codigo2", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.usuario_rols),
    (0, typeorm_1.JoinColumn)([{ name: "usurol_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], UsuarioRol.prototype, "usurol_estado", void 0);
UsuarioRol = __decorate([
    (0, typeorm_1.Index)("usuario_rol_pk", ["rol_codigo", "usu_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("usuario_rol", { schema: "autenticacion" })
], UsuarioRol);
exports.UsuarioRol = UsuarioRol;
//# sourceMappingURL=UsuarioRol.js.map