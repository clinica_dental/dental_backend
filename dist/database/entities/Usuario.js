"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Usuario = void 0;
const typeorm_1 = require("typeorm");
const Persona_1 = require("./Persona");
const Estado_1 = require("./Estado");
const UsuarioRol_1 = require("./UsuarioRol");
let Usuario = class Usuario {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "usu_codigo" }),
    __metadata("design:type", Number)
], Usuario.prototype, "usu_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "usu_usuario" }),
    __metadata("design:type", String)
], Usuario.prototype, "usu_usuario", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "usu_contrasenia" }),
    __metadata("design:type", String)
], Usuario.prototype, "usu_contrasenia", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Usuario.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Usuario.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Usuario.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Usuario.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Usuario.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Usuario.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Persona_1.Persona, (persona) => persona.usuarios),
    (0, typeorm_1.JoinColumn)([{ name: "per_codigo", referencedColumnName: "per_codigo" }]),
    __metadata("design:type", Persona_1.Persona)
], Usuario.prototype, "per_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.usuarios),
    (0, typeorm_1.JoinColumn)([{ name: "usu_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Usuario.prototype, "usu_estado", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => UsuarioRol_1.UsuarioRol, (usuario_rol) => usuario_rol.usu_codigo2),
    __metadata("design:type", Array)
], Usuario.prototype, "usuario_rols", void 0);
Usuario = __decorate([
    (0, typeorm_1.Index)("usuario_pk", ["usu_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("usuario", { schema: "autenticacion" })
], Usuario);
exports.Usuario = Usuario;
//# sourceMappingURL=Usuario.js.map