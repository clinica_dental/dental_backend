import { Antecedente } from "./Antecedente";
import { EstadoCivil } from "./EstadoCivil";
import { Estado } from "./Estado";
import { Persona } from "./Persona";
import { Reservas } from "./Reservas";
import { Tratamiento } from "./Tratamiento";
export declare class Paciente {
    pac_codigo: number;
    pac_direccion: string | null;
    pac_telefono: string | null;
    pac_ocupacion: string | null;
    pac_lugar_nacimiento: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    antecedentes: Antecedente[];
    estciv_codigo: EstadoCivil;
    pac_estado: Estado;
    per_codigo: Persona;
    reservas: Reservas[];
    tratamientos: Tratamiento[];
}
