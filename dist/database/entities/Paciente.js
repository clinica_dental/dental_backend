"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Paciente = void 0;
const typeorm_1 = require("typeorm");
const Antecedente_1 = require("./Antecedente");
const EstadoCivil_1 = require("./EstadoCivil");
const Estado_1 = require("./Estado");
const Persona_1 = require("./Persona");
const Reservas_1 = require("./Reservas");
const Tratamiento_1 = require("./Tratamiento");
let Paciente = class Paciente {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "pac_codigo" }),
    __metadata("design:type", Number)
], Paciente.prototype, "pac_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "pac_direccion", nullable: true }),
    __metadata("design:type", String)
], Paciente.prototype, "pac_direccion", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "pac_telefono", nullable: true }),
    __metadata("design:type", String)
], Paciente.prototype, "pac_telefono", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "pac_ocupacion", nullable: true }),
    __metadata("design:type", String)
], Paciente.prototype, "pac_ocupacion", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "pac_lugar_nacimiento", nullable: true }),
    __metadata("design:type", String)
], Paciente.prototype, "pac_lugar_nacimiento", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Paciente.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Paciente.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Paciente.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Paciente.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Paciente.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Paciente.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Antecedente_1.Antecedente, (antecedente) => antecedente.pac_codigo),
    __metadata("design:type", Array)
], Paciente.prototype, "antecedentes", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => EstadoCivil_1.EstadoCivil, (estado_civil) => estado_civil.pacientes),
    (0, typeorm_1.JoinColumn)([
        { name: "estciv_codigo", referencedColumnName: "estciv_codigo" },
    ]),
    __metadata("design:type", EstadoCivil_1.EstadoCivil)
], Paciente.prototype, "estciv_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.pacientes),
    (0, typeorm_1.JoinColumn)([{ name: "pac_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Paciente.prototype, "pac_estado", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Persona_1.Persona, (persona) => persona.pacientes),
    (0, typeorm_1.JoinColumn)([{ name: "per_codigo", referencedColumnName: "per_codigo" }]),
    __metadata("design:type", Persona_1.Persona)
], Paciente.prototype, "per_codigo", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Reservas_1.Reservas, (reservas) => reservas.pac_codigo),
    __metadata("design:type", Array)
], Paciente.prototype, "reservas", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Tratamiento_1.Tratamiento, (tratamiento) => tratamiento.pac_codigo),
    __metadata("design:type", Array)
], Paciente.prototype, "tratamientos", void 0);
Paciente = __decorate([
    (0, typeorm_1.Index)("paciente_pk", ["pac_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("paciente", { schema: "registro" })
], Paciente);
exports.Paciente = Paciente;
//# sourceMappingURL=Paciente.js.map