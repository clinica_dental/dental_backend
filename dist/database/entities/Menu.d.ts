import { Estado } from "./Estado";
import { RolMenu } from "./RolMenu";
export declare class Menu {
    men_codigo: number;
    men_codigo_padre: number | null;
    men_orden: number;
    men_nombre: string;
    men_icono: string;
    men_ruta: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    men_codigo2: Menu;
    menu: Menu;
    men_estado: Estado;
    rol_menus: RolMenu[];
}
