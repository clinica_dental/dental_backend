"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tratamiento = void 0;
const typeorm_1 = require("typeorm");
const Paciente_1 = require("./Paciente");
const PersonalOdontologico_1 = require("./PersonalOdontologico");
const TipoTratamiento_1 = require("./TipoTratamiento");
const Estado_1 = require("./Estado");
const TratamientoPlan_1 = require("./TratamientoPlan");
let Tratamiento = class Tratamiento {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "tra_codigo" }),
    __metadata("design:type", Number)
], Tratamiento.prototype, "tra_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("numeric", {
        name: "tra_total",
        precision: 10,
        scale: 2,
        default: () => "0.0",
    }),
    __metadata("design:type", String)
], Tratamiento.prototype, "tra_total", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "tra_observaciones", nullable: true }),
    __metadata("design:type", String)
], Tratamiento.prototype, "tra_observaciones", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Tratamiento.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Tratamiento.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Tratamiento.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Tratamiento.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Tratamiento.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Tratamiento.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Paciente_1.Paciente, (paciente) => paciente.tratamientos),
    (0, typeorm_1.JoinColumn)([{ name: "pac_codigo", referencedColumnName: "pac_codigo" }]),
    __metadata("design:type", Paciente_1.Paciente)
], Tratamiento.prototype, "pac_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => PersonalOdontologico_1.PersonalOdontologico, (personal_odontologico) => personal_odontologico.tratamientos),
    (0, typeorm_1.JoinColumn)([
        { name: "perodo_codigo", referencedColumnName: "perodo_codigo" },
    ]),
    __metadata("design:type", PersonalOdontologico_1.PersonalOdontologico)
], Tratamiento.prototype, "perodo_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => TipoTratamiento_1.TipoTratamiento, (tipo_tratamiento) => tipo_tratamiento.tratamientos),
    (0, typeorm_1.JoinColumn)([
        { name: "tiptra_codigo", referencedColumnName: "tiptra_codigo" },
    ]),
    __metadata("design:type", TipoTratamiento_1.TipoTratamiento)
], Tratamiento.prototype, "tiptra_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.tratamientos),
    (0, typeorm_1.JoinColumn)([{ name: "tra_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Tratamiento.prototype, "tra_estado", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => TratamientoPlan_1.TratamientoPlan, (tratamiento_plan) => tratamiento_plan.tra_codigo),
    __metadata("design:type", Array)
], Tratamiento.prototype, "tratamiento_plans", void 0);
Tratamiento = __decorate([
    (0, typeorm_1.Index)("tratamiento_pk", ["tra_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("tratamiento", { schema: "registro" })
], Tratamiento);
exports.Tratamiento = Tratamiento;
//# sourceMappingURL=Tratamiento.js.map