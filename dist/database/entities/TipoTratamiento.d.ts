import { Estado } from "./Estado";
import { Tratamiento } from "./Tratamiento";
export declare class TipoTratamiento {
    tiptra_codigo: number;
    tiptra_nombre: string;
    tiptra_descripcion: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    tiptra_estado: Estado;
    tratamientos: Tratamiento[];
}
