"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Menu_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.Menu = void 0;
const typeorm_1 = require("typeorm");
const Estado_1 = require("./Estado");
const RolMenu_1 = require("./RolMenu");
let Menu = Menu_1 = class Menu {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "men_codigo" }),
    __metadata("design:type", Number)
], Menu.prototype, "men_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "men_codigo_padre", nullable: true }),
    __metadata("design:type", Number)
], Menu.prototype, "men_codigo_padre", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "men_orden" }),
    __metadata("design:type", Number)
], Menu.prototype, "men_orden", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "men_nombre" }),
    __metadata("design:type", String)
], Menu.prototype, "men_nombre", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "men_icono" }),
    __metadata("design:type", String)
], Menu.prototype, "men_icono", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "men_ruta" }),
    __metadata("design:type", String)
], Menu.prototype, "men_ruta", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Menu.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Menu.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Menu.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Menu.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Menu.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Menu.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => Menu_1, (menu) => menu.menu),
    (0, typeorm_1.JoinColumn)([{ name: "men_codigo", referencedColumnName: "men_codigo" }]),
    __metadata("design:type", Menu)
], Menu.prototype, "men_codigo2", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => Menu_1, (menu) => menu.men_codigo2),
    __metadata("design:type", Menu)
], Menu.prototype, "menu", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.menus),
    (0, typeorm_1.JoinColumn)([{ name: "men_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Menu.prototype, "men_estado", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => RolMenu_1.RolMenu, (rol_menu) => rol_menu.men_codigo2),
    __metadata("design:type", Array)
], Menu.prototype, "rol_menus", void 0);
Menu = Menu_1 = __decorate([
    (0, typeorm_1.Index)("menu_pk", ["men_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("menu", { schema: "autenticacion" })
], Menu);
exports.Menu = Menu;
//# sourceMappingURL=Menu.js.map