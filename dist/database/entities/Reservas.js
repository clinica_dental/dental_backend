"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Reservas = void 0;
const typeorm_1 = require("typeorm");
const Paciente_1 = require("./Paciente");
const PersonalOdontologico_1 = require("./PersonalOdontologico");
const Estado_1 = require("./Estado");
let Reservas = class Reservas {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "res_codigo" }),
    __metadata("design:type", Number)
], Reservas.prototype, "res_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("date", { name: "res_fecha" }),
    __metadata("design:type", String)
], Reservas.prototype, "res_fecha", void 0);
__decorate([
    (0, typeorm_1.Column)("time without time zone", { name: "res_hora" }),
    __metadata("design:type", String)
], Reservas.prototype, "res_hora", void 0);
__decorate([
    (0, typeorm_1.Column)("boolean", { name: "res_confirmacion", default: () => "false" }),
    __metadata("design:type", Boolean)
], Reservas.prototype, "res_confirmacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Reservas.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Reservas.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Reservas.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Reservas.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Reservas.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Reservas.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Paciente_1.Paciente, (paciente) => paciente.reservas),
    (0, typeorm_1.JoinColumn)([{ name: "pac_codigo", referencedColumnName: "pac_codigo" }]),
    __metadata("design:type", Paciente_1.Paciente)
], Reservas.prototype, "pac_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => PersonalOdontologico_1.PersonalOdontologico, (personal_odontologico) => personal_odontologico.reservas),
    (0, typeorm_1.JoinColumn)([
        { name: "perodo_codigo", referencedColumnName: "perodo_codigo" },
    ]),
    __metadata("design:type", PersonalOdontologico_1.PersonalOdontologico)
], Reservas.prototype, "perodo_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.reservas),
    (0, typeorm_1.JoinColumn)([{ name: "res_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Reservas.prototype, "res_estado", void 0);
Reservas = __decorate([
    (0, typeorm_1.Index)("reservas_pk", ["res_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("reservas", { schema: "registro" })
], Reservas);
exports.Reservas = Reservas;
//# sourceMappingURL=Reservas.js.map