import { Paciente } from "./Paciente";
import { PersonalOdontologico } from "./PersonalOdontologico";
import { Estado } from "./Estado";
export declare class Reservas {
    res_codigo: number;
    res_fecha: string;
    res_hora: string;
    res_confirmacion: boolean;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    pac_codigo: Paciente;
    perodo_codigo: PersonalOdontologico;
    res_estado: Estado;
}
