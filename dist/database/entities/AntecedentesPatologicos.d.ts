import { Antecedente } from "./Antecedente";
import { Estado } from "./Estado";
export declare class AntecedentesPatologicos {
    antpat_codigo: number;
    antpat_familiares: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    ant_codigo: Antecedente;
    antpat_estado: Estado;
}
