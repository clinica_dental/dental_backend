import { AntecedentesAfecciones } from "./AntecedentesAfecciones";
import { Estado } from "./Estado";
export declare class TipoAfeccion {
    tipafe_codigo: number;
    tipafe_nombre: string;
    tipafe_descripcion: string | null;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    antecedentes_afecciones: AntecedentesAfecciones[];
    tipafe_estado: Estado;
}
