"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Persona = void 0;
const typeorm_1 = require("typeorm");
const Paciente_1 = require("./Paciente");
const Genero_1 = require("./Genero");
const Estado_1 = require("./Estado");
const TipoDocumento_1 = require("./TipoDocumento");
const PersonalOdontologico_1 = require("./PersonalOdontologico");
const Usuario_1 = require("./Usuario");
let Persona = class Persona {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "per_codigo" }),
    __metadata("design:type", Number)
], Persona.prototype, "per_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "per_docidentidad", length: 20 }),
    __metadata("design:type", String)
], Persona.prototype, "per_docidentidad", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "per_nombres" }),
    __metadata("design:type", String)
], Persona.prototype, "per_nombres", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "per_primer_apellido" }),
    __metadata("design:type", String)
], Persona.prototype, "per_primer_apellido", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "per_segundo_apellido", nullable: true }),
    __metadata("design:type", String)
], Persona.prototype, "per_segundo_apellido", void 0);
__decorate([
    (0, typeorm_1.Column)("date", { name: "per_fecha_nacimiento" }),
    __metadata("design:type", String)
], Persona.prototype, "per_fecha_nacimiento", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "per_celular", nullable: true }),
    __metadata("design:type", String)
], Persona.prototype, "per_celular", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "per_correo", nullable: true }),
    __metadata("design:type", String)
], Persona.prototype, "per_correo", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Persona.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Persona.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Persona.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Persona.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Persona.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Persona.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Paciente_1.Paciente, (paciente) => paciente.per_codigo),
    __metadata("design:type", Array)
], Persona.prototype, "pacientes", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Genero_1.Genero, (genero) => genero.personas),
    (0, typeorm_1.JoinColumn)([{ name: "gen_codigo", referencedColumnName: "gen_codigo" }]),
    __metadata("design:type", Genero_1.Genero)
], Persona.prototype, "gen_codigo", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.personas),
    (0, typeorm_1.JoinColumn)([{ name: "per_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Persona.prototype, "per_estado", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => TipoDocumento_1.TipoDocumento, (tipo_documento) => tipo_documento.personas),
    (0, typeorm_1.JoinColumn)([
        { name: "tipdoc_codigo", referencedColumnName: "tipdoc_codigo" },
    ]),
    __metadata("design:type", TipoDocumento_1.TipoDocumento)
], Persona.prototype, "tipdoc_codigo", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => PersonalOdontologico_1.PersonalOdontologico, (personal_odontologico) => personal_odontologico.per_codigo),
    __metadata("design:type", Array)
], Persona.prototype, "personal_odontologicos", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => Usuario_1.Usuario, (usuario) => usuario.per_codigo),
    __metadata("design:type", Array)
], Persona.prototype, "usuarios", void 0);
Persona = __decorate([
    (0, typeorm_1.Index)("persona_pk", ["per_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("persona", { schema: "registro" })
], Persona);
exports.Persona = Persona;
//# sourceMappingURL=Persona.js.map