"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesDentales = void 0;
const typeorm_1 = require("typeorm");
const Antecedente_1 = require("./Antecedente");
const Estado_1 = require("./Estado");
let AntecedentesDentales = class AntecedentesDentales {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "ant_codigo" }),
    __metadata("design:type", Number)
], AntecedentesDentales.prototype, "ant_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_higiene", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_higiene", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_respiracion", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_respiracion", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_onicofagia", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_onicofagia", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_queilifagia", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_queilifagia", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_bruxismo", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_bruxismo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_cepillado", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_cepillado", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_diagnostico", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_diagnostico", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", {
        name: "antden_ultimo_tratamiento",
        nullable: true,
    }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_ultimo_tratamiento", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "antden_observaciones", nullable: true }),
    __metadata("design:type", String)
], AntecedentesDentales.prototype, "antden_observaciones", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], AntecedentesDentales.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], AntecedentesDentales.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], AntecedentesDentales.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], AntecedentesDentales.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], AntecedentesDentales.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], AntecedentesDentales.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => Antecedente_1.Antecedente, (antecedente) => antecedente.antecedentes_dentales),
    (0, typeorm_1.JoinColumn)([{ name: "ant_codigo", referencedColumnName: "ant_codigo" }]),
    __metadata("design:type", Antecedente_1.Antecedente)
], AntecedentesDentales.prototype, "ant_codigo2", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.antecedentes_dentales),
    (0, typeorm_1.JoinColumn)([{ name: "antden_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], AntecedentesDentales.prototype, "antden_estado", void 0);
AntecedentesDentales = __decorate([
    (0, typeorm_1.Index)("antedecentes_dentales_pk", ["ant_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("antecedentes_dentales", { schema: "registro" })
], AntecedentesDentales);
exports.AntecedentesDentales = AntecedentesDentales;
//# sourceMappingURL=AntecedentesDentales.js.map