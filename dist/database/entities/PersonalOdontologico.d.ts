import { Especialidad } from "./Especialidad";
import { Persona } from "./Persona";
import { Estado } from "./Estado";
import { Reservas } from "./Reservas";
import { Tratamiento } from "./Tratamiento";
export declare class PersonalOdontologico {
    perodo_codigo: number;
    perodo_turno: string;
    perodo_hora_inicio: string;
    perodo_hora_final: string;
    usuario_registro: number;
    usuario_modificacion: number;
    usuario_baja: number;
    fecha_registro: Date;
    fecha_modificacion: Date;
    fecha_baja: Date;
    esp_codigo: Especialidad;
    per_codigo: Persona;
    perodo_estado: Estado;
    reservas: Reservas[];
    tratamientos: Tratamiento[];
}
