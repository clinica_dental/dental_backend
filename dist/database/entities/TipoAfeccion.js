"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoAfeccion = void 0;
const typeorm_1 = require("typeorm");
const AntecedentesAfecciones_1 = require("./AntecedentesAfecciones");
const Estado_1 = require("./Estado");
let TipoAfeccion = class TipoAfeccion {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "tipafe_codigo" }),
    __metadata("design:type", Number)
], TipoAfeccion.prototype, "tipafe_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "tipafe_nombre" }),
    __metadata("design:type", String)
], TipoAfeccion.prototype, "tipafe_nombre", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "tipafe_descripcion", nullable: true }),
    __metadata("design:type", String)
], TipoAfeccion.prototype, "tipafe_descripcion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], TipoAfeccion.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], TipoAfeccion.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], TipoAfeccion.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], TipoAfeccion.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TipoAfeccion.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], TipoAfeccion.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => AntecedentesAfecciones_1.AntecedentesAfecciones, (antecedentes_afecciones) => antecedentes_afecciones.tipafe_codigo),
    __metadata("design:type", Array)
], TipoAfeccion.prototype, "antecedentes_afecciones", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.tipo_afeccions),
    (0, typeorm_1.JoinColumn)([{ name: "tipafe_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], TipoAfeccion.prototype, "tipafe_estado", void 0);
TipoAfeccion = __decorate([
    (0, typeorm_1.Index)("tipo_afeccion_pk", ["tipafe_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("tipo_afeccion", { schema: "parametricas" })
], TipoAfeccion);
exports.TipoAfeccion = TipoAfeccion;
//# sourceMappingURL=TipoAfeccion.js.map