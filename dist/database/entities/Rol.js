"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rol = void 0;
const typeorm_1 = require("typeorm");
const Estado_1 = require("./Estado");
const RolMenu_1 = require("./RolMenu");
const UsuarioRol_1 = require("./UsuarioRol");
let Rol = class Rol {
};
__decorate([
    (0, typeorm_1.Column)("integer", { primary: true, name: "rol_codigo" }),
    __metadata("design:type", Number)
], Rol.prototype, "rol_codigo", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "rol_nombre" }),
    __metadata("design:type", String)
], Rol.prototype, "rol_nombre", void 0);
__decorate([
    (0, typeorm_1.Column)("character varying", { name: "rol_descripcion" }),
    __metadata("design:type", String)
], Rol.prototype, "rol_descripcion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_registro" }),
    __metadata("design:type", Number)
], Rol.prototype, "usuario_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_modificacion", default: () => "0" }),
    __metadata("design:type", Number)
], Rol.prototype, "usuario_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("integer", { name: "usuario_baja", default: () => "0" }),
    __metadata("design:type", Number)
], Rol.prototype, "usuario_baja", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_registro",
        default: () => "(CURRENT_TIMESTAMP)::timestamp without time zone",
    }),
    __metadata("design:type", Date)
], Rol.prototype, "fecha_registro", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_modificacion",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Rol.prototype, "fecha_modificacion", void 0);
__decorate([
    (0, typeorm_1.Column)("timestamp without time zone", {
        name: "fecha_baja",
        default: () => "'1999-01-01 00:00:00'",
    }),
    __metadata("design:type", Date)
], Rol.prototype, "fecha_baja", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => Estado_1.Estado, (estado) => estado.rols),
    (0, typeorm_1.JoinColumn)([{ name: "rol_estado", referencedColumnName: "est_codigo" }]),
    __metadata("design:type", Estado_1.Estado)
], Rol.prototype, "rol_estado", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => RolMenu_1.RolMenu, (rol_menu) => rol_menu.rol_codigo2),
    __metadata("design:type", Array)
], Rol.prototype, "rol_menus", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => UsuarioRol_1.UsuarioRol, (usuario_rol) => usuario_rol.rol_codigo2),
    __metadata("design:type", Array)
], Rol.prototype, "usuario_rols", void 0);
Rol = __decorate([
    (0, typeorm_1.Index)("rol_pk", ["rol_codigo"], { unique: true }),
    (0, typeorm_1.Entity)("rol", { schema: "autenticacion" })
], Rol);
exports.Rol = Rol;
//# sourceMappingURL=Rol.js.map