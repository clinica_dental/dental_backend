"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicKey = void 0;
class PublicKey {
    constructor(authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
        this.callPublicKey();
    }
    get getPublicKey() {
        return this.publicKey;
    }
    async callPublicKey() {
        process.env.PUBLIC_KEY = 'ANY';
    }
}
exports.PublicKey = PublicKey;
//# sourceMappingURL=publicKey.js.map