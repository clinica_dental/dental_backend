import { AuthenticationProvider } from 'src/provider/authentication';
export declare class PublicKey {
    private readonly authenticationProvider;
    private publicKey;
    constructor(authenticationProvider: AuthenticationProvider);
    get getPublicKey(): string;
    callPublicKey(): Promise<void>;
}
