"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const http_exception_filter_1 = require("./common/exception/http.exception.filter");
const CustomLogger_1 = require("./common/util/CustomLogger");
const logger = new CustomLogger_1.Logger('SERVER');
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule, {
        logger: ['debug', 'error', 'warn', 'verbose'], cors: true
    });
    app.useGlobalPipes(new common_1.ValidationPipe({ whitelist: true, transform: true }));
    app.useGlobalFilters(new http_exception_filter_1.HttpExceptionFilter());
    app.use(bodyParser.json({ limit: '10mb' }));
    app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
    const config = new swagger_1.DocumentBuilder()
        .setTitle(process.env.NAME)
        .setDescription(process.env.DESCRIPTION)
        .setVersion(process.env.VERSION)
        .addBearerAuth({
        description: `Ingrese solamente el token obtenido de la autenticacion no incluya <strong>Bearer</strong>`,
        name: 'Authorization',
        type: 'http',
        in: 'Header'
    })
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('api', app, document);
    await app.listen(process.env.PORT || 3000);
    logger.verbose(`⚡️[SERVER:${process.env.NODE_ENV.toUpperCase()}]: REST Server is running at http://localhost:${process.env.PORT}/`);
    logger.verbose(`  Public Key getting from Auth Server.`);
    return app;
}
bootstrap();
//# sourceMappingURL=main.js.map