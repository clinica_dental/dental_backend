"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var ReservasController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservasController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_reservas_dto_1 = require("./dto/create-reservas.dto");
const update_reservas_dto_1 = require("./dto/update-reservas.dto");
const getall_reservas_dto_1 = require("./dto/getall-reservas.dto");
const delete_enable_reservas_dto_1 = require("./dto/delete-enable-reservas.dto");
const reservas_service_1 = require("./reservas.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let ReservasController = ReservasController_1 = class ReservasController {
    constructor(request, reservasService, customTransaction) {
        this.request = request;
        this.reservasService = reservasService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(ReservasController_1.name);
    }
    async findAllDetail(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.reservasService.findAllDetail(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllHorasPersonalOdontologico(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.reservasService.findAllHorasPersonalOdontologico(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllCalendar(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.reservasService.findAllCalendar(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createReservasDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultReservas = await this.reservasService.create(createReservasDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultReservas.res_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updateReservasDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultReservas = await this.reservasService.update(updateReservasDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updateReservasDto.res_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async updateConfirmar(res, updateConformarReservaDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultReservas = await this.reservasService.updateConfirmar(updateConformarReservaDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updateConformarReservaDto.res_codigo, 'SE REALIZO LA CONFIRMACIÓN DE LA RESERVA DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnableReservasDto, usuario) {
        try {
            deleteEnableReservasDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultReservas = await this.reservasService.remove(deleteEnableReservasDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultReservas.res_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnableReservasDto, usuario) {
        try {
            deleteEnableReservasDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultReservas = await this.reservasService.enable(deleteEnableReservasDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultReservas.res_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_reservas_dto_1.GetAllReservasDto]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "findAllDetail", null);
__decorate([
    (0, common_1.Get)('/horas'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_reservas_dto_1.GetAllReservasPersonalOdontologicoDto]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "findAllHorasPersonalOdontologico", null);
__decorate([
    (0, common_1.Get)('/calendario'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_reservas_dto_1.GetAllReservasDto]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "findAllCalendar", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_reservas_dto_1.CreateReservasDto]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_reservas_dto_1.UpdateReservasDto]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "update", null);
__decorate([
    (0, common_1.Put)('/confirmar'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_reservas_dto_1.UpdateConformarReservaDto]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "updateConfirmar", null);
__decorate([
    (0, common_1.Delete)(':res_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_reservas_dto_1.DeleteEnableReservasDto, Number]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:res_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_reservas_dto_1.DeleteEnableReservasDto, Number]),
    __metadata("design:returntype", Promise)
], ReservasController.prototype, "enable", null);
ReservasController = ReservasController_1 = __decorate([
    (0, swagger_1.ApiTags)('Reservas'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('reservas'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, reservas_service_1.ReservasService, CustomTransaction_1.CustomTransaction])
], ReservasController);
exports.ReservasController = ReservasController;
//# sourceMappingURL=reservas.controller.js.map