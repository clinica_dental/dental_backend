"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var ReservasService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservasService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const Reservas_1 = require("../../database/entities/Reservas");
const create_reservas_dto_1 = require("./dto/create-reservas.dto");
const update_reservas_dto_1 = require("./dto/update-reservas.dto");
const getall_reservas_dto_1 = require("./dto/getall-reservas.dto");
const delete_enable_reservas_dto_1 = require("./dto/delete-enable-reservas.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const personal_odontologico_service_1 = require("../personal-odontologico/personal-odontologico.service");
const getall_personal_odontologico_dto_1 = require("../personal-odontologico/dto/getall-personal-odontologico.dto");
const getall_paciente_dto_1 = require("../paciente/dto/getall-paciente.dto");
const paciente_service_1 = require("../paciente/paciente.service");
let ReservasService = ReservasService_1 = class ReservasService {
    constructor(request, personalOdontologicoService, pacienteService) {
        this.request = request;
        this.personalOdontologicoService = personalOdontologicoService;
        this.pacienteService = pacienteService;
        this.message_custom = 'RESERVAS -';
        this.logger = this.request.logger;
        this.logger.setContext(ReservasService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const reservas = new Reservas_1.Reservas();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            params.res_fecha = params.res_fecha ? (0, util_1.getDateFormat)(params.res_fecha) : undefined;
            if (!operation || (operation < 1 || operation > 5))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(Reservas_1.Reservas, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.res_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    estados.est_codigo = 1;
                    const custumWhereInsert = {
                        perodo_codigo: params.perodo_codigo,
                        res_hora: params.res_hora,
                        res_fecha: params.res_fecha,
                        res_estado: estados
                    };
                    findResult = await manager.findOne(Reservas_1.Reservas, { where: custumWhereInsert });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.res_codigo)
                        (0, error_handling_1.throwError)(400, 'EXISTE UNA RESERVA PARA EL PERSONAL ODONTOLÓGICO EN LA FECHA Y HORA ENVIADA');
                    try {
                        const getAllPersonalOdontologicoDto = new getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto();
                        getAllPersonalOdontologicoDto.perodo_codigo = `(${params.perodo_codigo})`;
                        getAllPersonalOdontologicoDto.perodo_estado = '(1)';
                        getAllPersonalOdontologicoDto.general_query = `AND '${params.res_hora}'::time BETWEEN t.perodo_hora_inicio AND  perodo_hora_final`;
                        await this.personalOdontologicoService.findAll(getAllPersonalOdontologicoDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE PERSONAL ODONTOLÓGICO NO EXISTE, NO ESTÁ ACTIVO O LA HORA ESTÁ FUERA DEL RANGO DE ATENCIÓN');
                    }
                    (0, util_1.bindingObjects)(reservas, params);
                    reservas.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    reservas.res_codigo = params.res_codigo;
                    reservas.res_estado = estados;
                    findResult = await manager.findOne(Reservas_1.Reservas, { where: reservas });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.res_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    estados.est_codigo = 1;
                    const custumWhereUpdate = {
                        perodo_codigo: params.perodo_codigo,
                        res_hora: params.res_hora,
                        res_fecha: params.res_fecha,
                        res_estado: estados
                    };
                    findResult = await manager.findOne(Reservas_1.Reservas, { where: custumWhereUpdate });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.res_codigo)
                        (0, error_handling_1.throwError)(400, 'EXISTE UNA RESERVA PARA EL PERSONAL ODONTOLÓGICO EN LA FECHA Y HORA ENVIADA');
                    try {
                        const getAllPersonalOdontologicoDto = new getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto();
                        getAllPersonalOdontologicoDto.perodo_codigo = `(${params.perodo_codigo})`;
                        getAllPersonalOdontologicoDto.perodo_estado = '(1)';
                        getAllPersonalOdontologicoDto.general_query = `AND '${params.res_hora}'::time BETWEEN t.perodo_hora_inicio AND  perodo_hora_final`;
                        await this.personalOdontologicoService.findAll(getAllPersonalOdontologicoDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE PERSONAL ODONTOLÓGICO NO EXISTE, NO ESTÁ ACTIVO O LA HORA ESTÁ FUERA DEL RANGO DE ATENCIÓN');
                    }
                    (0, util_1.bindingObjects)(reservas, params);
                    reservas.usuario_modificacion = usuario;
                    reservas.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    reservas.res_codigo = params.res_codigo;
                    reservas.res_estado = estados;
                    findResult = await manager.findOne(Reservas_1.Reservas, { where: reservas });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.res_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    reservas.usuario_baja = params.usuario;
                    reservas.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    reservas.res_codigo = params.res_codigo;
                    reservas.res_estado = estados;
                    findResult = await manager.findOne(Reservas_1.Reservas, { where: reservas });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.res_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    reservas.usuario_modificacion = params.usuario;
                    reservas.fecha_modificacion = new Date();
                    break;
                case 5:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    reservas.res_estado = estados;
                    reservas.res_codigo = params.res_codigo;
                    reservas.res_confirmacion = false;
                    findResult = await manager.findOne(Reservas_1.Reservas, { where: reservas });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.res_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE, ESTÁ INACTIVO O LA RESERVA YA SE CONFIRMO.');
                    (0, util_1.bindingObjects)(reservas, params);
                    reservas.usuario_modificacion = usuario;
                    reservas.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return reservas;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.res_codigo, 
        t.perodo_codigo, 
        t.pac_codigo, 
        to_char(t.res_fecha, 'DD/MM/YYYY') AS res_fecha, 
        t.res_hora,
        t.res_confirmacion,
        t.res_estado, 
        e.est_nombre AS res_estado_descripcion 
      FROM registro.reservas t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.res_estado
      WHERE TRUE
      ${query.res_codigo ? `AND t.res_codigo IN ${query.res_codigo}` : ''}
      ${query.res_estado ? `AND t.res_estado IN ${query.res_estado}` : ''}
      ${query.pac_codigo ? `AND t.pac_codigo IN ${query.pac_codigo}` : ''}
      ${query.res_fecha ? `AND t.res_fecha = '${(0, util_1.getDateFormat)(query.res_fecha)}'` : ''}
      ${query.perodo_codigo ? `AND t.perodo_codigo IN ${query.perodo_codigo}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllDetail(query, manager) {
        try {
            let sql = `
      SELECT 
        t.res_codigo, 
        t.perodo_codigo, 
        t.pac_codigo, 
        to_char(t.res_fecha, 'DD/MM/YYYY') AS res_fecha, 
        to_char(concat_ws(' ', t.res_fecha, t.res_hora)::timestamp, 'YYYY-MM-DD hh:mi') res_fecha_inicio, 
        to_char(concat_ws(' ', t.res_fecha, (t.res_hora + interval '1 hour'))::timestamp, 'YYYY-MM-DD hh:mi') res_fecha_final, 
        t.res_hora,
        t.res_confirmacion,
        '' AS paciente,
        '' AS personal_odontologico,
        t.res_estado, 
        e.est_nombre AS res_estado_descripcion 
      FROM registro.reservas t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.res_estado
      WHERE TRUE
      ${query.res_codigo ? `AND t.res_codigo IN ${query.res_codigo}` : ''}
      ${query.res_estado ? `AND t.res_estado IN ${query.res_estado}` : ''}
      ${query.pac_codigo ? `AND t.pac_codigo IN ${query.pac_codigo}` : ''}
      ${query.res_fecha ? `AND t.res_fecha = '${(0, util_1.getDateFormat)(query.res_fecha)}'` : ''}
      ${query.perodo_codigo ? `AND t.perodo_codigo IN ${query.perodo_codigo}` : ''}
      ${query.res_fecha_inicio && query.res_fecha_final ? `AND t.res_fecha BETWEEN '${(0, util_1.getDateFormat)(query.res_fecha_inicio)}'::date AND '${(0, util_1.getDateFormat)(query.res_fecha_final)}'::date` : ''}
      ORDER BY t.res_hora;`;
            const resultQuery = await manager.query(sql);
            if (resultQuery.length > 0) {
                for (const reserva of resultQuery) {
                    try {
                        const getAllPacienteDto = new getall_paciente_dto_1.GetAllPacienteDto();
                        getAllPacienteDto.pac_codigo = `(${reserva.pac_codigo})`;
                        getAllPacienteDto.pac_estado = '(1)';
                        const pacienteResult = await this.pacienteService.findAll(getAllPacienteDto, manager);
                        reserva.paciente = pacienteResult[0];
                    }
                    catch (error) {
                        reserva.paciente = {};
                    }
                    try {
                        const getAllPersonalOdontologicoDto = new getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto();
                        getAllPersonalOdontologicoDto.perodo_codigo = `(${reserva.perodo_codigo})`;
                        getAllPersonalOdontologicoDto.perodo_estado = '(1)';
                        const personalOdontologicoResult = await this.personalOdontologicoService.findAll(getAllPersonalOdontologicoDto, manager);
                        reserva.personal_odontologico = personalOdontologicoResult[0];
                    }
                    catch (error) {
                        reserva.personal_odontologico = {};
                    }
                }
            }
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllCalendar(query, manager) {
        try {
            const colors = ['blue', 'indigo', 'deep-purple', 'cyan', 'pink', 'orange', 'grey darken-1', 'green'];
            if (!query.res_fecha_inicio && !query.res_fecha_final) {
                (0, error_handling_1.throwError)(400, 'SE REQUEIREN LAS FECHA DE INICIO Y FINALIZACION');
            }
            const queryDates = new getall_reservas_dto_1.GetAllReservasDto();
            queryDates.res_fecha_inicio = query.res_fecha_inicio;
            queryDates.res_fecha_final = query.res_fecha_final;
            const resultQuery = await this.findAllDetail(queryDates, manager);
            const result = [];
            for (const item of resultQuery) {
                result.push({
                    perodo_codigo: item.personal_odontologico.perodo_codigo,
                    pac_codigo: item.paciente.pac_codigo,
                    name: item.personal_odontologico.per_nombre_completo,
                    timed: true,
                    start: item.res_fecha_inicio,
                    end: item.res_fecha_final,
                    color: colors[Math.floor(Math.random() * colors.length)]
                });
            }
            return customService_1.CustomService.verifyingDataResult(result, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllHorasPersonalOdontologico(query, manager) {
        try {
            let sql = `
      SELECT 
        -- po.perodo_codigo, 
        dd::time hora,
        0 AS dandera_tienereserva
      FROM registro.personal_odontologico po 
      LEFT JOIN generate_series( 
        ('1999-01-01 '|| po.perodo_hora_inicio)::timestamp, 
        ('1999-01-01 '|| po.perodo_hora_final)::timestamp, 
        '1 hour'::interval) dd ON TRUE 
      WHERE TRUE
      ${query.perodo_codigo ? `AND po.perodo_codigo IN ${query.perodo_codigo}` : ''};`;
            const resultQuery = await manager.query(sql);
            let reservasResult = {};
            let personalOdontologicoResult = {};
            const getAllReservasDto = new getall_reservas_dto_1.GetAllReservasDto();
            getAllReservasDto.perodo_codigo = query.perodo_codigo;
            getAllReservasDto.res_fecha = query.res_fecha;
            getAllReservasDto.res_estado = '(1)';
            reservasResult = await this.findAll(getAllReservasDto, manager);
            if (resultQuery.length > 0) {
                for (const reserva of resultQuery) {
                    const withReservation = reservasResult.find(f => f.res_hora == reserva.hora);
                    reserva.dandera_tienereserva = withReservation ? 1 : 0;
                }
                try {
                    const getAllPersonalOdontologicoDto = new getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto();
                    getAllPersonalOdontologicoDto.perodo_codigo = `(${query.perodo_codigo})`;
                    getAllPersonalOdontologicoDto.perodo_estado = '(1)';
                    personalOdontologicoResult = await this.personalOdontologicoService.findAll(getAllPersonalOdontologicoDto, manager);
                    personalOdontologicoResult[0].horarios = resultQuery;
                }
                catch (error) { }
            }
            else {
                (0, error_handling_1.throwError)(400, 'EL PERSONAL ODONTOLÓGICO NO TIENE ASIGNADO UN HORARIO PARA RESERVAS');
            }
            return customService_1.CustomService.verifyingDataResult(personalOdontologicoResult[0], this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(Reservas_1.Reservas, { where: { res_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createReservasDto, manager) {
        try {
            const reservas = await this.validations(1, manager, createReservasDto);
            const sql = 'SELECT COALESCE(MAX(reservas.res_codigo), 0) + 1 codigo FROM registro.reservas;';
            const codeResult = await manager.query(sql);
            reservas.res_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(reservas);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateReservasDto, manager) {
        try {
            const reservas = await this.validations(2, manager, updateReservasDto);
            const resultQuery = await manager.update(Reservas_1.Reservas, reservas.res_codigo, reservas);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async updateConfirmar(updateConformarReservaDto, manager) {
        try {
            const reservas = await this.validations(5, manager, updateConformarReservaDto);
            const resultQuery = await manager.update(Reservas_1.Reservas, reservas.res_codigo, reservas);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableReservasDto, manager) {
        try {
            const reservas = await this.validations(3, manager, deleteEnableReservasDto);
            const resultQuery = await manager.update(Reservas_1.Reservas, reservas.res_codigo, reservas);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableReservasDto, manager) {
        try {
            const reservas = await this.validations(4, manager, deleteEnableReservasDto);
            const resultQuery = await manager.update(Reservas_1.Reservas, reservas.res_codigo, reservas);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_reservas_dto_1.GetAllReservasDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_reservas_dto_1.GetAllReservasDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "findAllDetail", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_reservas_dto_1.GetAllReservasDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "findAllCalendar", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_reservas_dto_1.GetAllReservasPersonalOdontologicoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "findAllHorasPersonalOdontologico", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_reservas_dto_1.CreateReservasDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_reservas_dto_1.UpdateReservasDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_reservas_dto_1.UpdateConformarReservaDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "updateConfirmar", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_reservas_dto_1.DeleteEnableReservasDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_reservas_dto_1.DeleteEnableReservasDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReservasService.prototype, "enable", null);
ReservasService = ReservasService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, personal_odontologico_service_1.PersonalOdontologicoService,
        paciente_service_1.PacienteService])
], ReservasService);
exports.ReservasService = ReservasService;
//# sourceMappingURL=reservas.service.js.map