import { Request } from 'express';
import { Reservas } from 'src/database/entities/Reservas';
import { CreateReservasDto } from './dto/create-reservas.dto';
import { UpdateConformarReservaDto, UpdateReservasDto } from './dto/update-reservas.dto';
import { GetAllReservasDto, GetAllReservasPersonalOdontologicoDto } from './dto/getall-reservas.dto';
import { DeleteEnableReservasDto } from './dto/delete-enable-reservas.dto';
import { EntityManager } from 'typeorm';
import { PersonalOdontologicoService } from '../personal-odontologico/personal-odontologico.service';
import { PacienteService } from '../paciente/paciente.service';
export declare class ReservasService {
    private request;
    private personalOdontologicoService;
    private pacienteService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, personalOdontologicoService: PersonalOdontologicoService, pacienteService: PacienteService);
    private validations;
    findAll(query: GetAllReservasDto, manager: EntityManager): Promise<any>;
    findAllDetail(query: GetAllReservasDto, manager: EntityManager): Promise<any>;
    findAllCalendar(query: GetAllReservasDto, manager: EntityManager): Promise<any>;
    findAllHorasPersonalOdontologico(query: GetAllReservasPersonalOdontologicoDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createReservasDto: CreateReservasDto, manager: EntityManager): Promise<Reservas>;
    update(updateReservasDto: UpdateReservasDto, manager: EntityManager): Promise<any>;
    updateConfirmar(updateConformarReservaDto: UpdateConformarReservaDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableReservasDto: DeleteEnableReservasDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableReservasDto: DeleteEnableReservasDto, manager: EntityManager): Promise<any>;
}
