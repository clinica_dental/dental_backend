export declare class GetAllReservasDto {
    res_codigo: string;
    res_estado: string;
    pac_codigo: string;
    perodo_codigo: string;
    res_fecha: string;
    res_fecha_inicio: string;
    res_fecha_final: string;
    usuario: number;
}
export declare class GetAllReservasPersonalOdontologicoDto {
    perodo_codigo: string;
    res_fecha: string;
    usuario: number;
}
