"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateConformarReservaDto = exports.UpdateReservasDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const create_reservas_dto_1 = require("./create-reservas.dto");
const class_transformer_1 = require("class-transformer");
class UpdateReservasDto extends create_reservas_dto_1.CreateReservasDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.IsNotEmpty)({ message: "($property) Es requerido" }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateReservasDto.prototype, "res_codigo", void 0);
exports.UpdateReservasDto = UpdateReservasDto;
class UpdateConformarReservaDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.IsNotEmpty)({ message: "($property) Es requerido" }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateConformarReservaDto.prototype, "res_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.IsBoolean)({ message: '($property), El valor ($value) debe ser de tipo si/no.' }),
    __metadata("design:type", Boolean)
], UpdateConformarReservaDto.prototype, "res_confirmacion", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: '($property) El valor ($value) no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: '($property) Valor Maximo Superado' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], UpdateConformarReservaDto.prototype, "usuario", void 0);
exports.UpdateConformarReservaDto = UpdateConformarReservaDto;
//# sourceMappingURL=update-reservas.dto.js.map