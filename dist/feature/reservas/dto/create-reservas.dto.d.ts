export declare class CreateReservasDto {
    perodo_codigo: number;
    pac_codigo: number;
    res_fecha: string;
    res_hora: string;
    res_confirmacion: boolean;
    usuario: number;
}
