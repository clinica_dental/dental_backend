import { CreateReservasDto } from './create-reservas.dto';
export declare class UpdateReservasDto extends CreateReservasDto {
    res_codigo: number;
}
export declare class UpdateConformarReservaDto {
    res_codigo: number;
    res_confirmacion: boolean;
    usuario: number;
}
