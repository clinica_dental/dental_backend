import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateReservasDto } from './dto/create-reservas.dto';
import { UpdateConformarReservaDto, UpdateReservasDto } from './dto/update-reservas.dto';
import { GetAllReservasDto, GetAllReservasPersonalOdontologicoDto } from './dto/getall-reservas.dto';
import { DeleteEnableReservasDto } from './dto/delete-enable-reservas.dto';
import { ReservasService } from './reservas.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class ReservasController {
    private request;
    private readonly reservasService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, reservasService: ReservasService, customTransaction: CustomTransaction);
    findAllDetail(res: any, query: GetAllReservasDto): Promise<any>;
    findAllHorasPersonalOdontologico(res: any, query: GetAllReservasPersonalOdontologicoDto): Promise<any>;
    findAllCalendar(res: any, query: GetAllReservasDto): Promise<any>;
    create(res: any, createReservasDto: CreateReservasDto): Promise<ResultDto>;
    update(res: any, updateReservasDto: UpdateReservasDto): Promise<any>;
    updateConfirmar(res: any, updateConformarReservaDto: UpdateConformarReservaDto): Promise<any>;
    remove(res: any, deleteEnableReservasDto: DeleteEnableReservasDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableReservasDto: DeleteEnableReservasDto, usuario: number): Promise<any>;
}
