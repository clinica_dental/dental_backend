import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreatePersonaDto } from './dto/create-persona.dto';
import { UpdatePersonaDto } from './dto/update-persona.dto';
import { GetAllPersonaDto } from './dto/getall-persona.dto';
import { DeleteEnablePersonaDto } from './dto/delete-enable-persona.dto';
import { PersonaService } from './persona.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class PersonaController {
    private request;
    private readonly personaService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, personaService: PersonaService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllPersonaDto): Promise<any>;
    create(res: any, createPersonaDto: CreatePersonaDto): Promise<ResultDto>;
    update(res: any, updatePersonaDto: UpdatePersonaDto): Promise<any>;
    remove(res: any, deleteEnablePersonaDto: DeleteEnablePersonaDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnablePersonaDto: DeleteEnablePersonaDto, usuario: number): Promise<any>;
}
