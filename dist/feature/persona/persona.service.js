"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var PersonaService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonaService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const Persona_1 = require("../../database/entities/Persona");
const create_persona_dto_1 = require("./dto/create-persona.dto");
const update_persona_dto_1 = require("./dto/update-persona.dto");
const getall_persona_dto_1 = require("./dto/getall-persona.dto");
const delete_enable_persona_dto_1 = require("./dto/delete-enable-persona.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
let PersonaService = PersonaService_1 = class PersonaService {
    constructor(request) {
        this.request = request;
        this.message_custom = 'PERSONA -';
        this.logger = this.request.logger;
        this.logger.setContext(PersonaService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const persona = new Persona_1.Persona();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            params.per_fecha_nacimiento = (0, util_1.getDateFormat)(params.per_fecha_nacimiento);
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(Persona_1.Persona, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.per_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    (0, util_1.bindingObjects)(persona, params);
                    persona.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    persona.per_codigo = params.per_codigo;
                    persona.per_estado = estados;
                    findResult = await manager.findOne(Persona_1.Persona, { where: persona });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.per_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    (0, util_1.bindingObjects)(persona, params);
                    persona.usuario_modificacion = usuario;
                    persona.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    persona.per_codigo = params.per_codigo;
                    persona.per_estado = estados;
                    findResult = await manager.findOne(Persona_1.Persona, { where: persona });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.per_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    persona.usuario_baja = params.usuario;
                    persona.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    persona.per_codigo = params.per_codigo;
                    persona.per_estado = estados;
                    findResult = await manager.findOne(Persona_1.Persona, { where: persona });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.per_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    persona.usuario_modificacion = params.usuario;
                    persona.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return persona;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.per_codigo, 
        t.tipdoc_codigo, 
        t.gen_codigo, 
        t.per_docidentidad, 
        t.per_nombres, 
        t.per_primer_apellido, 
        t.per_segundo_apellido,
        concat_ws(' ', t.per_nombres, t.per_primer_apellido, t.per_segundo_apellido) AS per_nombre_completo,
        to_char(t.per_fecha_nacimiento, 'DD/MM/YYYY') AS per_fecha_nacimiento,
        t.per_celular, 
        t.per_correo,
        t.per_estado, 
        e.est_nombre AS per_estado_descripcion 
      FROM registro.persona t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.per_estado
      LEFT JOIN parametricas.tipo_documento td ON td.tipdoc_codigo = t.tipdoc_codigo
      LEFT JOIN parametricas.genero g ON g.gen_codigo = t.gen_codigo
      WHERE TRUE
      ${query.per_codigo ? `AND t.per_codigo IN ${query.per_codigo}` : ''}
      ${query.per_estado ? `AND t.per_estado IN ${query.per_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(Persona_1.Persona, { where: { per_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createPersonaDto, manager) {
        try {
            const persona = await this.validations(1, manager, createPersonaDto);
            const sql = 'SELECT COALESCE(MAX(persona.per_codigo), 0) + 1 codigo FROM registro.persona;';
            const codeResult = await manager.query(sql);
            persona.per_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(persona);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updatePersonaDto, manager) {
        try {
            const persona = await this.validations(2, manager, updatePersonaDto);
            const resultQuery = await manager.update(Persona_1.Persona, persona.per_codigo, persona);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnablePersonaDto, manager) {
        try {
            const persona = await this.validations(3, manager, deleteEnablePersonaDto);
            const resultQuery = await manager.update(Persona_1.Persona, persona.per_codigo, persona);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnablePersonaDto, manager) {
        try {
            const persona = await this.validations(4, manager, deleteEnablePersonaDto);
            const resultQuery = await manager.update(Persona_1.Persona, persona.per_codigo, persona);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], PersonaService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_persona_dto_1.GetAllPersonaDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonaService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonaService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_persona_dto_1.CreatePersonaDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonaService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_persona_dto_1.UpdatePersonaDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonaService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_persona_dto_1.DeleteEnablePersonaDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonaService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_persona_dto_1.DeleteEnablePersonaDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonaService.prototype, "enable", null);
PersonaService = PersonaService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object])
], PersonaService);
exports.PersonaService = PersonaService;
//# sourceMappingURL=persona.service.js.map