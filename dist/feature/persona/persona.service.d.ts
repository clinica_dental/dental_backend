import { Request } from 'express';
import { Persona } from 'src/database/entities/Persona';
import { CreatePersonaDto } from './dto/create-persona.dto';
import { UpdatePersonaDto } from './dto/update-persona.dto';
import { GetAllPersonaDto } from './dto/getall-persona.dto';
import { DeleteEnablePersonaDto } from './dto/delete-enable-persona.dto';
import { EntityManager } from 'typeorm';
export declare class PersonaService {
    private request;
    private readonly logger;
    private message_custom;
    constructor(request: Request);
    private validations;
    findAll(query: GetAllPersonaDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createPersonaDto: CreatePersonaDto, manager: EntityManager): Promise<Persona>;
    update(updatePersonaDto: UpdatePersonaDto, manager: EntityManager): Promise<any>;
    remove(deleteEnablePersonaDto: DeleteEnablePersonaDto, manager: EntityManager): Promise<any>;
    enable(deleteEnablePersonaDto: DeleteEnablePersonaDto, manager: EntityManager): Promise<any>;
}
