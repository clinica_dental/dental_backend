"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonaModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
const index_1 = require("../../database/entities/index");
const persona_controller_1 = require("./persona.controller");
const persona_service_1 = require("./persona.service");
let PersonaModule = class PersonaModule {
};
PersonaModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature(index_1.entities)],
        providers: [persona_service_1.PersonaService, CustomTransaction_1.CustomTransaction],
        controllers: [persona_controller_1.PersonaController],
        exports: [persona_service_1.PersonaService],
    })
], PersonaModule);
exports.PersonaModule = PersonaModule;
//# sourceMappingURL=persona.module.js.map