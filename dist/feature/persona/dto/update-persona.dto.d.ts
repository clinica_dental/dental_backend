import { CreatePersonaDto } from './create-persona.dto';
export declare class UpdatePersonaDto extends CreatePersonaDto {
    per_codigo: number;
}
