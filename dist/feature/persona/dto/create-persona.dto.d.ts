export declare class CreatePersonaDto {
    tipdoc_codigo: number;
    gen_codigo: number;
    per_docidentidad: string;
    per_nombres: string;
    per_primer_apellido: string;
    per_segundo_apellido: string;
    per_fecha_nacimiento: string;
    per_celular: string;
    per_correo: string;
    usuario: number;
}
