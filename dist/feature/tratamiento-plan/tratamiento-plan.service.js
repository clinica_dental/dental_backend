"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var TratamientoPlanService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.TratamientoPlanService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const TratamientoPlan_1 = require("../../database/entities/TratamientoPlan");
const create_tratamiento_plan_dto_1 = require("./dto/create-tratamiento-plan.dto");
const update_tratamiento_plan_dto_1 = require("./dto/update-tratamiento-plan.dto");
const getall_tratamiento_plan_dto_1 = require("./dto/getall-tratamiento-plan.dto");
const delete_enable_tratamiento_plan_dto_1 = require("./dto/delete-enable-tratamiento-plan.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const getall_tratamiento_dto_1 = require("../tratamiento/dto/getall-tratamiento.dto");
const tratamiento_service_1 = require("../tratamiento/tratamiento.service");
let TratamientoPlanService = TratamientoPlanService_1 = class TratamientoPlanService {
    constructor(request, tratamientoService) {
        this.request = request;
        this.tratamientoService = tratamientoService;
        this.message_custom = 'TRATAMIENTO PLAN -';
        this.logger = this.request.logger;
        this.logger.setContext(TratamientoPlanService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const tratamientoPlan = new TratamientoPlan_1.TratamientoPlan();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(TratamientoPlan_1.TratamientoPlan, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.trapla_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllTratamientoDto = new getall_tratamiento_dto_1.GetAllTratamientoDto();
                        getAllTratamientoDto.tra_codigo = `(${params.tra_codigo})`;
                        getAllTratamientoDto.tra_estado = '(1)';
                        await this.tratamientoService.findAll(getAllTratamientoDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE TRATAMIENTO NO EXISTE O NO ESTA ACTIVO');
                    }
                    (0, util_1.bindingObjects)(tratamientoPlan, params);
                    tratamientoPlan.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    tratamientoPlan.trapla_codigo = params.trapla_codigo;
                    tratamientoPlan.trapla_estado = estados;
                    findResult = await manager.findOne(TratamientoPlan_1.TratamientoPlan, { where: tratamientoPlan });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.trapla_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllTratamientoDto = new getall_tratamiento_dto_1.GetAllTratamientoDto();
                        getAllTratamientoDto.tra_codigo = `(${params.tra_codigo})`;
                        getAllTratamientoDto.tra_estado = '(1)';
                        await this.tratamientoService.findAll(getAllTratamientoDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE TRATAMIENTO NO EXISTE O NO ESTA ACTIVO');
                    }
                    (0, util_1.bindingObjects)(tratamientoPlan, params);
                    tratamientoPlan.usuario_modificacion = usuario;
                    tratamientoPlan.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    tratamientoPlan.trapla_codigo = params.trapla_codigo;
                    tratamientoPlan.trapla_estado = estados;
                    findResult = await manager.findOne(TratamientoPlan_1.TratamientoPlan, { where: tratamientoPlan });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.trapla_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    tratamientoPlan.usuario_baja = params.usuario;
                    tratamientoPlan.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    tratamientoPlan.trapla_codigo = params.trapla_codigo;
                    tratamientoPlan.trapla_estado = estados;
                    findResult = await manager.findOne(TratamientoPlan_1.TratamientoPlan, { where: tratamientoPlan });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.trapla_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    tratamientoPlan.usuario_modificacion = params.usuario;
                    tratamientoPlan.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return tratamientoPlan;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      WITH tmp_ultimo_registro AS (
        SELECT 
          tp.trapla_codigo,
          tp.tra_codigo
        FROM registro.tratamiento_plan tp
        WHERE tp.tra_codigo IN ${query.tra_codigo}
        ORDER BY tp.trapla_codigo DESC 
        LIMIT 1
      ), tmp_tratamiento_plan_total AS (
        SELECT 
          tp.tra_codigo, 
          sum(tp.trapla_acuenta) AS total_pagos
        FROM registro.tratamiento_plan tp 
        WHERE tp.tra_codigo = ${query.tra_codigo}
        GROUP BY tp.tra_codigo 
      )
      SELECT 
        t.trapla_codigo, 
        t.tra_codigo, 
        to_char(t.trapla_fecha, 'DD/MM/YYYY') trapla_fecha, 
        t.trapla_pieza, 
        t.trapla_diagnostico, 
        t.trapla_tratamiento, 
        t.trapla_acuenta,
        t.trapla_saldo,
        ttpt.total_pagos AS trapla_total_pagos,
        t2.tra_total, 
        t.trapla_observaciones,
        t.trapla_estado, 
        e.est_nombre AS trapla_estado_descripcion,
        CASE WHEN tur.trapla_codigo IS NOT NULL THEN 1 ELSE 0 END AS bandera_puedemodificar
      FROM registro.tratamiento_plan t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.trapla_estado
      LEFT JOIN registro.tratamiento t2 ON t2.tra_codigo = t.tra_codigo
      LEFT JOIN tmp_ultimo_registro tur ON tur.trapla_codigo = t.trapla_codigo
      LEFT JOIN tmp_tratamiento_plan_total ttpt ON ttpt.tra_codigo = t.tra_codigo
      WHERE TRUE
      AND t2.tra_estado > 0
      ${query.trapla_codigo ? `AND t.trapla_codigo IN ${query.trapla_codigo}` : ''}
      ${query.tra_codigo ? `AND t2.tra_codigo IN ${query.tra_codigo}` : ''}
      ${query.trapla_estado ? `AND t.trapla_estado IN ${query.trapla_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findMontosTratamiento(traCodigo, traplaAcuenta, traplaCodigo = null, manager) {
        try {
            let sql = `
      WITH tmp_tratamiento_plan_total AS (
        SELECT 
          tp.tra_codigo, 
          sum(tp.trapla_acuenta) AS total_pagos
        FROM registro.tratamiento_plan tp 
        WHERE tp.tra_codigo = ${traCodigo}
        ${traplaCodigo ? `AND tp.trapla_codigo <> ${traplaCodigo}` : ''}
        GROUP BY tp.tra_codigo 
      )
      SELECT 
        t.tra_total,
        COALESCE (ttpt.total_pagos, 0) + (${traplaAcuenta})::NUMERIC(10,2) AS trapla_acuenta,
        t.tra_total - (COALESCE (ttpt.total_pagos, 0) + (${traplaAcuenta})::NUMERIC(10,2)) AS trapla_saldo
      FROM registro.tratamiento t 
      LEFT JOIN tmp_tratamiento_plan_total ttpt ON ttpt.tra_codigo = t.tra_codigo
      WHERE t.tra_estado > 0
      AND t.tra_codigo = ${traCodigo};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(TratamientoPlan_1.TratamientoPlan, { where: { trapla_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createTratamientoPlanDto, manager) {
        try {
            try {
                const resultMontos = await this.findMontosTratamiento(createTratamientoPlanDto.tra_codigo, createTratamientoPlanDto.trapla_acuenta, null, manager);
                createTratamientoPlanDto.trapla_fecha = new Date();
                createTratamientoPlanDto.trapla_saldo = resultMontos[0].trapla_saldo;
            }
            catch (error) { }
            const tratamientoPlan = await this.validations(1, manager, createTratamientoPlanDto);
            const sql = 'SELECT COALESCE(MAX(tratamiento_plan.trapla_codigo), 0) + 1 codigo FROM registro.tratamiento_plan;';
            const codeResult = await manager.query(sql);
            tratamientoPlan.trapla_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(tratamientoPlan);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateTratamientoPlanDto, manager) {
        try {
            try {
                const resultMontos = await this.findMontosTratamiento(updateTratamientoPlanDto.tra_codigo, updateTratamientoPlanDto.trapla_acuenta, updateTratamientoPlanDto.trapla_codigo, manager);
                updateTratamientoPlanDto.trapla_fecha = new Date();
                updateTratamientoPlanDto.trapla_saldo = resultMontos[0].trapla_saldo;
            }
            catch (error) { }
            const tratamientoPlan = await this.validations(2, manager, updateTratamientoPlanDto);
            const resultQuery = await manager.update(TratamientoPlan_1.TratamientoPlan, tratamientoPlan.trapla_codigo, tratamientoPlan);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableTratamientoPlanDto, manager) {
        try {
            const tratamientoPlan = await this.validations(3, manager, deleteEnableTratamientoPlanDto);
            const resultQuery = await manager.update(TratamientoPlan_1.TratamientoPlan, tratamientoPlan.trapla_codigo, tratamientoPlan);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableTratamientoPlanDto, manager) {
        try {
            const tratamientoPlan = await this.validations(4, manager, deleteEnableTratamientoPlanDto);
            const resultQuery = await manager.update(TratamientoPlan_1.TratamientoPlan, tratamientoPlan.trapla_codigo, tratamientoPlan);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_tratamiento_plan_dto_1.GetAllTratamientoPlanDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "findMontosTratamiento", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_tratamiento_plan_dto_1.CreateTratamientoPlanDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_tratamiento_plan_dto_1.UpdateTratamientoPlanDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_tratamiento_plan_dto_1.DeleteEnableTratamientoPlanDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_tratamiento_plan_dto_1.DeleteEnableTratamientoPlanDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoPlanService.prototype, "enable", null);
TratamientoPlanService = TratamientoPlanService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, tratamiento_service_1.TratamientoService])
], TratamientoPlanService);
exports.TratamientoPlanService = TratamientoPlanService;
//# sourceMappingURL=tratamiento-plan.service.js.map