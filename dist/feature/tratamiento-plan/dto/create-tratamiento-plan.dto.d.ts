export declare class CreateTratamientoPlanDto {
    tra_codigo: number;
    trapla_pieza: string;
    trapla_diagnostico: string;
    trapla_tratamiento: string;
    trapla_acuenta: string;
    trapla_observaciones: string;
    usuario: number;
    trapla_fecha: Date;
    trapla_saldo: string;
}
