import { Request } from 'express';
import { TratamientoPlan } from 'src/database/entities/TratamientoPlan';
import { CreateTratamientoPlanDto } from './dto/create-tratamiento-plan.dto';
import { UpdateTratamientoPlanDto } from './dto/update-tratamiento-plan.dto';
import { GetAllTratamientoPlanDto } from './dto/getall-tratamiento-plan.dto';
import { DeleteEnableTratamientoPlanDto } from './dto/delete-enable-tratamiento-plan.dto';
import { EntityManager } from 'typeorm';
import { TratamientoService } from '../tratamiento/tratamiento.service';
export declare class TratamientoPlanService {
    private request;
    private tratamientoService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, tratamientoService: TratamientoService);
    private validations;
    findAll(query: GetAllTratamientoPlanDto, manager: EntityManager): Promise<any>;
    findMontosTratamiento(traCodigo: number, traplaAcuenta: string, traplaCodigo: number, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createTratamientoPlanDto: CreateTratamientoPlanDto, manager: EntityManager): Promise<TratamientoPlan>;
    update(updateTratamientoPlanDto: UpdateTratamientoPlanDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableTratamientoPlanDto: DeleteEnableTratamientoPlanDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableTratamientoPlanDto: DeleteEnableTratamientoPlanDto, manager: EntityManager): Promise<any>;
}
