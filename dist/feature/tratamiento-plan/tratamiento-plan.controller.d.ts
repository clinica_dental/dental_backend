import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateTratamientoPlanDto } from './dto/create-tratamiento-plan.dto';
import { UpdateTratamientoPlanDto } from './dto/update-tratamiento-plan.dto';
import { GetAllTratamientoPlanDto } from './dto/getall-tratamiento-plan.dto';
import { DeleteEnableTratamientoPlanDto } from './dto/delete-enable-tratamiento-plan.dto';
import { TratamientoPlanService } from './tratamiento-plan.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class TratamientoPlanController {
    private request;
    private readonly tratamientoPlanService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, tratamientoPlanService: TratamientoPlanService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllTratamientoPlanDto): Promise<any>;
    create(res: any, createTratamientoPlanDto: CreateTratamientoPlanDto): Promise<ResultDto>;
    update(res: any, updateTratamientoPlanDto: UpdateTratamientoPlanDto): Promise<any>;
    remove(res: any, deleteEnableTratamientoPlanDto: DeleteEnableTratamientoPlanDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableTratamientoPlanDto: DeleteEnableTratamientoPlanDto, usuario: number): Promise<any>;
}
