"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var TratamientoPlanController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.TratamientoPlanController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_tratamiento_plan_dto_1 = require("./dto/create-tratamiento-plan.dto");
const update_tratamiento_plan_dto_1 = require("./dto/update-tratamiento-plan.dto");
const getall_tratamiento_plan_dto_1 = require("./dto/getall-tratamiento-plan.dto");
const delete_enable_tratamiento_plan_dto_1 = require("./dto/delete-enable-tratamiento-plan.dto");
const tratamiento_plan_service_1 = require("./tratamiento-plan.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let TratamientoPlanController = TratamientoPlanController_1 = class TratamientoPlanController {
    constructor(request, tratamientoPlanService, customTransaction) {
        this.request = request;
        this.tratamientoPlanService = tratamientoPlanService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(TratamientoPlanController_1.name);
    }
    async findAll(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.tratamientoPlanService.findAll(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createTratamientoPlanDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultTratamientoPlan = await this.tratamientoPlanService.create(createTratamientoPlanDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultTratamientoPlan.trapla_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updateTratamientoPlanDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultTratamientoPlan = await this.tratamientoPlanService.update(updateTratamientoPlanDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updateTratamientoPlanDto.trapla_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnableTratamientoPlanDto, usuario) {
        try {
            deleteEnableTratamientoPlanDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultTratamientoPlan = await this.tratamientoPlanService.remove(deleteEnableTratamientoPlanDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultTratamientoPlan.trapla_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnableTratamientoPlanDto, usuario) {
        try {
            deleteEnableTratamientoPlanDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultTratamientoPlan = await this.tratamientoPlanService.enable(deleteEnableTratamientoPlanDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultTratamientoPlan.trapla_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_tratamiento_plan_dto_1.GetAllTratamientoPlanDto]),
    __metadata("design:returntype", Promise)
], TratamientoPlanController.prototype, "findAll", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_tratamiento_plan_dto_1.CreateTratamientoPlanDto]),
    __metadata("design:returntype", Promise)
], TratamientoPlanController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_tratamiento_plan_dto_1.UpdateTratamientoPlanDto]),
    __metadata("design:returntype", Promise)
], TratamientoPlanController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':trapla_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_tratamiento_plan_dto_1.DeleteEnableTratamientoPlanDto, Number]),
    __metadata("design:returntype", Promise)
], TratamientoPlanController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:trapla_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_tratamiento_plan_dto_1.DeleteEnableTratamientoPlanDto, Number]),
    __metadata("design:returntype", Promise)
], TratamientoPlanController.prototype, "enable", null);
TratamientoPlanController = TratamientoPlanController_1 = __decorate([
    (0, swagger_1.ApiTags)('TratamientoPlan'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('tratamiento-plan'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, tratamiento_plan_service_1.TratamientoPlanService, CustomTransaction_1.CustomTransaction])
], TratamientoPlanController);
exports.TratamientoPlanController = TratamientoPlanController;
//# sourceMappingURL=tratamiento-plan.controller.js.map