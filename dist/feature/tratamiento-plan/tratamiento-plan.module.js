"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TratamientoPlanModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
const index_1 = require("../../database/entities/index");
const tratamiento_plan_controller_1 = require("./tratamiento-plan.controller");
const tratamiento_plan_service_1 = require("./tratamiento-plan.service");
let TratamientoPlanModule = class TratamientoPlanModule {
};
TratamientoPlanModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature(index_1.entities)],
        providers: [tratamiento_plan_service_1.TratamientoPlanService, CustomTransaction_1.CustomTransaction],
        controllers: [tratamiento_plan_controller_1.TratamientoPlanController],
        exports: [tratamiento_plan_service_1.TratamientoPlanService],
    })
], TratamientoPlanModule);
exports.TratamientoPlanModule = TratamientoPlanModule;
//# sourceMappingURL=tratamiento-plan.module.js.map