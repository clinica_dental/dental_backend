"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AntecedentesAfeccionesService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesAfeccionesService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const AntecedentesAfecciones_1 = require("../../database/entities/AntecedentesAfecciones");
const create_antecedentes_afecciones_dto_1 = require("./dto/create-antecedentes-afecciones.dto");
const update_antecedentes_afecciones_dto_1 = require("./dto/update-antecedentes-afecciones.dto");
const getall_antecedentes_afecciones_dto_1 = require("./dto/getall-antecedentes-afecciones.dto");
const delete_enable_antecedentes_afecciones_dto_1 = require("./dto/delete-enable-antecedentes-afecciones.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const antecedente_service_1 = require("../antecedente/antecedente.service");
const getall_antecedente_dto_1 = require("../antecedente/dto/getall-antecedente.dto");
let AntecedentesAfeccionesService = AntecedentesAfeccionesService_1 = class AntecedentesAfeccionesService {
    constructor(request, antecedenteService) {
        this.request = request;
        this.antecedenteService = antecedenteService;
        this.message_custom = 'ANTECEDENTES AFECCIONES -';
        this.logger = this.request.logger;
        this.logger.setContext(AntecedentesAfeccionesService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const antecedentesAfecciones = new AntecedentesAfecciones_1.AntecedentesAfecciones();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(AntecedentesAfecciones_1.AntecedentesAfecciones, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.antafe_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllAntecedenteDto = new getall_antecedente_dto_1.GetAllAntecedenteDto();
                        getAllAntecedenteDto.ant_codigo = `(${params.ant_codigo})`;
                        getAllAntecedenteDto.ant_estado = '(1)';
                        await this.antecedenteService.findAll(getAllAntecedenteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE ANTECEDENTE NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(antecedentesAfecciones, params);
                    antecedentesAfecciones.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    antecedentesAfecciones.antafe_codigo = params.antafe_codigo;
                    antecedentesAfecciones.antafe_estado = estados;
                    findResult = await manager.findOne(AntecedentesAfecciones_1.AntecedentesAfecciones, { where: antecedentesAfecciones });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.antafe_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllAntecedenteDto = new getall_antecedente_dto_1.GetAllAntecedenteDto();
                        getAllAntecedenteDto.ant_codigo = `(${params.ant_codigo})`;
                        getAllAntecedenteDto.ant_estado = '(1)';
                        await this.antecedenteService.findAll(getAllAntecedenteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE ANTECEDENTE NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(antecedentesAfecciones, params);
                    antecedentesAfecciones.usuario_modificacion = usuario;
                    antecedentesAfecciones.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    antecedentesAfecciones.antafe_codigo = params.antafe_codigo;
                    antecedentesAfecciones.antafe_estado = estados;
                    findResult = await manager.findOne(AntecedentesAfecciones_1.AntecedentesAfecciones, { where: antecedentesAfecciones });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.antafe_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    antecedentesAfecciones.usuario_baja = params.usuario;
                    antecedentesAfecciones.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    antecedentesAfecciones.antafe_codigo = params.antafe_codigo;
                    antecedentesAfecciones.antafe_estado = estados;
                    findResult = await manager.findOne(AntecedentesAfecciones_1.AntecedentesAfecciones, { where: antecedentesAfecciones });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.antafe_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    antecedentesAfecciones.usuario_modificacion = params.usuario;
                    antecedentesAfecciones.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return antecedentesAfecciones;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.antafe_codigo, 
        t.ant_codigo,
        t.tipafe_codigo,
        ta.tipafe_nombre, 
        ta.tipafe_descripcion,
        t.antafe_estado, 
        e.est_nombre AS antafe_estado_descripcion 
      FROM registro.antecedentes_afecciones t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.antafe_estado
      LEFT JOIN parametricas.tipo_afeccion ta ON ta.tipafe_codigo = t.tipafe_codigo
      WHERE TRUE
      ${query.antafe_codigo ? `AND t.antafe_codigo IN ${query.antafe_codigo}` : ''}
      ${query.antafe_estado ? `AND t.antafe_estado IN ${query.antafe_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(AntecedentesAfecciones_1.AntecedentesAfecciones, { where: { antafe_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createAntecedentesAfeccionesDto, manager) {
        try {
            const antecedentesAfecciones = await this.validations(1, manager, createAntecedentesAfeccionesDto);
            const sql = 'SELECT COALESCE(MAX(antecedentes_afecciones.antafe_codigo), 0) + 1 codigo FROM registro.antecedentes_afecciones;';
            const codeResult = await manager.query(sql);
            antecedentesAfecciones.antafe_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(antecedentesAfecciones);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateAntecedentesAfeccionesDto, manager) {
        try {
            const antecedentesAfecciones = await this.validations(2, manager, updateAntecedentesAfeccionesDto);
            const resultQuery = await manager.update(AntecedentesAfecciones_1.AntecedentesAfecciones, antecedentesAfecciones.antafe_codigo, antecedentesAfecciones);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableAntecedentesAfeccionesDto, manager) {
        try {
            const antecedentesAfecciones = await this.validations(3, manager, deleteEnableAntecedentesAfeccionesDto);
            const resultQuery = await manager.update(AntecedentesAfecciones_1.AntecedentesAfecciones, antecedentesAfecciones.antafe_codigo, antecedentesAfecciones);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableAntecedentesAfeccionesDto, manager) {
        try {
            const antecedentesAfecciones = await this.validations(4, manager, deleteEnableAntecedentesAfeccionesDto);
            const resultQuery = await manager.update(AntecedentesAfecciones_1.AntecedentesAfecciones, antecedentesAfecciones.antafe_codigo, antecedentesAfecciones);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_antecedentes_afecciones_dto_1.GetAllAntecedentesAfeccionesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_antecedentes_afecciones_dto_1.CreateAntecedentesAfeccionesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_antecedentes_afecciones_dto_1.UpdateAntecedentesAfeccionesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedentes_afecciones_dto_1.DeleteEnableAntecedentesAfeccionesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedentes_afecciones_dto_1.DeleteEnableAntecedentesAfeccionesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesService.prototype, "enable", null);
AntecedentesAfeccionesService = AntecedentesAfeccionesService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, antecedente_service_1.AntecedenteService])
], AntecedentesAfeccionesService);
exports.AntecedentesAfeccionesService = AntecedentesAfeccionesService;
//# sourceMappingURL=antecedentes-afecciones.service.js.map