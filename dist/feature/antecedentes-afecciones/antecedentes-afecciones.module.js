"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesAfeccionesModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
const index_1 = require("../../database/entities/index");
const antecedentes_afecciones_controller_1 = require("./antecedentes-afecciones.controller");
const antecedentes_afecciones_service_1 = require("./antecedentes-afecciones.service");
let AntecedentesAfeccionesModule = class AntecedentesAfeccionesModule {
};
AntecedentesAfeccionesModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature(index_1.entities)],
        providers: [antecedentes_afecciones_service_1.AntecedentesAfeccionesService, CustomTransaction_1.CustomTransaction],
        controllers: [antecedentes_afecciones_controller_1.AntecedentesAfeccionesController],
        exports: [antecedentes_afecciones_service_1.AntecedentesAfeccionesService],
    })
], AntecedentesAfeccionesModule);
exports.AntecedentesAfeccionesModule = AntecedentesAfeccionesModule;
//# sourceMappingURL=antecedentes-afecciones.module.js.map