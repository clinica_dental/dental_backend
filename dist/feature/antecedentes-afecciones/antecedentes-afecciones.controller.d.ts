import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateAntecedentesAfeccionesDto } from './dto/create-antecedentes-afecciones.dto';
import { UpdateAntecedentesAfeccionesDto } from './dto/update-antecedentes-afecciones.dto';
import { GetAllAntecedentesAfeccionesDto } from './dto/getall-antecedentes-afecciones.dto';
import { DeleteEnableAntecedentesAfeccionesDto } from './dto/delete-enable-antecedentes-afecciones.dto';
import { AntecedentesAfeccionesService } from './antecedentes-afecciones.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class AntecedentesAfeccionesController {
    private request;
    private readonly antecedentesAfeccionesService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, antecedentesAfeccionesService: AntecedentesAfeccionesService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllAntecedentesAfeccionesDto): Promise<any>;
    create(res: any, createAntecedentesAfeccionesDto: CreateAntecedentesAfeccionesDto): Promise<ResultDto>;
    update(res: any, updateAntecedentesAfeccionesDto: UpdateAntecedentesAfeccionesDto): Promise<any>;
    remove(res: any, deleteEnableAntecedentesAfeccionesDto: DeleteEnableAntecedentesAfeccionesDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableAntecedentesAfeccionesDto: DeleteEnableAntecedentesAfeccionesDto, usuario: number): Promise<any>;
}
