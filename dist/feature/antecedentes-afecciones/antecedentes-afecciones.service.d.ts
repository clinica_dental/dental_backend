import { Request } from 'express';
import { AntecedentesAfecciones } from 'src/database/entities/AntecedentesAfecciones';
import { CreateAntecedentesAfeccionesDto } from './dto/create-antecedentes-afecciones.dto';
import { UpdateAntecedentesAfeccionesDto } from './dto/update-antecedentes-afecciones.dto';
import { GetAllAntecedentesAfeccionesDto } from './dto/getall-antecedentes-afecciones.dto';
import { DeleteEnableAntecedentesAfeccionesDto } from './dto/delete-enable-antecedentes-afecciones.dto';
import { EntityManager } from 'typeorm';
import { AntecedenteService } from '../antecedente/antecedente.service';
export declare class AntecedentesAfeccionesService {
    private request;
    private readonly antecedenteService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, antecedenteService: AntecedenteService);
    private validations;
    findAll(query: GetAllAntecedentesAfeccionesDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createAntecedentesAfeccionesDto: CreateAntecedentesAfeccionesDto, manager: EntityManager): Promise<AntecedentesAfecciones>;
    update(updateAntecedentesAfeccionesDto: UpdateAntecedentesAfeccionesDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableAntecedentesAfeccionesDto: DeleteEnableAntecedentesAfeccionesDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableAntecedentesAfeccionesDto: DeleteEnableAntecedentesAfeccionesDto, manager: EntityManager): Promise<any>;
}
