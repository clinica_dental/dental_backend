"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AntecedentesAfeccionesController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesAfeccionesController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_antecedentes_afecciones_dto_1 = require("./dto/create-antecedentes-afecciones.dto");
const update_antecedentes_afecciones_dto_1 = require("./dto/update-antecedentes-afecciones.dto");
const getall_antecedentes_afecciones_dto_1 = require("./dto/getall-antecedentes-afecciones.dto");
const delete_enable_antecedentes_afecciones_dto_1 = require("./dto/delete-enable-antecedentes-afecciones.dto");
const antecedentes_afecciones_service_1 = require("./antecedentes-afecciones.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let AntecedentesAfeccionesController = AntecedentesAfeccionesController_1 = class AntecedentesAfeccionesController {
    constructor(request, antecedentesAfeccionesService, customTransaction) {
        this.request = request;
        this.antecedentesAfeccionesService = antecedentesAfeccionesService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(AntecedentesAfeccionesController_1.name);
    }
    async findAll(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.antecedentesAfeccionesService.findAll(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createAntecedentesAfeccionesDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesAfecciones = await this.antecedentesAfeccionesService.create(createAntecedentesAfeccionesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedentesAfecciones.antafe_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updateAntecedentesAfeccionesDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesAfecciones = await this.antecedentesAfeccionesService.update(updateAntecedentesAfeccionesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updateAntecedentesAfeccionesDto.antafe_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnableAntecedentesAfeccionesDto, usuario) {
        try {
            deleteEnableAntecedentesAfeccionesDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesAfecciones = await this.antecedentesAfeccionesService.remove(deleteEnableAntecedentesAfeccionesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedentesAfecciones.antafe_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnableAntecedentesAfeccionesDto, usuario) {
        try {
            deleteEnableAntecedentesAfeccionesDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesAfecciones = await this.antecedentesAfeccionesService.enable(deleteEnableAntecedentesAfeccionesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedentesAfecciones.antafe_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_antecedentes_afecciones_dto_1.GetAllAntecedentesAfeccionesDto]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesController.prototype, "findAll", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_antecedentes_afecciones_dto_1.CreateAntecedentesAfeccionesDto]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_antecedentes_afecciones_dto_1.UpdateAntecedentesAfeccionesDto]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':antafe_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_antecedentes_afecciones_dto_1.DeleteEnableAntecedentesAfeccionesDto, Number]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:antafe_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_antecedentes_afecciones_dto_1.DeleteEnableAntecedentesAfeccionesDto, Number]),
    __metadata("design:returntype", Promise)
], AntecedentesAfeccionesController.prototype, "enable", null);
AntecedentesAfeccionesController = AntecedentesAfeccionesController_1 = __decorate([
    (0, swagger_1.ApiTags)('AntecedentesAfecciones'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('antecedentes-afecciones'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, antecedentes_afecciones_service_1.AntecedentesAfeccionesService, CustomTransaction_1.CustomTransaction])
], AntecedentesAfeccionesController);
exports.AntecedentesAfeccionesController = AntecedentesAfeccionesController;
//# sourceMappingURL=antecedentes-afecciones.controller.js.map