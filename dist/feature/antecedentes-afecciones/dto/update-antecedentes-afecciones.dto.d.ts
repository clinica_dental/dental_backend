import { CreateAntecedentesAfeccionesDto } from './create-antecedentes-afecciones.dto';
export declare class UpdateAntecedentesAfeccionesDto extends CreateAntecedentesAfeccionesDto {
    antafe_codigo: number;
}
