import { Request } from 'express';
import { GetAllEspecialidadDto, GetAllEstadoCivilDto, GetAllGeneroDto, GetAllTipoAfeccionDto, GetAllTipoDocumentoDto, GetAllTipoTratamientoDto } from './dto/getall-parametricas.dto';
import { ParametricasService } from './parametricas.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class ParametricasController {
    private request;
    private readonly parametricasService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, parametricasService: ParametricasService, customTransaction: CustomTransaction);
    findAllTipoDocumento(res: any, query: GetAllTipoDocumentoDto): Promise<any>;
    findAllGenero(res: any, query: GetAllGeneroDto): Promise<any>;
    findAllEstadoCivil(res: any, query: GetAllEstadoCivilDto): Promise<any>;
    findAllTipoAfeccion(res: any, query: GetAllTipoAfeccionDto): Promise<any>;
    findAllTipoTratamiento(res: any, query: GetAllTipoTratamientoDto): Promise<any>;
    findAllEspecialidad(res: any, query: GetAllEspecialidadDto): Promise<any>;
}
