"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllEspecialidadDto = exports.GetAllTipoTratamientoDto = exports.GetAllTipoAfeccionDto = exports.GetAllEstadoCivilDto = exports.GetAllGeneroDto = exports.GetAllTipoDocumentoDto = void 0;
const class_validator_1 = require("class-validator");
const IsCodesQuery_1 = require("../../../common/decorator/IsCodesQuery");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
class GetAllTipoDocumentoDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllTipoDocumentoDto.prototype, "tipdoc_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllTipoDocumentoDto.prototype, "tipdoc_estado", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: 'El usuario no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: 'El usuario no cumple con el valor maximo' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], GetAllTipoDocumentoDto.prototype, "usuario", void 0);
exports.GetAllTipoDocumentoDto = GetAllTipoDocumentoDto;
class GetAllGeneroDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllGeneroDto.prototype, "gen_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllGeneroDto.prototype, "gen_estado", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: 'El usuario no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: 'El usuario no cumple con el valor maximo' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], GetAllGeneroDto.prototype, "usuario", void 0);
exports.GetAllGeneroDto = GetAllGeneroDto;
class GetAllEstadoCivilDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllEstadoCivilDto.prototype, "estciv_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllEstadoCivilDto.prototype, "estciv_estado", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: 'El usuario no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: 'El usuario no cumple con el valor maximo' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], GetAllEstadoCivilDto.prototype, "usuario", void 0);
exports.GetAllEstadoCivilDto = GetAllEstadoCivilDto;
class GetAllTipoAfeccionDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllTipoAfeccionDto.prototype, "tipafe_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllTipoAfeccionDto.prototype, "tipafe_estado", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: 'El usuario no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: 'El usuario no cumple con el valor maximo' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], GetAllTipoAfeccionDto.prototype, "usuario", void 0);
exports.GetAllTipoAfeccionDto = GetAllTipoAfeccionDto;
class GetAllTipoTratamientoDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllTipoTratamientoDto.prototype, "tiptra_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllTipoTratamientoDto.prototype, "tiptra_estado", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: 'El usuario no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: 'El usuario no cumple con el valor maximo' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], GetAllTipoTratamientoDto.prototype, "usuario", void 0);
exports.GetAllTipoTratamientoDto = GetAllTipoTratamientoDto;
class GetAllEspecialidadDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllEspecialidadDto.prototype, "esp_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllEspecialidadDto.prototype, "esp_estado", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: 'El usuario no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: 'El usuario no cumple con el valor maximo' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], GetAllEspecialidadDto.prototype, "usuario", void 0);
exports.GetAllEspecialidadDto = GetAllEspecialidadDto;
//# sourceMappingURL=getall-parametricas.dto.js.map