export declare class GetAllTipoDocumentoDto {
    tipdoc_codigo: string;
    tipdoc_estado: string;
    usuario: number;
}
export declare class GetAllGeneroDto {
    gen_codigo: string;
    gen_estado: string;
    usuario: number;
}
export declare class GetAllEstadoCivilDto {
    estciv_codigo: string;
    estciv_estado: string;
    usuario: number;
}
export declare class GetAllTipoAfeccionDto {
    tipafe_codigo: string;
    tipafe_estado: string;
    usuario: number;
}
export declare class GetAllTipoTratamientoDto {
    tiptra_codigo: string;
    tiptra_estado: string;
    usuario: number;
}
export declare class GetAllEspecialidadDto {
    esp_codigo: string;
    esp_estado: string;
    usuario: number;
}
