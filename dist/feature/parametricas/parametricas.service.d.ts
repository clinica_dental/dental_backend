import { Request } from 'express';
import { GetAllTipoDocumentoDto, GetAllGeneroDto, GetAllEstadoCivilDto, GetAllTipoAfeccionDto, GetAllTipoTratamientoDto, GetAllEspecialidadDto } from './dto/getall-parametricas.dto';
import { EntityManager } from 'typeorm';
export declare class ParametricasService {
    private request;
    private readonly logger;
    private message_custom;
    constructor(request: Request);
    findAllTipoDocumento(query: GetAllTipoDocumentoDto, manager: EntityManager): Promise<any>;
    findAllGenero(query: GetAllGeneroDto, manager: EntityManager): Promise<any>;
    findAllEstadoCivil(query: GetAllEstadoCivilDto, manager: EntityManager): Promise<any>;
    findAllTipoAfeccion(query: GetAllTipoAfeccionDto, manager: EntityManager): Promise<any>;
    findAllTipoTratamiento(query: GetAllTipoTratamientoDto, manager: EntityManager): Promise<any>;
    findAllEspecialidad(query: GetAllEspecialidadDto, manager: EntityManager): Promise<any>;
}
