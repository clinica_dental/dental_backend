"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var ParametricasController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParametricasController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const getall_parametricas_dto_1 = require("./dto/getall-parametricas.dto");
const parametricas_service_1 = require("./parametricas.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let ParametricasController = ParametricasController_1 = class ParametricasController {
    constructor(request, parametricasService, customTransaction) {
        this.request = request;
        this.parametricasService = parametricasService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(ParametricasController_1.name);
    }
    async findAllTipoDocumento(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.parametricasService.findAllTipoDocumento(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllGenero(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.parametricasService.findAllGenero(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllEstadoCivil(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.parametricasService.findAllEstadoCivil(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllTipoAfeccion(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.parametricasService.findAllTipoAfeccion(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllTipoTratamiento(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.parametricasService.findAllTipoTratamiento(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllEspecialidad(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.parametricasService.findAllEspecialidad(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)('tipo-documento'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_parametricas_dto_1.GetAllTipoDocumentoDto]),
    __metadata("design:returntype", Promise)
], ParametricasController.prototype, "findAllTipoDocumento", null);
__decorate([
    (0, common_1.Get)('genero'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_parametricas_dto_1.GetAllGeneroDto]),
    __metadata("design:returntype", Promise)
], ParametricasController.prototype, "findAllGenero", null);
__decorate([
    (0, common_1.Get)('estado-civil'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_parametricas_dto_1.GetAllEstadoCivilDto]),
    __metadata("design:returntype", Promise)
], ParametricasController.prototype, "findAllEstadoCivil", null);
__decorate([
    (0, common_1.Get)('tipo-afeccion'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_parametricas_dto_1.GetAllTipoAfeccionDto]),
    __metadata("design:returntype", Promise)
], ParametricasController.prototype, "findAllTipoAfeccion", null);
__decorate([
    (0, common_1.Get)('tipo-tratamiento'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_parametricas_dto_1.GetAllTipoTratamientoDto]),
    __metadata("design:returntype", Promise)
], ParametricasController.prototype, "findAllTipoTratamiento", null);
__decorate([
    (0, common_1.Get)('especialidad'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_parametricas_dto_1.GetAllEspecialidadDto]),
    __metadata("design:returntype", Promise)
], ParametricasController.prototype, "findAllEspecialidad", null);
ParametricasController = ParametricasController_1 = __decorate([
    (0, swagger_1.ApiTags)('Parametricas'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('parametricas'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, parametricas_service_1.ParametricasService, CustomTransaction_1.CustomTransaction])
], ParametricasController);
exports.ParametricasController = ParametricasController;
//# sourceMappingURL=parametricas.controller.js.map