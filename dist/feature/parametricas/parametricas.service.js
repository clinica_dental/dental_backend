"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var ParametricasService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParametricasService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const customService_1 = require("../../common/exception/customService");
const getall_parametricas_dto_1 = require("./dto/getall-parametricas.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
let ParametricasService = ParametricasService_1 = class ParametricasService {
    constructor(request) {
        this.request = request;
        this.message_custom = 'PARAMETRICAS -';
        this.logger = this.request.logger;
        this.logger.setContext(ParametricasService_1.name);
    }
    async findAllTipoDocumento(query, manager) {
        try {
            let sql = `
      SELECT 
        t.tipdoc_codigo, 
        t.tipdoc_nombre,
        t.tipdoc_estado, 
        e.est_nombre AS tipdoc_estado_descripcion 
      FROM parametricas.tipo_documento t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.tipdoc_estado
      WHERE TRUE
      ${query.tipdoc_codigo ? `AND t.tipdoc_codigo IN ${query.tipdoc_codigo}` : ''}
      ${query.tipdoc_estado ? `AND t.tipdoc_estado IN ${query.tipdoc_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllGenero(query, manager) {
        try {
            let sql = `
      SELECT 
        t.gen_codigo, 
        t.gen_nombre,
        t.gen_estado, 
        e.est_nombre AS gen_estado_descripcion 
      FROM parametricas.genero t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.gen_estado
      WHERE TRUE
      ${query.gen_codigo ? `AND t.gen_codigo IN ${query.gen_codigo}` : ''}
      ${query.gen_estado ? `AND t.gen_estado IN ${query.gen_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllEstadoCivil(query, manager) {
        try {
            let sql = `
      SELECT 
        t.estciv_codigo, 
        t.estciv_nombre,
        t.estciv_estado,
        e.est_nombre AS estciv_estado_descripcion 
      FROM parametricas.estado_civil t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.estciv_estado
      WHERE TRUE
      ${query.estciv_codigo ? `AND t.estciv_codigo IN ${query.estciv_codigo}` : ''}
      ${query.estciv_estado ? `AND t.estciv_estado IN ${query.estciv_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllTipoAfeccion(query, manager) {
        try {
            let sql = `
      SELECT 
        t.tipafe_codigo, 
        t.tipafe_nombre,
        t.tipafe_descripcion,
        t.tipafe_estado,
        e.est_nombre AS tipafe_estado_descripcion 
      FROM parametricas.tipo_afeccion t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.tipafe_estado
      WHERE TRUE
      ${query.tipafe_codigo ? `AND t.tipafe_codigo IN ${query.tipafe_codigo}` : ''}
      ${query.tipafe_estado ? `AND t.tipafe_estado IN ${query.tipafe_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllTipoTratamiento(query, manager) {
        try {
            let sql = `
      SELECT 
        t.tiptra_codigo, 
        t.tiptra_nombre,
        t.tiptra_descripcion,
        t.tiptra_estado,
        e.est_nombre AS tiptra_estado_descripcion 
      FROM parametricas.tipo_tratamiento t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.tiptra_estado
      WHERE TRUE
      ${query.tiptra_codigo ? `AND t.tiptra_codigo IN ${query.tiptra_codigo}` : ''}
      ${query.tiptra_estado ? `AND t.tiptra_estado IN ${query.tiptra_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllEspecialidad(query, manager) {
        try {
            let sql = `
      SELECT 
        t.esp_codigo, 
        t.esp_nombre,
        t.esp_estado,
        e.est_nombre AS tiptra_estado_descripcion 
      FROM parametricas.especialidad t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.esp_estado
      WHERE TRUE
      ${query.esp_codigo ? `AND t.esp_codigo IN ${query.esp_codigo}` : ''}
      ${query.esp_estado ? `AND t.esp_estado IN ${query.esp_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_parametricas_dto_1.GetAllTipoDocumentoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ParametricasService.prototype, "findAllTipoDocumento", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_parametricas_dto_1.GetAllGeneroDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ParametricasService.prototype, "findAllGenero", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_parametricas_dto_1.GetAllEstadoCivilDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ParametricasService.prototype, "findAllEstadoCivil", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_parametricas_dto_1.GetAllTipoAfeccionDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ParametricasService.prototype, "findAllTipoAfeccion", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_parametricas_dto_1.GetAllTipoTratamientoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ParametricasService.prototype, "findAllTipoTratamiento", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_parametricas_dto_1.GetAllEspecialidadDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ParametricasService.prototype, "findAllEspecialidad", null);
ParametricasService = ParametricasService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object])
], ParametricasService);
exports.ParametricasService = ParametricasService;
//# sourceMappingURL=parametricas.service.js.map