import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateAntecedentesPersonalesDto } from './dto/create-antecedentes-personales.dto';
import { UpdateAntecedentesPersonalesDto } from './dto/update-antecedentes-personales.dto';
import { GetAllAntecedentesPersonalesDto } from './dto/getall-antecedentes-personales.dto';
import { DeleteEnableAntecedentesPersonalesDto } from './dto/delete-enable-antecedentes-personales.dto';
import { AntecedentesPersonalesService } from './antecedentes-personales.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class AntecedentesPersonalesController {
    private request;
    private readonly antecedentesPersonalesService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, antecedentesPersonalesService: AntecedentesPersonalesService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllAntecedentesPersonalesDto): Promise<any>;
    create(res: any, createAntecedentesPersonalesDto: CreateAntecedentesPersonalesDto): Promise<ResultDto>;
    update(res: any, updateAntecedentesPersonalesDto: UpdateAntecedentesPersonalesDto): Promise<any>;
    remove(res: any, deleteEnableAntecedentesPersonalesDto: DeleteEnableAntecedentesPersonalesDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableAntecedentesPersonalesDto: DeleteEnableAntecedentesPersonalesDto, usuario: number): Promise<any>;
}
