export declare class CreateAntecedentesPersonalesDto {
    ant_codigo: number;
    antper_hopitalizacion: string;
    antper_atencion: string;
    antper_alergias: string;
    antper_infecciosos: string;
    antper_alteraciones: string;
    antper_tratamiento: string;
    antper_medicacion: string;
    usuario: number;
}
