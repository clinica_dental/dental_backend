import { CreateAntecedentesPersonalesDto } from './create-antecedentes-personales.dto';
export declare class UpdateAntecedentesPersonalesDto extends CreateAntecedentesPersonalesDto {
    antper_codigo: number;
}
