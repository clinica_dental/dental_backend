import { Request } from 'express';
import { AntecedentesPersonales } from 'src/database/entities/AntecedentesPersonales';
import { CreateAntecedentesPersonalesDto } from './dto/create-antecedentes-personales.dto';
import { UpdateAntecedentesPersonalesDto } from './dto/update-antecedentes-personales.dto';
import { GetAllAntecedentesPersonalesDto } from './dto/getall-antecedentes-personales.dto';
import { DeleteEnableAntecedentesPersonalesDto } from './dto/delete-enable-antecedentes-personales.dto';
import { EntityManager } from 'typeorm';
import { AntecedenteService } from '../antecedente/antecedente.service';
export declare class AntecedentesPersonalesService {
    private request;
    private readonly antecedenteService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, antecedenteService: AntecedenteService);
    private validations;
    findAll(query: GetAllAntecedentesPersonalesDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createAntecedentesPersonalesDto: CreateAntecedentesPersonalesDto, manager: EntityManager): Promise<AntecedentesPersonales>;
    update(updateAntecedentesPersonalesDto: UpdateAntecedentesPersonalesDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableAntecedentesPersonalesDto: DeleteEnableAntecedentesPersonalesDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableAntecedentesPersonalesDto: DeleteEnableAntecedentesPersonalesDto, manager: EntityManager): Promise<any>;
}
