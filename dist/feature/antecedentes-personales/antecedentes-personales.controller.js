"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AntecedentesPersonalesController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesPersonalesController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_antecedentes_personales_dto_1 = require("./dto/create-antecedentes-personales.dto");
const update_antecedentes_personales_dto_1 = require("./dto/update-antecedentes-personales.dto");
const getall_antecedentes_personales_dto_1 = require("./dto/getall-antecedentes-personales.dto");
const delete_enable_antecedentes_personales_dto_1 = require("./dto/delete-enable-antecedentes-personales.dto");
const antecedentes_personales_service_1 = require("./antecedentes-personales.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let AntecedentesPersonalesController = AntecedentesPersonalesController_1 = class AntecedentesPersonalesController {
    constructor(request, antecedentesPersonalesService, customTransaction) {
        this.request = request;
        this.antecedentesPersonalesService = antecedentesPersonalesService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(AntecedentesPersonalesController_1.name);
    }
    async findAll(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.antecedentesPersonalesService.findAll(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createAntecedentesPersonalesDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesPersonales = await this.antecedentesPersonalesService.create(createAntecedentesPersonalesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedentesPersonales.antper_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updateAntecedentesPersonalesDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesPersonales = await this.antecedentesPersonalesService.update(updateAntecedentesPersonalesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updateAntecedentesPersonalesDto.antper_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnableAntecedentesPersonalesDto, usuario) {
        try {
            deleteEnableAntecedentesPersonalesDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesPersonales = await this.antecedentesPersonalesService.remove(deleteEnableAntecedentesPersonalesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedentesPersonales.antper_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnableAntecedentesPersonalesDto, usuario) {
        try {
            deleteEnableAntecedentesPersonalesDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultAntecedentesPersonales = await this.antecedentesPersonalesService.enable(deleteEnableAntecedentesPersonalesDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedentesPersonales.antper_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_antecedentes_personales_dto_1.GetAllAntecedentesPersonalesDto]),
    __metadata("design:returntype", Promise)
], AntecedentesPersonalesController.prototype, "findAll", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_antecedentes_personales_dto_1.CreateAntecedentesPersonalesDto]),
    __metadata("design:returntype", Promise)
], AntecedentesPersonalesController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_antecedentes_personales_dto_1.UpdateAntecedentesPersonalesDto]),
    __metadata("design:returntype", Promise)
], AntecedentesPersonalesController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':antper_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_antecedentes_personales_dto_1.DeleteEnableAntecedentesPersonalesDto, Number]),
    __metadata("design:returntype", Promise)
], AntecedentesPersonalesController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:antper_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_antecedentes_personales_dto_1.DeleteEnableAntecedentesPersonalesDto, Number]),
    __metadata("design:returntype", Promise)
], AntecedentesPersonalesController.prototype, "enable", null);
AntecedentesPersonalesController = AntecedentesPersonalesController_1 = __decorate([
    (0, swagger_1.ApiTags)('AntecedentesPersonales'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('antecedentes-personales'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, antecedentes_personales_service_1.AntecedentesPersonalesService, CustomTransaction_1.CustomTransaction])
], AntecedentesPersonalesController);
exports.AntecedentesPersonalesController = AntecedentesPersonalesController;
//# sourceMappingURL=antecedentes-personales.controller.js.map