import { CreateAntecedentesPatologicosDto } from './create-antecedentes-patologicos.dto';
export declare class UpdateAntecedentesPatologicosDto extends CreateAntecedentesPatologicosDto {
    antpat_codigo: number;
}
