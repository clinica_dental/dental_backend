import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateAntecedentesPatologicosDto } from './dto/create-antecedentes-patologicos.dto';
import { UpdateAntecedentesPatologicosDto } from './dto/update-antecedentes-patologicos.dto';
import { GetAllAntecedentesPatologicosDto } from './dto/getall-antecedentes-patologicos.dto';
import { DeleteEnableAntecedentesPatologicosDto } from './dto/delete-enable-antecedentes-patologicos.dto';
import { AntecedentesPatologicosService } from './antecedentes-patologicos.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class AntecedentesPatologicosController {
    private request;
    private readonly antecedentesPatologicosService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, antecedentesPatologicosService: AntecedentesPatologicosService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllAntecedentesPatologicosDto): Promise<any>;
    create(res: any, createAntecedentesPatologicosDto: CreateAntecedentesPatologicosDto): Promise<ResultDto>;
    update(res: any, updateAntecedentesPatologicosDto: UpdateAntecedentesPatologicosDto): Promise<any>;
    remove(res: any, deleteEnableAntecedentesPatologicosDto: DeleteEnableAntecedentesPatologicosDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableAntecedentesPatologicosDto: DeleteEnableAntecedentesPatologicosDto, usuario: number): Promise<any>;
}
