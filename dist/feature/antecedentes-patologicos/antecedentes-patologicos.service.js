"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AntecedentesPatologicosService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesPatologicosService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const AntecedentesPatologicos_1 = require("../../database/entities/AntecedentesPatologicos");
const create_antecedentes_patologicos_dto_1 = require("./dto/create-antecedentes-patologicos.dto");
const update_antecedentes_patologicos_dto_1 = require("./dto/update-antecedentes-patologicos.dto");
const getall_antecedentes_patologicos_dto_1 = require("./dto/getall-antecedentes-patologicos.dto");
const delete_enable_antecedentes_patologicos_dto_1 = require("./dto/delete-enable-antecedentes-patologicos.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const getall_antecedente_dto_1 = require("../antecedente/dto/getall-antecedente.dto");
const antecedente_service_1 = require("../antecedente/antecedente.service");
let AntecedentesPatologicosService = AntecedentesPatologicosService_1 = class AntecedentesPatologicosService {
    constructor(request, antecedenteService) {
        this.request = request;
        this.antecedenteService = antecedenteService;
        this.message_custom = 'ANTECEDENTES PATOLOGICOS -';
        this.logger = this.request.logger;
        this.logger.setContext(AntecedentesPatologicosService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const antecedentesPatologicos = new AntecedentesPatologicos_1.AntecedentesPatologicos();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(AntecedentesPatologicos_1.AntecedentesPatologicos, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.antpat_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllAntecedenteDto = new getall_antecedente_dto_1.GetAllAntecedenteDto();
                        getAllAntecedenteDto.ant_codigo = `(${params.ant_codigo})`;
                        getAllAntecedenteDto.ant_estado = '(1)';
                        await this.antecedenteService.findAll(getAllAntecedenteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE ANTECEDENTE NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(antecedentesPatologicos, params);
                    antecedentesPatologicos.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    antecedentesPatologicos.antpat_codigo = params.antpat_codigo;
                    antecedentesPatologicos.antpat_estado = estados;
                    findResult = await manager.findOne(AntecedentesPatologicos_1.AntecedentesPatologicos, { where: antecedentesPatologicos });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.antpat_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllAntecedenteDto = new getall_antecedente_dto_1.GetAllAntecedenteDto();
                        getAllAntecedenteDto.ant_codigo = `(${params.ant_codigo})`;
                        getAllAntecedenteDto.ant_estado = '(1)';
                        await this.antecedenteService.findAll(getAllAntecedenteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE ANTECEDENTE NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(antecedentesPatologicos, params);
                    antecedentesPatologicos.usuario_modificacion = usuario;
                    antecedentesPatologicos.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    antecedentesPatologicos.antpat_codigo = params.antpat_codigo;
                    antecedentesPatologicos.antpat_estado = estados;
                    findResult = await manager.findOne(AntecedentesPatologicos_1.AntecedentesPatologicos, { where: antecedentesPatologicos });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.antpat_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    antecedentesPatologicos.usuario_baja = params.usuario;
                    antecedentesPatologicos.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    antecedentesPatologicos.antpat_codigo = params.antpat_codigo;
                    antecedentesPatologicos.antpat_estado = estados;
                    findResult = await manager.findOne(AntecedentesPatologicos_1.AntecedentesPatologicos, { where: antecedentesPatologicos });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.antpat_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    antecedentesPatologicos.usuario_modificacion = params.usuario;
                    antecedentesPatologicos.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return antecedentesPatologicos;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.antpat_codigo, 
        t.ant_codigo, 
        t.antpat_familiares, 
        t.antpat_estado, 
        e.est_nombre AS antpat_estado_descripcion 
      FROM registro.antecedentes_patologicos t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.antpat_estado
      WHERE TRUE
      ${query.antpat_codigo ? `AND t.antpat_codigo IN ${query.antpat_codigo}` : ''}
      ${query.antpat_estado ? `AND t.antpat_estado IN ${query.antpat_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(AntecedentesPatologicos_1.AntecedentesPatologicos, { where: { antpat_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createAntecedentesPatologicosDto, manager) {
        try {
            const antecedentesPatologicos = await this.validations(1, manager, createAntecedentesPatologicosDto);
            const sql = 'SELECT COALESCE(MAX(antecedentes_patologicos.antpat_codigo), 0) + 1 codigo FROM registro.antecedentes_patologicos;';
            const codeResult = await manager.query(sql);
            antecedentesPatologicos.antpat_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(antecedentesPatologicos);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateAntecedentesPatologicosDto, manager) {
        try {
            const antecedentesPatologicos = await this.validations(2, manager, updateAntecedentesPatologicosDto);
            const resultQuery = await manager.update(AntecedentesPatologicos_1.AntecedentesPatologicos, antecedentesPatologicos.antpat_codigo, antecedentesPatologicos);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableAntecedentesPatologicosDto, manager) {
        try {
            const antecedentesPatologicos = await this.validations(3, manager, deleteEnableAntecedentesPatologicosDto);
            const resultQuery = await manager.update(AntecedentesPatologicos_1.AntecedentesPatologicos, antecedentesPatologicos.antpat_codigo, antecedentesPatologicos);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableAntecedentesPatologicosDto, manager) {
        try {
            const antecedentesPatologicos = await this.validations(4, manager, deleteEnableAntecedentesPatologicosDto);
            const resultQuery = await manager.update(AntecedentesPatologicos_1.AntecedentesPatologicos, antecedentesPatologicos.antpat_codigo, antecedentesPatologicos);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], AntecedentesPatologicosService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_antecedentes_patologicos_dto_1.GetAllAntecedentesPatologicosDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesPatologicosService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesPatologicosService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_antecedentes_patologicos_dto_1.CreateAntecedentesPatologicosDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesPatologicosService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_antecedentes_patologicos_dto_1.UpdateAntecedentesPatologicosDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesPatologicosService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedentes_patologicos_dto_1.DeleteEnableAntecedentesPatologicosDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesPatologicosService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedentes_patologicos_dto_1.DeleteEnableAntecedentesPatologicosDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesPatologicosService.prototype, "enable", null);
AntecedentesPatologicosService = AntecedentesPatologicosService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, antecedente_service_1.AntecedenteService])
], AntecedentesPatologicosService);
exports.AntecedentesPatologicosService = AntecedentesPatologicosService;
//# sourceMappingURL=antecedentes-patologicos.service.js.map