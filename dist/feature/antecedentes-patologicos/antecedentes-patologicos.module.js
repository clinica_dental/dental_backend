"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesPatologicosModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
const index_1 = require("../../database/entities/index");
const antecedentes_patologicos_controller_1 = require("./antecedentes-patologicos.controller");
const antecedentes_patologicos_service_1 = require("./antecedentes-patologicos.service");
let AntecedentesPatologicosModule = class AntecedentesPatologicosModule {
};
AntecedentesPatologicosModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature(index_1.entities)],
        providers: [antecedentes_patologicos_service_1.AntecedentesPatologicosService, CustomTransaction_1.CustomTransaction],
        controllers: [antecedentes_patologicos_controller_1.AntecedentesPatologicosController],
        exports: [antecedentes_patologicos_service_1.AntecedentesPatologicosService],
    })
], AntecedentesPatologicosModule);
exports.AntecedentesPatologicosModule = AntecedentesPatologicosModule;
//# sourceMappingURL=antecedentes-patologicos.module.js.map