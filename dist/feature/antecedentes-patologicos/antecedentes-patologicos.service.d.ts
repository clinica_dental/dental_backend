import { Request } from 'express';
import { AntecedentesPatologicos } from 'src/database/entities/AntecedentesPatologicos';
import { CreateAntecedentesPatologicosDto } from './dto/create-antecedentes-patologicos.dto';
import { UpdateAntecedentesPatologicosDto } from './dto/update-antecedentes-patologicos.dto';
import { GetAllAntecedentesPatologicosDto } from './dto/getall-antecedentes-patologicos.dto';
import { DeleteEnableAntecedentesPatologicosDto } from './dto/delete-enable-antecedentes-patologicos.dto';
import { EntityManager } from 'typeorm';
import { AntecedenteService } from '../antecedente/antecedente.service';
export declare class AntecedentesPatologicosService {
    private request;
    private readonly antecedenteService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, antecedenteService: AntecedenteService);
    private validations;
    findAll(query: GetAllAntecedentesPatologicosDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createAntecedentesPatologicosDto: CreateAntecedentesPatologicosDto, manager: EntityManager): Promise<AntecedentesPatologicos>;
    update(updateAntecedentesPatologicosDto: UpdateAntecedentesPatologicosDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableAntecedentesPatologicosDto: DeleteEnableAntecedentesPatologicosDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableAntecedentesPatologicosDto: DeleteEnableAntecedentesPatologicosDto, manager: EntityManager): Promise<any>;
}
