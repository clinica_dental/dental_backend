"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var PacienteService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.PacienteService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const Paciente_1 = require("../../database/entities/Paciente");
const create_paciente_dto_1 = require("./dto/create-paciente.dto");
const update_paciente_dto_1 = require("./dto/update-paciente.dto");
const getall_paciente_dto_1 = require("./dto/getall-paciente.dto");
const delete_enable_paciente_dto_1 = require("./dto/delete-enable-paciente.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const persona_service_1 = require("../persona/persona.service");
const getall_persona_dto_1 = require("../persona/dto/getall-persona.dto");
let PacienteService = PacienteService_1 = class PacienteService {
    constructor(request, personaService) {
        this.request = request;
        this.personaService = personaService;
        this.message_custom = 'PACIENTE -';
        this.logger = this.request.logger;
        this.logger.setContext(PacienteService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const paciente = new Paciente_1.Paciente();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(Paciente_1.Paciente, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.pac_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllPersonaDto = new getall_persona_dto_1.GetAllPersonaDto();
                        getAllPersonaDto.per_codigo = `(${params.per_codigo})`;
                        getAllPersonaDto.per_estado = '(1)';
                        await this.personaService.findAll(getAllPersonaDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE LA PERSONA ENVIADA NO EXISTE O ESTA INACTIVA');
                    }
                    (0, util_1.bindingObjects)(paciente, params);
                    paciente.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    paciente.pac_codigo = params.pac_codigo;
                    paciente.pac_estado = estados;
                    findResult = await manager.findOne(Paciente_1.Paciente, { where: paciente });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.pac_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllPersonaDto = new getall_persona_dto_1.GetAllPersonaDto();
                        getAllPersonaDto.per_codigo = `(${params.per_codigo})`;
                        getAllPersonaDto.per_estado = '(1)';
                        await this.personaService.findAll(getAllPersonaDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE LA PERSONA ENVIADA NO EXISTE O ESTA INACTIVA');
                    }
                    (0, util_1.bindingObjects)(paciente, params);
                    paciente.usuario_modificacion = usuario;
                    paciente.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    paciente.pac_codigo = params.pac_codigo;
                    paciente.pac_estado = estados;
                    findResult = await manager.findOne(Paciente_1.Paciente, { where: paciente });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.pac_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    paciente.usuario_baja = params.usuario;
                    paciente.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    paciente.pac_codigo = params.pac_codigo;
                    paciente.pac_estado = estados;
                    findResult = await manager.findOne(Paciente_1.Paciente, { where: paciente });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.pac_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    paciente.usuario_modificacion = params.usuario;
                    paciente.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return paciente;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.pac_codigo, 
        p.per_codigo,
        p.per_docidentidad,
        concat_ws(' ', p.per_nombres, p.per_primer_apellido, p.per_segundo_apellido) AS per_nombre_completo,
        p.per_celular, 
        p.per_correo, 
        t.estciv_codigo, 
				ec.estciv_nombre, 
        t.pac_direccion,  
        t.pac_telefono, 
        t.pac_ocupacion, 
        t.pac_lugar_nacimiento,
        t.pac_estado, 
        e.est_nombre AS pac_estado_descripcion 
      FROM registro.paciente t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.pac_estado
      LEFT JOIN registro.persona p ON p.per_codigo = t.per_codigo
      LEFT JOIN parametricas.estado_civil ec ON ec.estciv_codigo = t.estciv_codigo
      WHERE TRUE
      ${query.pac_codigo ? `AND t.pac_codigo IN ${query.pac_codigo}` : ''}
      ${query.pac_estado ? `AND t.pac_estado IN ${query.pac_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAfiliation(query, manager) {
        try {
            let sql = `
      WITH tmp_data_paciente_persona AS (
        SELECT 
          per.per_codigo,
          pac.pac_codigo 
        FROM registro.persona per
        INNER JOIN registro.paciente pac ON pac.per_codigo = per.per_codigo 
        WHERE TRUE 
        AND pac.pac_codigo IN ${query.pac_codigo}
      ), tmp_datos_paciente AS (
        SELECT 
          p.pac_codigo,
          p.per_codigo, 
          p2.per_docidentidad,  
          concat_ws(' ', p2.per_primer_apellido, p2.per_segundo_apellido, p2.per_nombres)  AS per_nombre_completo,
          to_char(p2.per_fecha_nacimiento, 'DD/MM/YYYY') AS per_fecha_nacimiento,
          p.pac_lugar_nacimiento,
          (EXTRACT (YEAR FROM current_date) - EXTRACT (YEAR FROM p2.per_fecha_nacimiento)) AS per_edad,
          p2.gen_codigo, 
          g.gen_nombre, 
          p.estciv_codigo, 
          ec.estciv_nombre, 
          p.pac_ocupacion, 
          p.pac_direccion, 
          p.pac_telefono, 
          '' AS pac_motivo_consulta -- this fiels IS missing
        FROM tmp_data_paciente_persona tdpp
        INNER JOIN registro.paciente p ON p.pac_codigo = tdpp.pac_codigo
        LEFT JOIN registro.persona p2 ON p2.per_codigo = p.per_codigo 
        LEFT JOIN parametricas.genero g ON g.gen_codigo = p2.gen_codigo 
        LEFT JOIN parametricas.estado_civil ec ON ec.estciv_codigo = p.estciv_codigo
      ), tmp_antedecentes AS (
        SELECT 
          a.pac_codigo,  
          a.ant_codigo
        FROM tmp_data_paciente_persona tdpp
        INNER JOIN registro.antecedente a ON a.pac_codigo = tdpp.pac_codigo
        WHERE TRUE 
        AND a.ant_estado = 1
      ), tmp_antedecentes_patologicos AS (
        SELECT 
          ta.pac_codigo,
          ap.antpat_codigo,  
          COALESCE (ap.antpat_familiares, '[SIN DATOS]') AS antpat_familiares
        FROM tmp_antedecentes ta
        INNER JOIN registro.antecedentes_patologicos ap ON ap.ant_codigo = ta.ant_codigo
        WHERE TRUE 
        AND ap.antpat_estado = 1
      ), tmp_antecedentes_personales AS (
        SELECT 
          ta.pac_codigo,
          ap2.antper_codigo, 
          COALESCE (ap2.antper_hopitalizacion, '[SIN DATOS]') AS antper_hopitalizacion, 
          COALESCE (ap2.antper_atencion, '[SIN DATOS]') AS antper_atencion,  
          COALESCE (ap2.antper_alergias, '[SIN DATOS]') AS antper_alergias,
          COALESCE (ap2.antper_infecciosos, '[SIN DATOS]') AS antper_infecciosos, 
          COALESCE (ap2.antper_alteraciones, '[SIN DATOS]') AS antper_alteraciones, 
          COALESCE (ap2.antper_tratamiento, '[SIN DATOS]') AS antper_tratamiento, 
          COALESCE (ap2.antper_medicacion, '[SIN DATOS]') AS antper_medicacion 
        FROM tmp_antedecentes ta
        INNER JOIN registro.antecedentes_personales ap2 ON ap2.ant_codigo = ta.ant_codigo
        WHERE TRUE 
        AND ap2.antper_estado = 1
      ), tmp_antencedentes_afecciones AS (
        SELECT 
          COALESCE (ta.pac_codigo, 0) AS pac_codigo, 
          COALESCE (aa.antafe_codigo, 0) AS antafe_codigo, 
          COALESCE (aa.tipafe_codigo, 0) AS tipafe_codigo, 
          ta2.tipafe_nombre 
        FROM parametricas.tipo_afeccion ta2
        LEFT JOIN registro.antecedentes_afecciones aa ON aa.tipafe_codigo = ta2.tipafe_codigo AND aa.antafe_estado = 1
        LEFT JOIN tmp_antedecentes ta ON ta.pac_codigo = aa.ant_codigo 
        WHERE TRUE 
        AND ta2.tipafe_codigo <> 0
        AND ta2.tipafe_estado = 1 
      ), tmp_antecedentes_dentales AS (
        SELECT 
          ta.pac_codigo, 
          ad.ant_codigo, 
          COALESCE (ad.antden_higiene, '[SIN DATOS]') AS antden_higiene, 
          COALESCE (ad.antden_respiracion, '[SIN DATOS]') AS antden_respiracion, 
          COALESCE (ad.antden_onicofagia, '[SIN DATOS]') AS antden_onicofagia, 
          COALESCE (ad.antden_queilifagia, '[SIN DATOS]') AS antden_queilifagia, 
          COALESCE (ad.antden_bruxismo, '[SIN DATOS]') AS antden_bruxismo, 
          COALESCE (ad.antden_cepillado, '[SIN DATOS]') AS antden_cepillado, 
          COALESCE (ad.antden_diagnostico, '[SIN DATOS]') AS antden_diagnostico, 
          COALESCE (ad.antden_ultimo_tratamiento, '[SIN DATOS]') AS antden_ultimo_tratamiento,
          COALESCE (ad.antden_observaciones, '[SIN DATOS]') AS antden_observaciones 
        FROM tmp_antedecentes ta 
        INNER JOIN registro.antecedentes_dentales ad ON ad.ant_codigo = ta.ant_codigo
        WHERE TRUE 
        AND ad.antden_estado = 1
      ), tmp_datos_paciente_agrupado AS (
        SELECT
          json_agg(tmp_datos_paciente) AS datos_paciente
        FROM tmp_datos_paciente
      ), tmp_antedecentes_patologicos_agrupado AS (
        SELECT
          json_agg(tmp_antedecentes_patologicos) AS antedecentes_patologicos
        FROM tmp_antedecentes_patologicos
      ), tmp_antecedentes_personales_agrupado AS (
        SELECT
          json_agg(tmp_antecedentes_personales) AS antecedentes_personales
        FROM tmp_antecedentes_personales
      ), tmp_antencedentes_afecciones_agrupado AS (
        SELECT
          json_agg(tmp_antencedentes_afecciones) AS antencedentes_afecciones
        FROM tmp_antencedentes_afecciones
      ), tmp_antecedentes_dentales_agrupado AS (
        SELECT
          json_agg(tmp_antecedentes_dentales) AS antecedentes_dentales 
        FROM tmp_antecedentes_dentales
      )
      SELECT
        COALESCE (datos_paciente, '[]'::json) AS datos_paciente,
        COALESCE (antedecentes_patologicos, '[]'::json) AS antedecentes_patologicos,
        COALESCE (antecedentes_personales, '[]'::json) AS antecedentes_personales, 
        COALESCE (antencedentes_afecciones, '[]'::json) AS antencedentes_afecciones, 
        COALESCE (antecedentes_dentales, '[]'::json) AS antecedentes_dentales
      FROM tmp_datos_paciente_agrupado, tmp_antedecentes_patologicos_agrupado, tmp_antecedentes_personales_agrupado, tmp_antencedentes_afecciones_agrupado, tmp_antecedentes_dentales_agrupado;`;
            const resultQuery = await manager.query(sql);
            resultQuery[0].datos_paciente = resultQuery[0].datos_paciente[0];
            return customService_1.CustomService.verifyingDataResult(resultQuery[0], this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(Paciente_1.Paciente, { where: { pac_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createPacienteDto, manager) {
        try {
            const paciente = await this.validations(1, manager, createPacienteDto);
            const sql = 'SELECT COALESCE(MAX(paciente.pac_codigo), 0) + 1 codigo FROM registro.paciente;';
            const codeResult = await manager.query(sql);
            paciente.pac_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(paciente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updatePacienteDto, manager) {
        try {
            const paciente = await this.validations(2, manager, updatePacienteDto);
            const resultQuery = await manager.update(Paciente_1.Paciente, paciente.pac_codigo, paciente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnablePacienteDto, manager) {
        try {
            const paciente = await this.validations(3, manager, deleteEnablePacienteDto);
            const resultQuery = await manager.update(Paciente_1.Paciente, paciente.pac_codigo, paciente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnablePacienteDto, manager) {
        try {
            const paciente = await this.validations(4, manager, deleteEnablePacienteDto);
            const resultQuery = await manager.update(Paciente_1.Paciente, paciente.pac_codigo, paciente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_paciente_dto_1.GetAllPacienteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_paciente_dto_1.GetAllPacienteAfiliationDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "findAfiliation", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_paciente_dto_1.CreatePacienteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_paciente_dto_1.UpdatePacienteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_paciente_dto_1.DeleteEnablePacienteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_paciente_dto_1.DeleteEnablePacienteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PacienteService.prototype, "enable", null);
PacienteService = PacienteService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, persona_service_1.PersonaService])
], PacienteService);
exports.PacienteService = PacienteService;
//# sourceMappingURL=paciente.service.js.map