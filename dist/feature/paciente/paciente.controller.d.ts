import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreatePacienteDto } from './dto/create-paciente.dto';
import { UpdatePacienteDto } from './dto/update-paciente.dto';
import { GetAllPacienteAfiliationDto, GetAllPacienteDto } from './dto/getall-paciente.dto';
import { DeleteEnablePacienteDto } from './dto/delete-enable-paciente.dto';
import { PacienteService } from './paciente.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class PacienteController {
    private request;
    private readonly pacienteService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, pacienteService: PacienteService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllPacienteDto): Promise<any>;
    findAfiliation(res: any, query: GetAllPacienteAfiliationDto): Promise<any>;
    create(res: any, createPacienteDto: CreatePacienteDto): Promise<ResultDto>;
    update(res: any, updatePacienteDto: UpdatePacienteDto): Promise<any>;
    remove(res: any, deleteEnablePacienteDto: DeleteEnablePacienteDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnablePacienteDto: DeleteEnablePacienteDto, usuario: number): Promise<any>;
}
