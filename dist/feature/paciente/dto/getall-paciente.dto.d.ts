export declare class GetAllPacienteDto {
    pac_codigo: string;
    pac_estado: string;
    usuario: number;
}
export declare class GetAllPacienteAfiliationDto {
    pac_codigo: string;
    usuario: number;
}
