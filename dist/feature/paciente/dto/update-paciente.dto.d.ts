import { CreatePacienteDto } from './create-paciente.dto';
export declare class UpdatePacienteDto extends CreatePacienteDto {
    pac_codigo: number;
}
