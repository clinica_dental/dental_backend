export declare class CreatePacienteDto {
    per_codigo: number;
    estciv_codigo: number;
    pac_direccion: string;
    pac_telefono: string;
    pac_ocupacion: string;
    pac_lugar_nacimiento: string;
    usuario: number;
}
