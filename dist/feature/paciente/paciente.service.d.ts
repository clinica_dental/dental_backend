import { Request } from 'express';
import { Paciente } from 'src/database/entities/Paciente';
import { CreatePacienteDto } from './dto/create-paciente.dto';
import { UpdatePacienteDto } from './dto/update-paciente.dto';
import { GetAllPacienteAfiliationDto, GetAllPacienteDto } from './dto/getall-paciente.dto';
import { DeleteEnablePacienteDto } from './dto/delete-enable-paciente.dto';
import { EntityManager } from 'typeorm';
import { PersonaService } from '../persona/persona.service';
export declare class PacienteService {
    private request;
    private readonly personaService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, personaService: PersonaService);
    private validations;
    findAll(query: GetAllPacienteDto, manager: EntityManager): Promise<any>;
    findAfiliation(query: GetAllPacienteAfiliationDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createPacienteDto: CreatePacienteDto, manager: EntityManager): Promise<Paciente>;
    update(updatePacienteDto: UpdatePacienteDto, manager: EntityManager): Promise<any>;
    remove(deleteEnablePacienteDto: DeleteEnablePacienteDto, manager: EntityManager): Promise<any>;
    enable(deleteEnablePacienteDto: DeleteEnablePacienteDto, manager: EntityManager): Promise<any>;
}
