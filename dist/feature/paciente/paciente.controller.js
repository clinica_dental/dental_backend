"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var PacienteController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.PacienteController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_paciente_dto_1 = require("./dto/create-paciente.dto");
const update_paciente_dto_1 = require("./dto/update-paciente.dto");
const getall_paciente_dto_1 = require("./dto/getall-paciente.dto");
const delete_enable_paciente_dto_1 = require("./dto/delete-enable-paciente.dto");
const paciente_service_1 = require("./paciente.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let PacienteController = PacienteController_1 = class PacienteController {
    constructor(request, pacienteService, customTransaction) {
        this.request = request;
        this.pacienteService = pacienteService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(PacienteController_1.name);
    }
    async findAll(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.pacienteService.findAll(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAfiliation(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.pacienteService.findAfiliation(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createPacienteDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultPaciente = await this.pacienteService.create(createPacienteDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultPaciente.pac_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updatePacienteDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultPaciente = await this.pacienteService.update(updatePacienteDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updatePacienteDto.pac_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnablePacienteDto, usuario) {
        try {
            deleteEnablePacienteDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultPaciente = await this.pacienteService.remove(deleteEnablePacienteDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultPaciente.pac_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnablePacienteDto, usuario) {
        try {
            deleteEnablePacienteDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultPaciente = await this.pacienteService.enable(deleteEnablePacienteDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultPaciente.pac_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_paciente_dto_1.GetAllPacienteDto]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)('afiliacion'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_paciente_dto_1.GetAllPacienteAfiliationDto]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "findAfiliation", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_paciente_dto_1.CreatePacienteDto]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_paciente_dto_1.UpdatePacienteDto]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':pac_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_paciente_dto_1.DeleteEnablePacienteDto, Number]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:pac_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_paciente_dto_1.DeleteEnablePacienteDto, Number]),
    __metadata("design:returntype", Promise)
], PacienteController.prototype, "enable", null);
PacienteController = PacienteController_1 = __decorate([
    (0, swagger_1.ApiTags)('Paciente'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('paciente'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, paciente_service_1.PacienteService, CustomTransaction_1.CustomTransaction])
], PacienteController);
exports.PacienteController = PacienteController;
//# sourceMappingURL=paciente.controller.js.map