import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateAntecedenteDto } from './dto/create-antecedente.dto';
import { GetAllAntecedenteDto } from './dto/getall-antecedente.dto';
import { DeleteEnableAntecedenteDto } from './dto/delete-enable-antecedente.dto';
import { AntecedenteService } from './antecedente.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class AntecedenteController {
    private request;
    private readonly antecedenteService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, antecedenteService: AntecedenteService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllAntecedenteDto): Promise<any>;
    create(res: any, createAntecedenteDto: CreateAntecedenteDto): Promise<ResultDto>;
    remove(res: any, deleteEnableAntecedenteDto: DeleteEnableAntecedenteDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableAntecedenteDto: DeleteEnableAntecedenteDto, usuario: number): Promise<any>;
}
