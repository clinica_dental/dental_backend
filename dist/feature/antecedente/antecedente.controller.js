"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AntecedenteController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedenteController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_antecedente_dto_1 = require("./dto/create-antecedente.dto");
const getall_antecedente_dto_1 = require("./dto/getall-antecedente.dto");
const delete_enable_antecedente_dto_1 = require("./dto/delete-enable-antecedente.dto");
const antecedente_service_1 = require("./antecedente.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let AntecedenteController = AntecedenteController_1 = class AntecedenteController {
    constructor(request, antecedenteService, customTransaction) {
        this.request = request;
        this.antecedenteService = antecedenteService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(AntecedenteController_1.name);
    }
    async findAll(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.antecedenteService.findAll(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createAntecedenteDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultAntecedente = await this.antecedenteService.create(createAntecedenteDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedente.ant_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnableAntecedenteDto, usuario) {
        try {
            deleteEnableAntecedenteDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultAntecedente = await this.antecedenteService.remove(deleteEnableAntecedenteDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedente.ant_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnableAntecedenteDto, usuario) {
        try {
            deleteEnableAntecedenteDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultAntecedente = await this.antecedenteService.enable(deleteEnableAntecedenteDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultAntecedente.ant_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_antecedente_dto_1.GetAllAntecedenteDto]),
    __metadata("design:returntype", Promise)
], AntecedenteController.prototype, "findAll", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_antecedente_dto_1.CreateAntecedenteDto]),
    __metadata("design:returntype", Promise)
], AntecedenteController.prototype, "create", null);
__decorate([
    (0, common_1.Delete)(':ant_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_antecedente_dto_1.DeleteEnableAntecedenteDto, Number]),
    __metadata("design:returntype", Promise)
], AntecedenteController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:ant_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_antecedente_dto_1.DeleteEnableAntecedenteDto, Number]),
    __metadata("design:returntype", Promise)
], AntecedenteController.prototype, "enable", null);
AntecedenteController = AntecedenteController_1 = __decorate([
    (0, swagger_1.ApiTags)('Antecedente'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('antecedente'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, antecedente_service_1.AntecedenteService, CustomTransaction_1.CustomTransaction])
], AntecedenteController);
exports.AntecedenteController = AntecedenteController;
//# sourceMappingURL=antecedente.controller.js.map