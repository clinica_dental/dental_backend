import { Request } from 'express';
import { Antecedente } from 'src/database/entities/Antecedente';
import { CreateAntecedenteDto } from './dto/create-antecedente.dto';
import { UpdateAntecedenteDto } from './dto/update-antecedente.dto';
import { GetAllAntecedenteDto } from './dto/getall-antecedente.dto';
import { DeleteEnableAntecedenteDto } from './dto/delete-enable-antecedente.dto';
import { EntityManager } from 'typeorm';
import { PacienteService } from '../paciente/paciente.service';
export declare class AntecedenteService {
    private request;
    private readonly pacienteService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, pacienteService: PacienteService);
    private validations;
    findAll(query: GetAllAntecedenteDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createAntecedenteDto: CreateAntecedenteDto, manager: EntityManager): Promise<Antecedente>;
    update(updateAntecedenteDto: UpdateAntecedenteDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableAntecedenteDto: DeleteEnableAntecedenteDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableAntecedenteDto: DeleteEnableAntecedenteDto, manager: EntityManager): Promise<any>;
}
