"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AntecedenteService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedenteService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const Antecedente_1 = require("../../database/entities/Antecedente");
const create_antecedente_dto_1 = require("./dto/create-antecedente.dto");
const update_antecedente_dto_1 = require("./dto/update-antecedente.dto");
const getall_antecedente_dto_1 = require("./dto/getall-antecedente.dto");
const delete_enable_antecedente_dto_1 = require("./dto/delete-enable-antecedente.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const paciente_service_1 = require("../paciente/paciente.service");
const getall_paciente_dto_1 = require("../paciente/dto/getall-paciente.dto");
let AntecedenteService = AntecedenteService_1 = class AntecedenteService {
    constructor(request, pacienteService) {
        this.request = request;
        this.pacienteService = pacienteService;
        this.message_custom = 'ANTECEDENTE -';
        this.logger = this.request.logger;
        this.logger.setContext(AntecedenteService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const antecedente = new Antecedente_1.Antecedente();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(Antecedente_1.Antecedente, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllPacienteDto = new getall_paciente_dto_1.GetAllPacienteDto();
                        getAllPacienteDto.pac_codigo = `(${params.pac_codigo})`;
                        getAllPacienteDto.pac_estado = '(1)';
                        await this.pacienteService.findAll(getAllPacienteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE PACIENTE NO EXISTE O NO ESTA ACTIVO');
                    }
                    (0, util_1.bindingObjects)(antecedente, params);
                    antecedente.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    antecedente.ant_codigo = params.ant_codigo;
                    antecedente.ant_estado = estados;
                    findResult = await manager.findOne(Antecedente_1.Antecedente, { where: antecedente });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    (0, util_1.bindingObjects)(antecedente, params);
                    antecedente.usuario_modificacion = usuario;
                    antecedente.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    antecedente.ant_codigo = params.ant_codigo;
                    antecedente.ant_estado = estados;
                    findResult = await manager.findOne(Antecedente_1.Antecedente, { where: antecedente });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    antecedente.usuario_baja = params.usuario;
                    antecedente.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    antecedente.ant_codigo = params.ant_codigo;
                    antecedente.ant_estado = estados;
                    findResult = await manager.findOne(Antecedente_1.Antecedente, { where: antecedente });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    antecedente.usuario_modificacion = params.usuario;
                    antecedente.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return antecedente;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      WITH tmp_antecedentes_patologicos AS (
        SELECT DISTINCT
          ap.ant_codigo
        FROM registro.antecedentes_patologicos ap  
        WHERE ap.antpat_estado > 0
    ), tmp_antecedentes_personales AS (
        SELECT DISTINCT
          ap2.ant_codigo
        FROM registro.antecedentes_personales ap2  
        WHERE ap2.antper_estado > 0
    ), tmp_antecedentes_afecciones AS (
       SELECT DISTINCT
          aa.ant_codigo
        FROM registro.antecedentes_afecciones aa  
        WHERE aa.antafe_estado > 0
    ), tmp_antecedentes_dentales AS (
        SELECT DISTINCT
          ad.ant_codigo
        FROM registro.antecedentes_dentales ad  
        WHERE ad.antden_estado > 0
    )
    SELECT 
       t.ant_codigo, 
       t.pac_codigo,
       '' AS paciente,
       t.ant_estado, 
       e.est_nombre AS ant_estado_descripcion, 
       CASE WHEN tap.ant_codigo IS NOT NULL THEN 1 ELSE 0 END AS bandera_tiene_patologicos, 
       CASE WHEN tap2.ant_codigo IS NOT NULL THEN 1 ELSE 0 END AS bandera_tiene_personales, 
       CASE WHEN taa.ant_codigo IS NOT NULL THEN 1 ELSE 0 END AS bandera_tiene_afecciones, 
       CASE WHEN tad.ant_codigo IS NOT NULL THEN 1 ELSE 0 END AS bandera_tiene_dentales
     FROM registro.antecedente t
     LEFT JOIN parametricas.estado e ON e.est_codigo = t.ant_estado
     LEFT JOIN tmp_antecedentes_patologicos tap ON tap.ant_codigo = t.ant_codigo
     LEFT JOIN tmp_antecedentes_personales tap2 ON tap2.ant_codigo = t.ant_codigo
     LEFT JOIN tmp_antecedentes_afecciones taa ON taa.ant_codigo = t.ant_codigo
     LEFT JOIN tmp_antecedentes_dentales tad ON tad.ant_codigo = t.ant_codigo
     WHERE TRUE
      ${query.ant_codigo ? `AND t.ant_codigo IN ${query.ant_codigo}` : ''}
      ${query.ant_estado ? `AND t.ant_estado IN ${query.ant_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            if (resultQuery.length > 0) {
                for (const antecedentes of resultQuery) {
                    try {
                        const getAllPacienteDto = new getall_paciente_dto_1.GetAllPacienteDto();
                        getAllPacienteDto.pac_codigo = `(${antecedentes.pac_codigo})`;
                        getAllPacienteDto.pac_estado = '(1)';
                        const pacienteResult = await this.pacienteService.findAll(getAllPacienteDto, manager);
                        antecedentes.paciente = pacienteResult[0];
                    }
                    catch (error) {
                        antecedentes.paciente = {};
                    }
                }
            }
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(Antecedente_1.Antecedente, { where: { ant_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createAntecedenteDto, manager) {
        try {
            const antecedente = await this.validations(1, manager, createAntecedenteDto);
            const sql = 'SELECT COALESCE(MAX(antecedente.ant_codigo), 0) + 1 codigo FROM registro.antecedente;';
            const codeResult = await manager.query(sql);
            antecedente.ant_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(antecedente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateAntecedenteDto, manager) {
        try {
            const antecedente = await this.validations(2, manager, updateAntecedenteDto);
            const resultQuery = await manager.update(Antecedente_1.Antecedente, antecedente.ant_codigo, antecedente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableAntecedenteDto, manager) {
        try {
            const antecedente = await this.validations(3, manager, deleteEnableAntecedenteDto);
            const resultQuery = await manager.update(Antecedente_1.Antecedente, antecedente.ant_codigo, antecedente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableAntecedenteDto, manager) {
        try {
            const antecedente = await this.validations(4, manager, deleteEnableAntecedenteDto);
            const resultQuery = await manager.update(Antecedente_1.Antecedente, antecedente.ant_codigo, antecedente);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], AntecedenteService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_antecedente_dto_1.GetAllAntecedenteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedenteService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedenteService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_antecedente_dto_1.CreateAntecedenteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedenteService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_antecedente_dto_1.UpdateAntecedenteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedenteService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedente_dto_1.DeleteEnableAntecedenteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedenteService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedente_dto_1.DeleteEnableAntecedenteDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedenteService.prototype, "enable", null);
AntecedenteService = AntecedenteService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, paciente_service_1.PacienteService])
], AntecedenteService);
exports.AntecedenteService = AntecedenteService;
//# sourceMappingURL=antecedente.service.js.map