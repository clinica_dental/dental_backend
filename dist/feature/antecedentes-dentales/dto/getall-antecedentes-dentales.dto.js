"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllAntecedentesDentalesDto = void 0;
const class_validator_1 = require("class-validator");
const IsCodesQuery_1 = require("../../../common/decorator/IsCodesQuery");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
class GetAllAntecedentesDentalesDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllAntecedentesDentalesDto.prototype, "ant_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    (0, IsCodesQuery_1.IsCodesQuery)({ message: '($property) No tiene el formato requerido.' }),
    __metadata("design:type", String)
], GetAllAntecedentesDentalesDto.prototype, "antden_estado", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: 'El usuario no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: 'El usuario no cumple con el valor maximo' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], GetAllAntecedentesDentalesDto.prototype, "usuario", void 0);
exports.GetAllAntecedentesDentalesDto = GetAllAntecedentesDentalesDto;
//# sourceMappingURL=getall-antecedentes-dentales.dto.js.map