import { CreateAntecedentesDentalesDto } from './create-antecedentes-dentales.dto';
export declare class UpdateAntecedentesDentalesDto extends CreateAntecedentesDentalesDto {
    ant_codigo: number;
}
