export declare class CreateAntecedentesDentalesDto {
    ant_codigo: number;
    antden_higiene: string;
    antden_respiracion: string;
    antden_onicofagia: string;
    antden_queilifagia: string;
    antden_bruxismo: string;
    antden_cepillado: string;
    antden_diagnostico: string;
    antden_ultimo_tratamiento: string;
    antden_observaciones: string;
    usuario: number;
}
