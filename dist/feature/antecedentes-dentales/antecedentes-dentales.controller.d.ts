import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateAntecedentesDentalesDto } from './dto/create-antecedentes-dentales.dto';
import { UpdateAntecedentesDentalesDto } from './dto/update-antecedentes-dentales.dto';
import { GetAllAntecedentesDentalesDto } from './dto/getall-antecedentes-dentales.dto';
import { DeleteEnableAntecedentesDentalesDto } from './dto/delete-enable-antecedentes-dentales.dto';
import { AntecedentesDentalesService } from './antecedentes-dentales.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class AntecedentesDentalesController {
    private request;
    private readonly antecedentesDentalesService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, antecedentesDentalesService: AntecedentesDentalesService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllAntecedentesDentalesDto): Promise<any>;
    create(res: any, createAntecedentesDentalesDto: CreateAntecedentesDentalesDto): Promise<ResultDto>;
    update(res: any, updateAntecedentesDentalesDto: UpdateAntecedentesDentalesDto): Promise<any>;
    remove(res: any, deleteEnableAntecedentesDentalesDto: DeleteEnableAntecedentesDentalesDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableAntecedentesDentalesDto: DeleteEnableAntecedentesDentalesDto, usuario: number): Promise<any>;
}
