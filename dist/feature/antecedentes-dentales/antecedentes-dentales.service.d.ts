import { Request } from 'express';
import { AntecedentesDentales } from 'src/database/entities/AntecedentesDentales';
import { CreateAntecedentesDentalesDto } from './dto/create-antecedentes-dentales.dto';
import { UpdateAntecedentesDentalesDto } from './dto/update-antecedentes-dentales.dto';
import { GetAllAntecedentesDentalesDto } from './dto/getall-antecedentes-dentales.dto';
import { DeleteEnableAntecedentesDentalesDto } from './dto/delete-enable-antecedentes-dentales.dto';
import { EntityManager } from 'typeorm';
import { AntecedenteService } from '../antecedente/antecedente.service';
export declare class AntecedentesDentalesService {
    private request;
    private readonly antecedenteService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, antecedenteService: AntecedenteService);
    private validations;
    findAll(query: GetAllAntecedentesDentalesDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createAntecedentesDentalesDto: CreateAntecedentesDentalesDto, manager: EntityManager): Promise<AntecedentesDentales>;
    update(updateAntecedentesDentalesDto: UpdateAntecedentesDentalesDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableAntecedentesDentalesDto: DeleteEnableAntecedentesDentalesDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableAntecedentesDentalesDto: DeleteEnableAntecedentesDentalesDto, manager: EntityManager): Promise<any>;
}
