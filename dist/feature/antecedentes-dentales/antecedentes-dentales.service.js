"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AntecedentesDentalesService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AntecedentesDentalesService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const AntecedentesDentales_1 = require("../../database/entities/AntecedentesDentales");
const create_antecedentes_dentales_dto_1 = require("./dto/create-antecedentes-dentales.dto");
const update_antecedentes_dentales_dto_1 = require("./dto/update-antecedentes-dentales.dto");
const getall_antecedentes_dentales_dto_1 = require("./dto/getall-antecedentes-dentales.dto");
const delete_enable_antecedentes_dentales_dto_1 = require("./dto/delete-enable-antecedentes-dentales.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const antecedente_service_1 = require("../antecedente/antecedente.service");
const getall_antecedente_dto_1 = require("../antecedente/dto/getall-antecedente.dto");
let AntecedentesDentalesService = AntecedentesDentalesService_1 = class AntecedentesDentalesService {
    constructor(request, antecedenteService) {
        this.request = request;
        this.antecedenteService = antecedenteService;
        this.message_custom = 'ANTECEDENTES DENTALES -';
        this.logger = this.request.logger;
        this.logger.setContext(AntecedentesDentalesService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const antecedentesDentales = new AntecedentesDentales_1.AntecedentesDentales();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(AntecedentesDentales_1.AntecedentesDentales, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllAntecedenteDto = new getall_antecedente_dto_1.GetAllAntecedenteDto();
                        getAllAntecedenteDto.ant_codigo = `(${params.ant_codigo})`;
                        getAllAntecedenteDto.ant_estado = '(1)';
                        await this.antecedenteService.findAll(getAllAntecedenteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE ANTECEDENTE NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(antecedentesDentales, params);
                    antecedentesDentales.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    antecedentesDentales.ant_codigo = params.ant_codigo;
                    antecedentesDentales.antden_estado = estados;
                    findResult = await manager.findOne(AntecedentesDentales_1.AntecedentesDentales, { where: antecedentesDentales });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllAntecedenteDto = new getall_antecedente_dto_1.GetAllAntecedenteDto();
                        getAllAntecedenteDto.ant_codigo = `(${params.ant_codigo})`;
                        getAllAntecedenteDto.ant_estado = '(1)';
                        await this.antecedenteService.findAll(getAllAntecedenteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE ANTECEDENTE NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(antecedentesDentales, params);
                    antecedentesDentales.usuario_modificacion = usuario;
                    antecedentesDentales.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    antecedentesDentales.ant_codigo = params.ant_codigo;
                    antecedentesDentales.antden_estado = estados;
                    findResult = await manager.findOne(AntecedentesDentales_1.AntecedentesDentales, { where: antecedentesDentales });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    antecedentesDentales.usuario_baja = params.usuario;
                    antecedentesDentales.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    antecedentesDentales.ant_codigo = params.ant_codigo;
                    antecedentesDentales.antden_estado = estados;
                    findResult = await manager.findOne(AntecedentesDentales_1.AntecedentesDentales, { where: antecedentesDentales });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.ant_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    antecedentesDentales.usuario_modificacion = params.usuario;
                    antecedentesDentales.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return antecedentesDentales;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.ant_codigo, 
        t.ant_codigo,
        t.antden_higiene,
        t.antden_respiracion,
        t.antden_onicofagia,
        t.antden_queilifagia,
        t.antden_bruxismo,
        t.antden_cepillado,
        t.antden_diagnostico,
        t.antden_ultimo_tratamiento,
        t.antden_observaciones,
        t.antden_estado, 
        e.est_nombre AS antden_estado_descripcion 
      FROM registro.antecedentes_dentales t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.antden_estado
      WHERE TRUE
      ${query.ant_codigo ? `AND t.ant_codigo IN ${query.ant_codigo}` : ''}
      ${query.antden_estado ? `AND t.antden_estado IN ${query.antden_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(AntecedentesDentales_1.AntecedentesDentales, { where: { ant_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createAntecedentesDentalesDto, manager) {
        try {
            const antecedentesDentales = await this.validations(1, manager, createAntecedentesDentalesDto);
            const resultQuery = await manager.save(antecedentesDentales);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateAntecedentesDentalesDto, manager) {
        try {
            const antecedentesDentales = await this.validations(2, manager, updateAntecedentesDentalesDto);
            const resultQuery = await manager.update(AntecedentesDentales_1.AntecedentesDentales, antecedentesDentales.ant_codigo, antecedentesDentales);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableAntecedentesDentalesDto, manager) {
        try {
            const antecedentesDentales = await this.validations(3, manager, deleteEnableAntecedentesDentalesDto);
            const resultQuery = await manager.update(AntecedentesDentales_1.AntecedentesDentales, antecedentesDentales.ant_codigo, antecedentesDentales);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableAntecedentesDentalesDto, manager) {
        try {
            const antecedentesDentales = await this.validations(4, manager, deleteEnableAntecedentesDentalesDto);
            const resultQuery = await manager.update(AntecedentesDentales_1.AntecedentesDentales, antecedentesDentales.ant_codigo, antecedentesDentales);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], AntecedentesDentalesService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_antecedentes_dentales_dto_1.GetAllAntecedentesDentalesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesDentalesService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesDentalesService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_antecedentes_dentales_dto_1.CreateAntecedentesDentalesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesDentalesService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_antecedentes_dentales_dto_1.UpdateAntecedentesDentalesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesDentalesService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedentes_dentales_dto_1.DeleteEnableAntecedentesDentalesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesDentalesService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_antecedentes_dentales_dto_1.DeleteEnableAntecedentesDentalesDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AntecedentesDentalesService.prototype, "enable", null);
AntecedentesDentalesService = AntecedentesDentalesService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, antecedente_service_1.AntecedenteService])
], AntecedentesDentalesService);
exports.AntecedentesDentalesService = AntecedentesDentalesService;
//# sourceMappingURL=antecedentes-dentales.service.js.map