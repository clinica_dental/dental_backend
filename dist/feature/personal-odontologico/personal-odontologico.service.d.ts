import { Request } from 'express';
import { PersonalOdontologico } from 'src/database/entities/PersonalOdontologico';
import { CreatePersonalOdontologicoDto } from './dto/create-personal-odontologico.dto';
import { UpdatePersonalOdontologicoDto } from './dto/update-personal-odontologico.dto';
import { GetAllPersonalOdontologicoDto } from './dto/getall-personal-odontologico.dto';
import { DeleteEnablePersonalOdontologicoDto } from './dto/delete-enable-personal-odontologico.dto';
import { EntityManager } from 'typeorm';
import { PersonaService } from '../persona/persona.service';
export declare class PersonalOdontologicoService {
    private request;
    private readonly personaService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, personaService: PersonaService);
    private validations;
    findAll(query: GetAllPersonalOdontologicoDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createPersonalOdontologicoDto: CreatePersonalOdontologicoDto, manager: EntityManager): Promise<PersonalOdontologico>;
    update(updatePersonalOdontologicoDto: UpdatePersonalOdontologicoDto, manager: EntityManager): Promise<any>;
    remove(deleteEnablePersonalOdontologicoDto: DeleteEnablePersonalOdontologicoDto, manager: EntityManager): Promise<any>;
    enable(deleteEnablePersonalOdontologicoDto: DeleteEnablePersonalOdontologicoDto, manager: EntityManager): Promise<any>;
}
