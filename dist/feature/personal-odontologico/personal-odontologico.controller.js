"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var PersonalOdontologicoController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonalOdontologicoController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_personal_odontologico_dto_1 = require("./dto/create-personal-odontologico.dto");
const update_personal_odontologico_dto_1 = require("./dto/update-personal-odontologico.dto");
const getall_personal_odontologico_dto_1 = require("./dto/getall-personal-odontologico.dto");
const delete_enable_personal_odontologico_dto_1 = require("./dto/delete-enable-personal-odontologico.dto");
const personal_odontologico_service_1 = require("./personal-odontologico.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let PersonalOdontologicoController = PersonalOdontologicoController_1 = class PersonalOdontologicoController {
    constructor(request, personalOdontologicoService, customTransaction) {
        this.request = request;
        this.personalOdontologicoService = personalOdontologicoService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(PersonalOdontologicoController_1.name);
    }
    async findAll(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.personalOdontologicoService.findAll(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createPersonalOdontologicoDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultPersonalOdontologico = await this.personalOdontologicoService.create(createPersonalOdontologicoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultPersonalOdontologico.perodo_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updatePersonalOdontologicoDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultPersonalOdontologico = await this.personalOdontologicoService.update(updatePersonalOdontologicoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updatePersonalOdontologicoDto.perodo_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnablePersonalOdontologicoDto, usuario) {
        try {
            deleteEnablePersonalOdontologicoDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultPersonalOdontologico = await this.personalOdontologicoService.remove(deleteEnablePersonalOdontologicoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultPersonalOdontologico.perodo_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnablePersonalOdontologicoDto, usuario) {
        try {
            deleteEnablePersonalOdontologicoDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultPersonalOdontologico = await this.personalOdontologicoService.enable(deleteEnablePersonalOdontologicoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultPersonalOdontologico.perodo_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoController.prototype, "findAll", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_personal_odontologico_dto_1.CreatePersonalOdontologicoDto]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_personal_odontologico_dto_1.UpdatePersonalOdontologicoDto]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':perodo_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_personal_odontologico_dto_1.DeleteEnablePersonalOdontologicoDto, Number]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:perodo_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_personal_odontologico_dto_1.DeleteEnablePersonalOdontologicoDto, Number]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoController.prototype, "enable", null);
PersonalOdontologicoController = PersonalOdontologicoController_1 = __decorate([
    (0, swagger_1.ApiTags)('PersonalOdontologico'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('personal-odontologico'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, personal_odontologico_service_1.PersonalOdontologicoService, CustomTransaction_1.CustomTransaction])
], PersonalOdontologicoController);
exports.PersonalOdontologicoController = PersonalOdontologicoController;
//# sourceMappingURL=personal-odontologico.controller.js.map