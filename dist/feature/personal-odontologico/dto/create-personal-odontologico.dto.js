"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePersonalOdontologicoDto = void 0;
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const cast_helper_1 = require("../../../common/helper/cast.helper");
const swagger_1 = require("@nestjs/swagger");
class CreatePersonalOdontologicoDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: '($property), El valor ($value) no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: '($property) El valor ($value) no cumple con el valor maximo' }),
    (0, class_validator_1.IsNumber)({}, { message: '($property) Debe ser de tipo número' }),
    __metadata("design:type", Number)
], CreatePersonalOdontologicoDto.prototype, "per_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.IsNumber)({}, { message: '($property) Debe ser de tipo número' }),
    __metadata("design:type", Number)
], CreatePersonalOdontologicoDto.prototype, "esp_codigo", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_transformer_1.Transform)(({ value }) => (0, cast_helper_1.trim)(value)),
    (0, class_validator_1.IsString)({ message: '($property) Debe ser una cadena de texto' }),
    __metadata("design:type", String)
], CreatePersonalOdontologicoDto.prototype, "perodo_turno", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_transformer_1.Transform)(({ value }) => (0, cast_helper_1.trim)(value)),
    (0, class_validator_1.IsString)({ message: '($property) Debe ser una cadena de texto' }),
    __metadata("design:type", String)
], CreatePersonalOdontologicoDto.prototype, "perodo_hora_inicio", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_transformer_1.Transform)(({ value }) => (0, cast_helper_1.trim)(value)),
    (0, class_validator_1.IsString)({ message: '($property) Debe ser una cadena de texto' }),
    __metadata("design:type", String)
], CreatePersonalOdontologicoDto.prototype, "perodo_hora_final", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: '($property) Es requerido' }),
    (0, class_validator_1.Min)(0, { message: '($property) El valor ($value) no cumple con el valor minimo' }),
    (0, class_validator_1.Max)(999999, { message: '($property) Valor Maximo Superado' }),
    (0, class_transformer_1.Type)(() => Number),
    __metadata("design:type", Number)
], CreatePersonalOdontologicoDto.prototype, "usuario", void 0);
exports.CreatePersonalOdontologicoDto = CreatePersonalOdontologicoDto;
//# sourceMappingURL=create-personal-odontologico.dto.js.map