import { CreatePersonalOdontologicoDto } from './create-personal-odontologico.dto';
export declare class UpdatePersonalOdontologicoDto extends CreatePersonalOdontologicoDto {
    perodo_codigo: number;
}
