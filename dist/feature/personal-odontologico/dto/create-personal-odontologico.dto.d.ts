export declare class CreatePersonalOdontologicoDto {
    per_codigo: number;
    esp_codigo: number;
    perodo_turno: string;
    perodo_hora_inicio: string;
    perodo_hora_final: string;
    usuario: number;
}
