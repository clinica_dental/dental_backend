export declare class GetAllPersonalOdontologicoDto {
    perodo_codigo: string;
    perodo_estado: string;
    general_query: string;
    usuario: number;
}
