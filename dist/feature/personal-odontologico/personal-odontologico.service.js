"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var PersonalOdontologicoService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonalOdontologicoService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const PersonalOdontologico_1 = require("../../database/entities/PersonalOdontologico");
const create_personal_odontologico_dto_1 = require("./dto/create-personal-odontologico.dto");
const update_personal_odontologico_dto_1 = require("./dto/update-personal-odontologico.dto");
const getall_personal_odontologico_dto_1 = require("./dto/getall-personal-odontologico.dto");
const delete_enable_personal_odontologico_dto_1 = require("./dto/delete-enable-personal-odontologico.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const getall_persona_dto_1 = require("../persona/dto/getall-persona.dto");
const persona_service_1 = require("../persona/persona.service");
let PersonalOdontologicoService = PersonalOdontologicoService_1 = class PersonalOdontologicoService {
    constructor(request, personaService) {
        this.request = request;
        this.personaService = personaService;
        this.message_custom = 'PERSONAL ODONTOLOGICO -';
        this.logger = this.request.logger;
        this.logger.setContext(PersonalOdontologicoService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const personalOdontologico = new PersonalOdontologico_1.PersonalOdontologico();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(PersonalOdontologico_1.PersonalOdontologico, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.perodo_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllPersonaDto = new getall_persona_dto_1.GetAllPersonaDto();
                        getAllPersonaDto.per_codigo = `(${params.per_codigo})`;
                        getAllPersonaDto.per_estado = '(1)';
                        await this.personaService.findAll(getAllPersonaDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE LA PERSONA ENVIADA NO EXISTE O ESTA INACTIVA');
                    }
                    findResult = await manager.findOne(PersonalOdontologico_1.PersonalOdontologico, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.perodo_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    (0, util_1.bindingObjects)(personalOdontologico, params);
                    personalOdontologico.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    personalOdontologico.perodo_codigo = params.perodo_codigo;
                    personalOdontologico.perodo_estado = estados;
                    findResult = await manager.findOne(PersonalOdontologico_1.PersonalOdontologico, { where: personalOdontologico });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.perodo_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllPersonaDto = new getall_persona_dto_1.GetAllPersonaDto();
                        getAllPersonaDto.per_codigo = `(${params.per_codigo})`;
                        getAllPersonaDto.per_estado = '(1)';
                        await this.personaService.findAll(getAllPersonaDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE LA PERSONA ENVIADA NO EXISTE O ESTA INACTIVA');
                    }
                    (0, util_1.bindingObjects)(personalOdontologico, params);
                    personalOdontologico.usuario_modificacion = usuario;
                    personalOdontologico.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    personalOdontologico.perodo_codigo = params.perodo_codigo;
                    personalOdontologico.perodo_estado = estados;
                    findResult = await manager.findOne(PersonalOdontologico_1.PersonalOdontologico, { where: personalOdontologico });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.perodo_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    personalOdontologico.usuario_baja = params.usuario;
                    personalOdontologico.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    personalOdontologico.perodo_codigo = params.perodo_codigo;
                    personalOdontologico.perodo_estado = estados;
                    findResult = await manager.findOne(PersonalOdontologico_1.PersonalOdontologico, { where: personalOdontologico });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.perodo_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    personalOdontologico.usuario_modificacion = params.usuario;
                    personalOdontologico.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return personalOdontologico;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.perodo_codigo,
        t.per_codigo,
        p.per_docidentidad,
        concat_ws(' ', p.per_nombres, p.per_primer_apellido, p.per_segundo_apellido) AS per_nombre_completo,
        p.per_celular, 
        p.per_correo, 
        e2.esp_codigo,
        e2.esp_nombre,
        t.perodo_turno,
        t.perodo_hora_inicio,
        t.perodo_hora_final,
        t.perodo_estado, 
        e.est_nombre AS perodo_estado_descripcion 
      FROM registro.personal_odontologico t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.perodo_estado
      LEFT JOIN registro.persona p ON p.per_codigo = t.per_codigo
      LEFT JOIN parametricas.especialidad e2 ON e2.esp_codigo = t.esp_codigo
      WHERE TRUE
      ${query.perodo_codigo ? `AND t.perodo_codigo IN ${query.perodo_codigo}` : ''}
      ${query.perodo_estado ? `AND t.perodo_estado IN ${query.perodo_estado}` : ''}
      ${query.general_query ? `${query.general_query}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(PersonalOdontologico_1.PersonalOdontologico, { where: { perodo_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createPersonalOdontologicoDto, manager) {
        try {
            const personalOdontologico = await this.validations(1, manager, createPersonalOdontologicoDto);
            const sql = 'SELECT COALESCE(MAX(personal_odontologico.perodo_codigo), 0) + 1 codigo FROM registro.personal_odontologico;';
            const codeResult = await manager.query(sql);
            personalOdontologico.perodo_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(personalOdontologico);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updatePersonalOdontologicoDto, manager) {
        try {
            const personalOdontologico = await this.validations(2, manager, updatePersonalOdontologicoDto);
            const resultQuery = await manager.update(PersonalOdontologico_1.PersonalOdontologico, personalOdontologico.perodo_codigo, personalOdontologico);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnablePersonalOdontologicoDto, manager) {
        try {
            const personalOdontologico = await this.validations(3, manager, deleteEnablePersonalOdontologicoDto);
            const resultQuery = await manager.update(PersonalOdontologico_1.PersonalOdontologico, personalOdontologico.perodo_codigo, personalOdontologico);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnablePersonalOdontologicoDto, manager) {
        try {
            const personalOdontologico = await this.validations(4, manager, deleteEnablePersonalOdontologicoDto);
            const resultQuery = await manager.update(PersonalOdontologico_1.PersonalOdontologico, personalOdontologico.perodo_codigo, personalOdontologico);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_personal_odontologico_dto_1.CreatePersonalOdontologicoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_personal_odontologico_dto_1.UpdatePersonalOdontologicoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_personal_odontologico_dto_1.DeleteEnablePersonalOdontologicoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_personal_odontologico_dto_1.DeleteEnablePersonalOdontologicoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PersonalOdontologicoService.prototype, "enable", null);
PersonalOdontologicoService = PersonalOdontologicoService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, persona_service_1.PersonaService])
], PersonalOdontologicoService);
exports.PersonalOdontologicoService = PersonalOdontologicoService;
//# sourceMappingURL=personal-odontologico.service.js.map