import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreatePersonalOdontologicoDto } from './dto/create-personal-odontologico.dto';
import { UpdatePersonalOdontologicoDto } from './dto/update-personal-odontologico.dto';
import { GetAllPersonalOdontologicoDto } from './dto/getall-personal-odontologico.dto';
import { DeleteEnablePersonalOdontologicoDto } from './dto/delete-enable-personal-odontologico.dto';
import { PersonalOdontologicoService } from './personal-odontologico.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class PersonalOdontologicoController {
    private request;
    private readonly personalOdontologicoService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, personalOdontologicoService: PersonalOdontologicoService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllPersonalOdontologicoDto): Promise<any>;
    create(res: any, createPersonalOdontologicoDto: CreatePersonalOdontologicoDto): Promise<ResultDto>;
    update(res: any, updatePersonalOdontologicoDto: UpdatePersonalOdontologicoDto): Promise<any>;
    remove(res: any, deleteEnablePersonalOdontologicoDto: DeleteEnablePersonalOdontologicoDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnablePersonalOdontologicoDto: DeleteEnablePersonalOdontologicoDto, usuario: number): Promise<any>;
}
