"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var TratamientoController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.TratamientoController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_tratamiento_dto_1 = require("./dto/create-tratamiento.dto");
const update_tratamiento_dto_1 = require("./dto/update-tratamiento.dto");
const getall_tratamiento_dto_1 = require("./dto/getall-tratamiento.dto");
const delete_enable_tratamiento_dto_1 = require("./dto/delete-enable-tratamiento.dto");
const tratamiento_service_1 = require("./tratamiento.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let TratamientoController = TratamientoController_1 = class TratamientoController {
    constructor(request, tratamientoService, customTransaction) {
        this.request = request;
        this.tratamientoService = tratamientoService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(TratamientoController_1.name);
    }
    async findAllDetail(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.tratamientoService.findAllDetail(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createTratamientoDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultTratamiento = await this.tratamientoService.create(createTratamientoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultTratamiento.tra_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updateTratamientoDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultTratamiento = await this.tratamientoService.update(updateTratamientoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updateTratamientoDto.tra_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnableTratamientoDto, usuario) {
        try {
            deleteEnableTratamientoDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultTratamiento = await this.tratamientoService.remove(deleteEnableTratamientoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultTratamiento.tra_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnableTratamientoDto, usuario) {
        try {
            deleteEnableTratamientoDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultTratamiento = await this.tratamientoService.enable(deleteEnableTratamientoDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultTratamiento.tra_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_tratamiento_dto_1.GetAllTratamientoDto]),
    __metadata("design:returntype", Promise)
], TratamientoController.prototype, "findAllDetail", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_tratamiento_dto_1.CreateTratamientoDto]),
    __metadata("design:returntype", Promise)
], TratamientoController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_tratamiento_dto_1.UpdateTratamientoDto]),
    __metadata("design:returntype", Promise)
], TratamientoController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':tra_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_tratamiento_dto_1.DeleteEnableTratamientoDto, Number]),
    __metadata("design:returntype", Promise)
], TratamientoController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:tra_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_tratamiento_dto_1.DeleteEnableTratamientoDto, Number]),
    __metadata("design:returntype", Promise)
], TratamientoController.prototype, "enable", null);
TratamientoController = TratamientoController_1 = __decorate([
    (0, swagger_1.ApiTags)('Tratamiento'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('tratamiento'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, tratamiento_service_1.TratamientoService, CustomTransaction_1.CustomTransaction])
], TratamientoController);
exports.TratamientoController = TratamientoController;
//# sourceMappingURL=tratamiento.controller.js.map