import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateTratamientoDto } from './dto/create-tratamiento.dto';
import { UpdateTratamientoDto } from './dto/update-tratamiento.dto';
import { GetAllTratamientoDto } from './dto/getall-tratamiento.dto';
import { DeleteEnableTratamientoDto } from './dto/delete-enable-tratamiento.dto';
import { TratamientoService } from './tratamiento.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class TratamientoController {
    private request;
    private readonly tratamientoService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, tratamientoService: TratamientoService, customTransaction: CustomTransaction);
    findAllDetail(res: any, query: GetAllTratamientoDto): Promise<any>;
    create(res: any, createTratamientoDto: CreateTratamientoDto): Promise<ResultDto>;
    update(res: any, updateTratamientoDto: UpdateTratamientoDto): Promise<any>;
    remove(res: any, deleteEnableTratamientoDto: DeleteEnableTratamientoDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableTratamientoDto: DeleteEnableTratamientoDto, usuario: number): Promise<any>;
}
