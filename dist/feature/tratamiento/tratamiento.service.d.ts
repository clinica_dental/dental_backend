import { Request } from 'express';
import { Tratamiento } from 'src/database/entities/Tratamiento';
import { CreateTratamientoDto } from './dto/create-tratamiento.dto';
import { UpdateTratamientoDto } from './dto/update-tratamiento.dto';
import { GetAllTratamientoDto } from './dto/getall-tratamiento.dto';
import { DeleteEnableTratamientoDto } from './dto/delete-enable-tratamiento.dto';
import { EntityManager } from 'typeorm';
import { PersonalOdontologicoService } from '../personal-odontologico/personal-odontologico.service';
import { PacienteService } from '../paciente/paciente.service';
export declare class TratamientoService {
    private request;
    private personalOdontologicoService;
    private pacienteService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, personalOdontologicoService: PersonalOdontologicoService, pacienteService: PacienteService);
    private validations;
    findAll(query: GetAllTratamientoDto, manager: EntityManager): Promise<any>;
    findAllDetail(query: GetAllTratamientoDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createTratamientoDto: CreateTratamientoDto, manager: EntityManager): Promise<Tratamiento>;
    update(updateTratamientoDto: UpdateTratamientoDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableTratamientoDto: DeleteEnableTratamientoDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableTratamientoDto: DeleteEnableTratamientoDto, manager: EntityManager): Promise<any>;
}
