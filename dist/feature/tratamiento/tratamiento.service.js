"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var TratamientoService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.TratamientoService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const Tratamiento_1 = require("../../database/entities/Tratamiento");
const create_tratamiento_dto_1 = require("./dto/create-tratamiento.dto");
const update_tratamiento_dto_1 = require("./dto/update-tratamiento.dto");
const getall_tratamiento_dto_1 = require("./dto/getall-tratamiento.dto");
const delete_enable_tratamiento_dto_1 = require("./dto/delete-enable-tratamiento.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const personal_odontologico_service_1 = require("../personal-odontologico/personal-odontologico.service");
const paciente_service_1 = require("../paciente/paciente.service");
const getall_paciente_dto_1 = require("../paciente/dto/getall-paciente.dto");
const getall_personal_odontologico_dto_1 = require("../personal-odontologico/dto/getall-personal-odontologico.dto");
let TratamientoService = TratamientoService_1 = class TratamientoService {
    constructor(request, personalOdontologicoService, pacienteService) {
        this.request = request;
        this.personalOdontologicoService = personalOdontologicoService;
        this.pacienteService = pacienteService;
        this.message_custom = 'TRATAMIENTO -';
        this.logger = this.request.logger;
        this.logger.setContext(TratamientoService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            let findResult = {};
            const tratamiento = new Tratamiento_1.Tratamiento();
            const estados = new Estado_1.Estado();
            const usuario = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(Tratamiento_1.Tratamiento, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.tra_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllPacienteDto = new getall_paciente_dto_1.GetAllPacienteDto();
                        getAllPacienteDto.pac_codigo = `(${params.pac_codigo})`;
                        getAllPacienteDto.pac_estado = '(1)';
                        await this.pacienteService.findAll(getAllPacienteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE PACIENTE NO EXISTE O ESTA INACTIVO');
                    }
                    try {
                        const getAllPersonalOdontologicoDto = new getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto();
                        getAllPersonalOdontologicoDto.perodo_codigo = `(${params.perodo_codigo})`;
                        getAllPersonalOdontologicoDto.perodo_estado = '(1)';
                        await this.personalOdontologicoService.findAll(getAllPersonalOdontologicoDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DEL PERSONAL ODONTOLOGICO NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(tratamiento, params);
                    tratamiento.usuario_registro = usuario;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    tratamiento.tra_codigo = params.tra_codigo;
                    tratamiento.tra_estado = estados;
                    findResult = await manager.findOne(Tratamiento_1.Tratamiento, { where: tratamiento });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.tra_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllPacienteDto = new getall_paciente_dto_1.GetAllPacienteDto();
                        getAllPacienteDto.pac_codigo = `(${params.pac_codigo})`;
                        getAllPacienteDto.pac_estado = '(1)';
                        await this.pacienteService.findAll(getAllPacienteDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE PACIENTE NO EXISTE O ESTA INACTIVO');
                    }
                    try {
                        const getAllPersonalOdontologicoDto = new getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto();
                        getAllPersonalOdontologicoDto.perodo_codigo = `(${params.perodo_codigo})`;
                        getAllPersonalOdontologicoDto.perodo_estado = '(1)';
                        await this.personalOdontologicoService.findAll(getAllPersonalOdontologicoDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DEL PERSONAL ODONTOLOGICO NO EXISTE O ESTA INACTIVO');
                    }
                    (0, util_1.bindingObjects)(tratamiento, params);
                    tratamiento.usuario_modificacion = usuario;
                    tratamiento.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    tratamiento.tra_codigo = params.tra_codigo;
                    tratamiento.tra_estado = estados;
                    findResult = await manager.findOne(Tratamiento_1.Tratamiento, { where: tratamiento });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.tra_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    tratamiento.usuario_baja = params.usuario;
                    tratamiento.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    tratamiento.tra_codigo = params.tra_codigo;
                    tratamiento.tra_estado = estados;
                    findResult = await manager.findOne(Tratamiento_1.Tratamiento, { where: tratamiento });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.tra_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    tratamiento.usuario_modificacion = params.usuario;
                    tratamiento.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return tratamiento;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.tra_codigo,
        t.tiptra_codigo, 
        t.pac_codigo, 
        t.perodo_codigo, 
        t.tra_total, 
        t.tra_observaciones,
        t.tra_estado, 
        e.est_nombre AS tra_estado_descripcion 
      FROM registro.tratamiento t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.tra_estado
      WHERE TRUE
      ${query.tra_codigo ? `AND t.tra_codigo IN ${query.tra_codigo}` : ''}
      ${query.tra_estado ? `AND t.tra_estado IN ${query.tra_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllDetail(query, manager) {
        try {
            let sql = `
      WITH tmp_tiene_plan_tratamiento AS (
        SELECT 
          DISTINCT 
          tp.tra_codigo  
        FROM registro.tratamiento_plan tp 
        WHERE tp.trapla_estado > 0
      )
      SELECT 
        t.tra_codigo,
        t.tiptra_codigo, 
        tt.tiptra_nombre,
        t.pac_codigo, 
        t.perodo_codigo, 
        t.tra_total, 
        t.tra_observaciones, 
        '' AS paciente,
        '' AS personal_odontologico,
        t.tra_estado, 
        e.est_nombre AS tra_estado_descripcion, 
        CASE WHEN ttpt.tra_codigo IS NOT NULL THEN 1 ELSE 0 END AS bandera_tiene_plantratamiento
      FROM registro.tratamiento t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.tra_estado
      LEFT JOIN parametricas.tipo_tratamiento tt ON tt.tiptra_codigo = t.tiptra_codigo
      LEFT JOIN tmp_tiene_plan_tratamiento ttpt ON ttpt.tra_codigo = t.tra_codigo
      WHERE TRUE
      
      ${query.tra_codigo ? `AND t.tra_codigo IN ${query.tra_codigo}` : ''}
      ${query.tra_estado ? `AND t.tra_estado IN ${query.tra_estado}` : ''}
      ${query.pac_codigo ? `AND t.pac_codigo IN ${query.pac_codigo}` : ''};`;
            const resultQuery = await manager.query(sql);
            if (resultQuery.length > 0) {
                for (const tratamiento of resultQuery) {
                    try {
                        const getAllPacienteDto = new getall_paciente_dto_1.GetAllPacienteDto();
                        getAllPacienteDto.pac_codigo = `(${tratamiento.pac_codigo})`;
                        getAllPacienteDto.pac_estado = '(1)';
                        const pacienteResult = await this.pacienteService.findAll(getAllPacienteDto, manager);
                        tratamiento.paciente = pacienteResult[0];
                    }
                    catch (error) {
                        tratamiento.paciente = {};
                    }
                    try {
                        const getAllPersonalOdontologicoDto = new getall_personal_odontologico_dto_1.GetAllPersonalOdontologicoDto();
                        getAllPersonalOdontologicoDto.perodo_codigo = `(${tratamiento.perodo_codigo})`;
                        getAllPersonalOdontologicoDto.perodo_estado = '(1)';
                        const personalOdontologicoResult = await this.personalOdontologicoService.findAll(getAllPersonalOdontologicoDto, manager);
                        tratamiento.personal_odontologico = personalOdontologicoResult[0];
                    }
                    catch (error) {
                        tratamiento.personal_odontologico = {};
                    }
                }
            }
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(Tratamiento_1.Tratamiento, { where: { tra_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createTratamientoDto, manager) {
        try {
            const tratamiento = await this.validations(1, manager, createTratamientoDto);
            const sql = 'SELECT COALESCE(MAX(tratamiento.tra_codigo), 0) + 1 codigo FROM registro.tratamiento;';
            const codeResult = await manager.query(sql);
            tratamiento.tra_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(tratamiento);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateTratamientoDto, manager) {
        try {
            const tratamiento = await this.validations(2, manager, updateTratamientoDto);
            const resultQuery = await manager.update(Tratamiento_1.Tratamiento, tratamiento.tra_codigo, tratamiento);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableTratamientoDto, manager) {
        try {
            const tratamiento = await this.validations(3, manager, deleteEnableTratamientoDto);
            const resultQuery = await manager.update(Tratamiento_1.Tratamiento, tratamiento.tra_codigo, tratamiento);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableTratamientoDto, manager) {
        try {
            const tratamiento = await this.validations(4, manager, deleteEnableTratamientoDto);
            const resultQuery = await manager.update(Tratamiento_1.Tratamiento, tratamiento.tra_codigo, tratamiento);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_tratamiento_dto_1.GetAllTratamientoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_tratamiento_dto_1.GetAllTratamientoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "findAllDetail", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_tratamiento_dto_1.CreateTratamientoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_tratamiento_dto_1.UpdateTratamientoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_tratamiento_dto_1.DeleteEnableTratamientoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_tratamiento_dto_1.DeleteEnableTratamientoDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], TratamientoService.prototype, "enable", null);
TratamientoService = TratamientoService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, personal_odontologico_service_1.PersonalOdontologicoService,
        paciente_service_1.PacienteService])
], TratamientoService);
exports.TratamientoService = TratamientoService;
//# sourceMappingURL=tratamiento.service.js.map