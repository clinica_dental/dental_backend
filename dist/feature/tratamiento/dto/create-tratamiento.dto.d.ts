export declare class CreateTratamientoDto {
    tiptra_codigo: number;
    pac_codigo: number;
    perodo_codigo: number;
    tra_total: string;
    tra_observaciones: string;
    usuario: number;
}
