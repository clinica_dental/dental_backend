import { CreateTratamientoDto } from './create-tratamiento.dto';
export declare class UpdateTratamientoDto extends CreateTratamientoDto {
    tra_codigo: number;
}
