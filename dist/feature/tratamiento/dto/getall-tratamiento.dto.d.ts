export declare class GetAllTratamientoDto {
    tra_codigo: string;
    pac_codigo: string;
    tra_estado: string;
    usuario: number;
}
