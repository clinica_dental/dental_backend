import { Request } from 'express';
import { Usuario } from 'src/database/entities/Usuario';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { GetAllUsuarioDto } from './dto/getall-usuario.dto';
import { DeleteEnableUsuarioDto } from './dto/delete-enable-usuario.dto';
import { EntityManager } from 'typeorm';
import { PersonaService } from '../persona/persona.service';
import { GetSessionDto } from './dto/getsession.dto';
export declare class UsuarioService {
    private request;
    private readonly personaService;
    private readonly logger;
    private message_custom;
    private customToken;
    constructor(request: Request, personaService: PersonaService);
    private validations;
    findAll(query: GetAllUsuarioDto, manager: EntityManager): Promise<any>;
    findOne(id: number, manager: EntityManager): Promise<any>;
    create(createUsuarioDto: CreateUsuarioDto, manager: EntityManager): Promise<Usuario>;
    finUserSession(query: GetSessionDto, manager: EntityManager): Promise<any>;
    update(updateUsuarioDto: UpdateUsuarioDto, manager: EntityManager): Promise<any>;
    remove(deleteEnableUsuarioDto: DeleteEnableUsuarioDto, manager: EntityManager): Promise<any>;
    enable(deleteEnableUsuarioDto: DeleteEnableUsuarioDto, manager: EntityManager): Promise<any>;
}
