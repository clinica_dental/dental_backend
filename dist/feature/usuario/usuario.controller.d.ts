import { Request } from 'express';
import { ResultDto } from 'src/common/dto/result';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { GetAllUsuarioDto } from './dto/getall-usuario.dto';
import { DeleteEnableUsuarioDto } from './dto/delete-enable-usuario.dto';
import { UsuarioService } from './usuario.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
import { GetSessionDto } from './dto/getsession.dto';
export declare class UsuarioController {
    private request;
    private readonly usuarioService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, usuarioService: UsuarioService, customTransaction: CustomTransaction);
    findAll(res: any, query: GetAllUsuarioDto): Promise<any>;
    finUserSession(res: any, getSessionDto: GetSessionDto): Promise<any>;
    create(res: any, createUsuarioDto: CreateUsuarioDto): Promise<ResultDto>;
    update(res: any, updateUsuarioDto: UpdateUsuarioDto): Promise<any>;
    remove(res: any, deleteEnableUsuarioDto: DeleteEnableUsuarioDto, usuario: number): Promise<any>;
    enable(res: any, deleteEnableUsuarioDto: DeleteEnableUsuarioDto, usuario: number): Promise<any>;
}
