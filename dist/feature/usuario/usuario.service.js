"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var UsuarioService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsuarioService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const util_1 = require("../../common/util/util");
const customService_1 = require("../../common/exception/customService");
const Estado_1 = require("../../database/entities/Estado");
const Usuario_1 = require("../../database/entities/Usuario");
const create_usuario_dto_1 = require("./dto/create-usuario.dto");
const update_usuario_dto_1 = require("./dto/update-usuario.dto");
const getall_usuario_dto_1 = require("./dto/getall-usuario.dto");
const delete_enable_usuario_dto_1 = require("./dto/delete-enable-usuario.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const persona_service_1 = require("../persona/persona.service");
const getall_persona_dto_1 = require("../persona/dto/getall-persona.dto");
const getsession_dto_1 = require("./dto/getsession.dto");
const CustomJwt_1 = require("../../common/util/CustomJwt");
let UsuarioService = UsuarioService_1 = class UsuarioService {
    constructor(request, personaService) {
        this.request = request;
        this.personaService = personaService;
        this.message_custom = 'USUARIO -';
        this.customToken = new CustomJwt_1.CustomToken();
        this.logger = this.request.logger;
        this.logger.setContext(UsuarioService_1.name);
    }
    async validations(operation, manager, params) {
        try {
            const { createHash } = await Promise.resolve().then(() => require('node:crypto'));
            let findResult = {};
            const usuario = new Usuario_1.Usuario();
            const estados = new Estado_1.Estado();
            const usuarioCodigo = params.usuario;
            if (!operation || (operation < 1 || operation > 4))
                (0, error_handling_1.throwError)(400, 'LA OPERACION NO ESTA PERMITIDA');
            if (!params.usuario || params.usuario < 0)
                (0, error_handling_1.throwError)(400, 'VALOR NO PERMITIDO PARA USUARIO');
            switch (operation) {
                case 1:
                    delete params.usuario;
                    findResult = await manager.findOne(Usuario_1.Usuario, { where: params });
                    if (findResult === null || findResult === void 0 ? void 0 : findResult.usu_codigo)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO YA EXISTE CON LOS DATOS ENVIADOS');
                    try {
                        const getAllPersonaDto = new getall_persona_dto_1.GetAllPersonaDto();
                        getAllPersonaDto.per_codigo = `(${params.per_codigo})`;
                        getAllPersonaDto.per_estado = '(1)';
                        await this.personaService.findAll(getAllPersonaDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE LA PERSONA ENVIADA NO EXISTE O ESTA INACTIVA');
                    }
                    (0, util_1.bindingObjects)(usuario, params);
                    usuario.usu_contrasenia = createHash('sha256').update(usuario.usu_contrasenia).digest('base64');
                    usuario.usuario_registro = usuarioCodigo;
                    break;
                case 2:
                    delete params.usuario;
                    estados.est_codigo = 1;
                    usuario.usu_codigo = params.usu_codigo;
                    usuario.usu_estado = estados;
                    if (params.usu_contrasenia) {
                        params.usu_contrasenia = createHash('sha256').update(params.usu_contrasenia).digest('base64');
                    }
                    findResult = await manager.findOne(Usuario_1.Usuario, { where: usuario });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.usu_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO EXISTE O ESTA INACTIVO');
                    let modificado = false;
                    for (const m of Object.keys(params)) {
                        let value = findResult[m];
                        if (typeof value == 'object')
                            value = findResult[m][m];
                        modificado = (params[m] != value) ? true : false;
                        if (modificado)
                            break;
                    }
                    if (!modificado)
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO HA SIDO MODIFICADO');
                    try {
                        const getAllPersonaDto = new getall_persona_dto_1.GetAllPersonaDto();
                        getAllPersonaDto.per_codigo = `(${params.per_codigo})`;
                        getAllPersonaDto.per_estado = '(1)';
                        await this.personaService.findAll(getAllPersonaDto, manager);
                    }
                    catch (error) {
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO DE LA PERSONA ENVIADA NO EXISTE O ESTA INACTIVA');
                    }
                    (0, util_1.bindingObjects)(usuario, params);
                    usuario.usuario_modificacion = usuarioCodigo;
                    usuario.fecha_modificacion = new Date();
                    break;
                case 3:
                    estados.est_codigo = 1;
                    usuario.usu_codigo = params.usu_codigo;
                    usuario.usu_estado = estados;
                    findResult = await manager.findOne(Usuario_1.Usuario, { where: usuario });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.usu_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA BAJA, O NO EXISTE.');
                    estados.est_codigo = 0;
                    usuario.usuario_baja = params.usuario;
                    usuario.fecha_baja = new Date();
                    break;
                case 4:
                    estados.est_codigo = 0;
                    usuario.usu_codigo = params.usu_codigo;
                    usuario.usu_estado = estados;
                    findResult = await manager.findOne(Usuario_1.Usuario, { where: usuario });
                    if (!(findResult === null || findResult === void 0 ? void 0 : findResult.usu_codigo))
                        (0, error_handling_1.throwError)(400, 'EL REGISTRO NO SE ENCUENTRA EN UN ESTADO PARA HABILITACION, O NO EXISTE.');
                    estados.est_codigo = 1;
                    usuario.usuario_modificacion = params.usuario;
                    usuario.fecha_modificacion = new Date();
                    break;
                default:
                    (0, error_handling_1.throwError)(400, 'OPERACION DE VALIDACIÓN NO PERMITIDA');
                    break;
            }
            return usuario;
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message || 'ERROR EN PROCESO DE VALIDACIÓN');
        }
    }
    async findAll(query, manager) {
        try {
            let sql = `
      SELECT 
        t.usu_codigo, 
        t.usu_usuario,
        p.per_docidentidad,
        concat_ws(' ', p.per_nombres, p.per_primer_apellido, p.per_segundo_apellido) AS per_nombre_completo,
        t.usu_estado, 
        e.est_nombre AS usu_estado_descripcion 
      FROM autenticacion.usuario t
      LEFT JOIN parametricas.estado e ON e.est_codigo = t.usu_estado
      LEFT JOIN registro.persona p ON p.per_codigo = t.per_codigo
      WHERE TRUE
      ${query.usu_codigo ? `AND t.usu_codigo IN ${query.usu_codigo}` : ''}
      ${query.usu_estado ? `AND t.usu_estado IN ${query.usu_estado}` : ''};`;
            const resultQuery = await manager.query(sql);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findOne(id, manager) {
        try {
            const resultQuery = await manager.findOne(Usuario_1.Usuario, { where: { usu_codigo: id } });
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async create(createUsuarioDto, manager) {
        try {
            const usuario = await this.validations(1, manager, createUsuarioDto);
            const sql = 'SELECT COALESCE(MAX(usuario.usu_codigo), 0) + 1 codigo FROM autenticacion.usuario;';
            const codeResult = await manager.query(sql);
            usuario.usu_codigo = codeResult[0].codigo;
            const resultQuery = await manager.save(usuario);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async finUserSession(query, manager) {
        try {
            const { createHash } = await Promise.resolve().then(() => require('node:crypto'));
            query.usu_contrasenia = createHash('sha256').update(query.usu_contrasenia).digest('base64');
            let sql = `
      WITH tmp_usuario AS (
        SELECT 
          u.usu_codigo, 
          u.per_codigo, 
          u.usu_usuario, 
          p.per_docidentidad, 
          p.per_nombres, 
          p.per_primer_apellido, 
          p.per_segundo_apellido,
          concat_ws(' ', p.per_nombres, p.per_primer_apellido, p.per_segundo_apellido) AS per_nombre_completo,
          p.per_correo,
          p.per_celular 
        FROM autenticacion.usuario u 
        INNER JOIN registro.persona p ON p.per_codigo = u.per_codigo 
        WHERE TRUE 
        AND p.per_estado = 1
        AND u.usu_estado = 1
        AND u.usu_usuario = '${query.usu_usuario}'
        AND u.usu_contrasenia = '${query.usu_contrasenia}'
      ), tmp_usuario_roles AS (
        SELECT 
          ur.usu_codigo, 
          ur.rol_codigo, 
          r.rol_nombre, 
          ur.usurol_fechaexpiracion 
        FROM autenticacion.usuario_rol ur
        INNER JOIN autenticacion.rol r ON r.rol_codigo = ur.rol_codigo 
        INNER JOIN tmp_usuario tu ON tu.usu_codigo = ur.usu_codigo 
        WHERE ur.usurol_estado = 1
        AND r.rol_estado = 1
        AND ur.usurol_estado = 1
      ), tmp_roles_menus AS (
        SELECT 
          DISTINCT 
      --		m.men_codigo,
          m.men_icono AS icon, 
          m.men_nombre AS title, 
          m.men_ruta AS to, 
          m.men_orden AS order
        FROM autenticacion.menu m 
        LEFT JOIN autenticacion.rol_menu rm ON rm.men_codigo = m.men_codigo 
        LEFT JOIN tmp_usuario_roles tur ON tur.rol_codigo = rm.rol_codigo 
        WHERE m.men_estado = 1
        AND rm.rolmen_estado = 1
        ORDER BY m.men_orden 
      ), tmp_usuario_agrupado AS (
        SELECT 
          json_agg(tmp_usuario) AS usuario
        FROM tmp_usuario
      ), tmp_rol_agrupado AS (
        SELECT 
          json_agg(tmp_usuario_roles) AS roles
        FROM tmp_usuario_roles
      ), tmp_menu_agrupado AS (
        SELECT 
          json_agg(tmp_roles_menus) AS menus
        FROM tmp_roles_menus
      )
      SELECT 
        *,
        '' AS token
      FROM tmp_usuario_agrupado, tmp_rol_agrupado, tmp_menu_agrupado;`;
            const resultQuery = await manager.query(sql);
            const result = resultQuery[0];
            result.usuario = result.usuario[0];
            if (!result.usuario) {
                (0, error_handling_1.throwError)(400, 'USUARIO O CONTRASEÑA INCORRECTOS');
            }
            const payload = {
                per_codigo: result.usuario.per_codigo,
                username: result.usuario.usu_usuario,
            };
            result.token = (await this.customToken.createUserToken(payload)).token;
            return customService_1.CustomService.verifyingDataResult(result, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async update(updateUsuarioDto, manager) {
        try {
            const usuario = await this.validations(2, manager, updateUsuarioDto);
            const resultQuery = await manager.update(Usuario_1.Usuario, usuario.usu_codigo, usuario);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async remove(deleteEnableUsuarioDto, manager) {
        try {
            const usuario = await this.validations(3, manager, deleteEnableUsuarioDto);
            const resultQuery = await manager.update(Usuario_1.Usuario, usuario.usu_codigo, usuario);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async enable(deleteEnableUsuarioDto, manager) {
        try {
            const usuario = await this.validations(4, manager, deleteEnableUsuarioDto);
            const resultQuery = await manager.update(Usuario_1.Usuario, usuario.usu_codigo, usuario);
            return customService_1.CustomService.verifyingDataResult(resultQuery, this.message_custom);
        }
        catch (error) {
            this.logger.debug(error);
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager, Object]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "validations", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_usuario_dto_1.GetAllUsuarioDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "findAll", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "findOne", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_usuario_dto_1.CreateUsuarioDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "create", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getsession_dto_1.GetSessionDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "finUserSession", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_usuario_dto_1.UpdateUsuarioDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "update", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_usuario_dto_1.DeleteEnableUsuarioDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "remove", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [delete_enable_usuario_dto_1.DeleteEnableUsuarioDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UsuarioService.prototype, "enable", null);
UsuarioService = UsuarioService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, persona_service_1.PersonaService])
], UsuarioService);
exports.UsuarioService = UsuarioService;
//# sourceMappingURL=usuario.service.js.map