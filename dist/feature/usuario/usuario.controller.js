"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var UsuarioController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsuarioController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const create_usuario_dto_1 = require("./dto/create-usuario.dto");
const update_usuario_dto_1 = require("./dto/update-usuario.dto");
const getall_usuario_dto_1 = require("./dto/getall-usuario.dto");
const delete_enable_usuario_dto_1 = require("./dto/delete-enable-usuario.dto");
const usuario_service_1 = require("./usuario.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
const getsession_dto_1 = require("./dto/getsession.dto");
let UsuarioController = UsuarioController_1 = class UsuarioController {
    constructor(request, usuarioService, customTransaction) {
        this.request = request;
        this.usuarioService = usuarioService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(UsuarioController_1.name);
    }
    async findAll(res, query) {
        try {
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', await this.usuarioService.findAll(query, this.customTransaction.manager));
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async finUserSession(res, getSessionDto) {
        try {
            const result = await this.usuarioService.finUserSession(getSessionDto, this.customTransaction.manager);
            const resultSuccess = {
                codigo: 0,
                error_existente: 0,
                error_mensaje: 'SE OBTUVIERON DATOS DE FORMA CORRECTA.',
                error_codigo: 2001,
                datos: result
            };
            return res.status(common_1.HttpStatus.OK).send(resultSuccess);
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async create(res, createUsuarioDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultUsuario = await this.usuarioService.create(createUsuarioDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultUsuario.usu_codigo, 'SE REALIZO EL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async update(res, updateUsuarioDto) {
        try {
            await this.customTransaction.getStartTransaction();
            let resultUsuario = await this.usuarioService.update(updateUsuarioDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, updateUsuarioDto.usu_codigo, 'SE REALIZO LA MODIFICACION DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async remove(res, deleteEnableUsuarioDto, usuario) {
        try {
            deleteEnableUsuarioDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultUsuario = await this.usuarioService.remove(deleteEnableUsuarioDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultUsuario.usu_codigo, 'SE REALIZO LA BAJA DEL REGISTRO DE FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async enable(res, deleteEnableUsuarioDto, usuario) {
        try {
            deleteEnableUsuarioDto.usuario = usuario;
            await this.customTransaction.getStartTransaction();
            let resultUsuario = await this.usuarioService.enable(deleteEnableUsuarioDto, this.customTransaction.manager);
            await this.customTransaction.getCommitTransaction();
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, resultUsuario.usu_codigo, 'SE REALIZO LA HABILITACIÓN DEL FORMA CORRECTA.');
        }
        catch (error) {
            if (this.customTransaction.getIsTransactionActive())
                await this.customTransaction.getRollbackTransaction();
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_usuario_dto_1.GetAllUsuarioDto]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "findAll", null);
__decorate([
    (0, common_1.Post)('login'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getsession_dto_1.GetSessionDto]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "finUserSession", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_usuario_dto_1.CreateUsuarioDto]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "create", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, update_usuario_dto_1.UpdateUsuarioDto]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':usu_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_usuario_dto_1.DeleteEnableUsuarioDto, Number]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "remove", null);
__decorate([
    (0, common_1.Patch)('/:usu_codigo'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)()),
    __param(2, (0, common_1.Body)('usuario')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, delete_enable_usuario_dto_1.DeleteEnableUsuarioDto, Number]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "enable", null);
UsuarioController = UsuarioController_1 = __decorate([
    (0, swagger_1.ApiTags)('Usuario'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('usuario'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, usuario_service_1.UsuarioService, CustomTransaction_1.CustomTransaction])
], UsuarioController);
exports.UsuarioController = UsuarioController;
//# sourceMappingURL=usuario.controller.js.map