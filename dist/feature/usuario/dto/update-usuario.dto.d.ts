import { CreateUsuarioDto } from './create-usuario.dto';
export declare class UpdateUsuarioDto extends CreateUsuarioDto {
    usu_codigo: number;
}
