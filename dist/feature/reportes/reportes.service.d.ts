import { Request } from 'express';
import { GetAllAfiliacionDto, GetAllTratamientoDtoPla } from './dto/getall-reportes.dto';
import { EntityManager } from 'typeorm';
import { PacienteService } from '../paciente/paciente.service';
import { TratamientoPlanService } from '../tratamiento-plan/tratamiento-plan.service';
export declare class ReportesService {
    private request;
    private pacienteService;
    private tratamientoPlanService;
    private readonly logger;
    private message_custom;
    constructor(request: Request, pacienteService: PacienteService, tratamientoPlanService: TratamientoPlanService);
    findAllAfiliacion(query: GetAllAfiliacionDto, manager: EntityManager): Promise<any>;
    findAllTratamientoPlan(query: GetAllTratamientoDtoPla, manager: EntityManager): Promise<any>;
    getPdf(html: any): Promise<any>;
}
