"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var ReportesService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportesService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const error_handling_1 = require("../../common/exception/error.handling");
const customService_1 = require("../../common/exception/customService");
const getall_reportes_dto_1 = require("./dto/getall-reportes.dto");
const typeorm_1 = require("typeorm");
const LoggerMethod_1 = require("../../common/decorator/LoggerMethod");
const pug_1 = require("pug");
const paciente_service_1 = require("../paciente/paciente.service");
const getall_paciente_dto_1 = require("../paciente/dto/getall-paciente.dto");
const html_pdf_node_1 = require("html-pdf-node");
const tratamiento_plan_service_1 = require("../tratamiento-plan/tratamiento-plan.service");
const getall_tratamiento_plan_dto_1 = require("../tratamiento-plan/dto/getall-tratamiento-plan.dto");
let ReportesService = ReportesService_1 = class ReportesService {
    constructor(request, pacienteService, tratamientoPlanService) {
        this.request = request;
        this.pacienteService = pacienteService;
        this.tratamientoPlanService = tratamientoPlanService;
        this.message_custom = 'REPORTES -';
        this.logger = this.request.logger;
        this.logger.setContext(ReportesService_1.name);
    }
    async findAllAfiliacion(query, manager) {
        try {
            let resultData = {};
            try {
                const params = new getall_paciente_dto_1.GetAllPacienteAfiliationDto();
                params.pac_codigo = query.pac_codigo;
                resultData = await this.pacienteService.findAfiliation(params, manager);
            }
            catch (error) { }
            const header = (0, pug_1.renderFile)('src/config/templates/afiliacion.header.pug', resultData.datos_paciente);
            const datos_paciente = (0, pug_1.renderFile)('src/config/templates/afiliacion.datos.paciente.pug', resultData.datos_paciente);
            const antedecentes_patologicos = (0, pug_1.renderFile)('src/config/templates/afiliacion.patologicos.pug', { patologicos: resultData.antedecentes_patologicos });
            const antecedentes_personales = (0, pug_1.renderFile)('src/config/templates/afiliacion.personales.pug', { personales: resultData.antecedentes_personales });
            const antencedentes_afecciones = (0, pug_1.renderFile)('src/config/templates/afiliacion.afecciones.pug', { afecciones: resultData.antencedentes_afecciones });
            const antecedentes_dentales = (0, pug_1.renderFile)('src/config/templates/afiliacion.dentales.pug', { dentales: resultData.antecedentes_dentales });
            const resultQuery = `<!DOCTYPE html><html>
        ${header}
        <body>
          ${datos_paciente}
          ${antedecentes_patologicos}
          ${antecedentes_personales}
          ${antencedentes_afecciones}
          ${antecedentes_dentales}
        </body>
      </html>
      `;
            const pdf = this.getPdf(resultQuery);
            return customService_1.CustomService.verifyingDataResult(pdf, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async findAllTratamientoPlan(query, manager) {
        try {
            let resultData = {};
            try {
                const params = new getall_tratamiento_plan_dto_1.GetAllTratamientoPlanDto();
                params.tra_codigo = query.tra_codigo;
                params.trapla_estado = '(1)';
                resultData = await this.tratamientoPlanService.findAll(params, manager);
            }
            catch (error) { }
            const resultQuery = (0, pug_1.renderFile)('src/config/templates/plan-tratamiento.pug', { planTratamiento: resultData });
            const pdf = this.getPdf(resultQuery);
            return customService_1.CustomService.verifyingDataResult(pdf, this.message_custom);
        }
        catch (error) {
            (0, error_handling_1.throwError)(400, error.message);
        }
    }
    async getPdf(html) {
        let html_validate = html;
        html_validate = html_validate.replace(/\n/g, '');
        html_validate = html_validate.replace(/\"/g, '"');
        const options = {
            format: 'Letter',
            printBackground: true,
            margin: {
                bottom: 30,
                top: 30,
                right: 30,
                left: 0
            },
            preferCSSPageSize: true
        };
        const file = { content: html };
        const pdf = await (0, html_pdf_node_1.generatePdf)(file, options);
        return pdf.toString('base64');
    }
};
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_reportes_dto_1.GetAllAfiliacionDto, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReportesService.prototype, "findAllAfiliacion", null);
__decorate([
    LoggerMethod_1.LoggerMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [getall_reportes_dto_1.GetAllTratamientoDtoPla, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], ReportesService.prototype, "findAllTratamientoPlan", null);
ReportesService = ReportesService_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, paciente_service_1.PacienteService,
        tratamiento_plan_service_1.TratamientoPlanService])
], ReportesService);
exports.ReportesService = ReportesService;
//# sourceMappingURL=reportes.service.js.map