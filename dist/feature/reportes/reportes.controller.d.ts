import { Request } from 'express';
import { GetAllAfiliacionDto, GetAllTratamientoDtoPla } from './dto/getall-reportes.dto';
import { ReportesService } from './reportes.service';
import { CustomTransaction } from 'src/common/util/CustomTransaction';
export declare class ReportesController {
    private request;
    private readonly reportesService;
    private customTransaction;
    private readonly logger;
    constructor(request: Request, reportesService: ReportesService, customTransaction: CustomTransaction);
    findAllAfiliacion(res: any, query: GetAllAfiliacionDto): Promise<any>;
    findAllTratamientoPlan(res: any, query: GetAllTratamientoDtoPla): Promise<any>;
}
