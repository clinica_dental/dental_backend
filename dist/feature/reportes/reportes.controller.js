"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var ReportesController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportesController = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const sender_handling_1 = require("../../common/exception/sender.handling");
const swagger_1 = require("@nestjs/swagger");
const getall_reportes_dto_1 = require("./dto/getall-reportes.dto");
const reportes_service_1 = require("./reportes.service");
const CustomTransaction_1 = require("../../common/util/CustomTransaction");
let ReportesController = ReportesController_1 = class ReportesController {
    constructor(request, reportesService, customTransaction) {
        this.request = request;
        this.reportesService = reportesService;
        this.customTransaction = customTransaction;
        this.logger = this.request.logger;
        this.logger.setContext(ReportesController_1.name);
    }
    async findAllAfiliacion(res, query) {
        try {
            const result = await this.reportesService.findAllAfiliacion(query, this.customTransaction.manager);
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', result);
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
    async findAllTratamientoPlan(res, query) {
        try {
            const result = await this.reportesService.findAllTratamientoPlan(query, this.customTransaction.manager);
            return (0, sender_handling_1.sendSuccess)(res, common_1.HttpStatus.OK, 0, 'SE OBTUVIERON DATOS DE FORMA CORRECTA.', result);
        }
        catch (error) {
            return (0, sender_handling_1.sendError)(res, common_1.HttpStatus.BAD_REQUEST, 0, error.message);
        }
    }
};
__decorate([
    (0, common_1.Get)('afiliacion'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_reportes_dto_1.GetAllAfiliacionDto]),
    __metadata("design:returntype", Promise)
], ReportesController.prototype, "findAllAfiliacion", null);
__decorate([
    (0, common_1.Get)('tratamiento-plan'),
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, getall_reportes_dto_1.GetAllTratamientoDtoPla]),
    __metadata("design:returntype", Promise)
], ReportesController.prototype, "findAllTratamientoPlan", null);
ReportesController = ReportesController_1 = __decorate([
    (0, swagger_1.ApiTags)('Reportes'),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.Controller)('reportes'),
    __param(0, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, reportes_service_1.ReportesService, CustomTransaction_1.CustomTransaction])
], ReportesController);
exports.ReportesController = ReportesController;
//# sourceMappingURL=reportes.controller.js.map