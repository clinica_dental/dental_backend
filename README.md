# Dental Backend
 
## Proyecto dental backend
 
### Instalación
 
> :warning: **Usar node v14 o superior**:
 
Clonar el proyecto desde el repositorio
```bash
git clone https://gitlab.com/proyecto_dental/dental_compilados/dental_backend.git
cd dental_backend
```
 
Instalar dependencias
 
```bash
npm i
```
 
Copiar el archivo de configuración donde están definidos puertos, conexiones a base de datos entre otras configuraciones.
 
```bash
cp .env.sample .env
```
 
Levantar el proyecto
 
```bash
npm run start
```
 
La aplicación se levantará en el puerto definido por defecto **http://localhost:5001**
La documentación del api en Open Api está disponible en la ruta **http://localhost:5001/api**